package nth.module.utilities.model.inon;

public class VehicleModelES {
    private String frame_no;
    private String iss_date;
    private String iss_place;
    private String init_value;
    private String machine_no;
    private String number_plate;
    private String contract_value;
    private String vehicle_type_name;

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getIss_date() {
        return iss_date;
    }

    public void setIss_date(String iss_date) {
        this.iss_date = iss_date;
    }

    public String getIss_place() {
        return iss_place;
    }

    public void setIss_place(String iss_place) {
        this.iss_place = iss_place;
    }

    public String getInit_value() {
        return init_value;
    }

    public void setInit_value(String init_value) {
        this.init_value = init_value;
    }

    public String getMachine_no() {
        return machine_no;
    }

    public void setMachine_no(String machine_no) {
        this.machine_no = machine_no;
    }

    public String getNumber_plate() {
        return number_plate;
    }

    public void setNumber_plate(String number_plate) {
        this.number_plate = number_plate;
    }

    public String getContract_value() {
        return contract_value;
    }

    public void setContract_value(String contract_value) {
        this.contract_value = contract_value;
    }

    public String getVehicle_type_name() {
        return vehicle_type_name;
    }

    public void setVehicle_type_name(String vehicle_type_name) {
        this.vehicle_type_name = vehicle_type_name;
    }
}

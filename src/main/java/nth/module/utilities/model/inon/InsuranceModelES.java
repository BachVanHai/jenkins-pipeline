package nth.module.utilities.model.inon;

public class InsuranceModelES {
    private String count_1;
    private String count_2;
    private String count_3;
    private String value_1;
    private String value_2;
    private String value_3;
    private String duration;
    private int is_enable;
    private String end_value_date;
    private String insurance_code;
    private String start_value_date;

    public String getCount_1() {
        return count_1;
    }

    public void setCount_1(String count_1) {
        this.count_1 = count_1;
    }

    public String getCount_2() {
        return count_2;
    }

    public void setCount_2(String count_2) {
        this.count_2 = count_2;
    }

    public String getCount_3() {
        return count_3;
    }

    public void setCount_3(String count_3) {
        this.count_3 = count_3;
    }

    public String getValue_1() {
        return value_1;
    }

    public void setValue_1(String value_1) {
        this.value_1 = value_1;
    }

    public String getValue_2() {
        return value_2;
    }

    public void setValue_2(String value_2) {
        this.value_2 = value_2;
    }

    public String getValue_3() {
        return value_3;
    }

    public void setValue_3(String value_3) {
        this.value_3 = value_3;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getIs_enable() {
        return is_enable;
    }

    public void setIs_enable(int is_enable) {
        this.is_enable = is_enable;
    }

    public String getEnd_value_date() {
        return end_value_date;
    }

    public void setEnd_value_date(String end_value_date) {
        this.end_value_date = end_value_date;
    }

    public String getInsurance_code() {
        return insurance_code;
    }

    public void setInsurance_code(String insurance_code) {
        this.insurance_code = insurance_code;
    }

    public String getStart_value_date() {
        return start_value_date;
    }

    public void setStart_value_date(String start_value_date) {
        this.start_value_date = start_value_date;
    }
}

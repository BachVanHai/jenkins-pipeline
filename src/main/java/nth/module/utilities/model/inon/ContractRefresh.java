package nth.module.utilities.model.inon;


import java.util.List;

public class ContractRefresh {
    List<String> contractCodes;

    String startDate;

    String endDate;

    private String username;

    private String password;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getContractCodes() {
        return contractCodes;
    }

    public void setContractCodes(List<String> contractCodes) {
        this.contractCodes = contractCodes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package nth.module.utilities.web.rest.vm;

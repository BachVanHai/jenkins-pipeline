package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import nth.module.utilities.repository.HCFileUploadRepository;
import nth.module.utilities.service.HCFileUploadService;
import nth.module.utilities.service.dto.HCFileUploadDTO;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link nth.module.utilities.domain.HCFileUpload}.
 */
@RestController
@RequestMapping("/api")
public class HCFileUploadResource {

    private final Logger log = LoggerFactory.getLogger(HCFileUploadResource.class);

    private static final String ENTITY_NAME = "hCFileUpload";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HCFileUploadService hCFileUploadService;

    private final HCFileUploadRepository hCFileUploadRepository;

    public HCFileUploadResource(HCFileUploadService hCFileUploadService, HCFileUploadRepository hCFileUploadRepository) {
        this.hCFileUploadService = hCFileUploadService;
        this.hCFileUploadRepository = hCFileUploadRepository;
    }

    /**
     * {@code POST  /hc-file-uploads} : Create a new hCFileUpload.
     *
     * @param hcFileUploadDTOList the hCFileUploadDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hCFileUploadDTO, or with status {@code 400 (Bad Request)} if the hCFileUpload has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hc-file-uploads")
    public ResponseEntity<List<HCFileUploadDTO>> createHCFileUpload(Principal principal,
                                                                    @Valid @RequestBody List<HCFileUploadDTO> hcFileUploadDTOList)
        throws URISyntaxException {
        log.debug("REST request to save HCFileUpload : {}", hcFileUploadDTOList);
        List<HCFileUploadDTO> result = hCFileUploadService.createFileUpload(principal.getName(), hcFileUploadDTOList);
        return ResponseEntity.ok().body(result);
    }

    //todo create file upload public.
    @PostMapping("/authenticate/hc-file-uploads")
    public ResponseEntity<List<HCFileUploadDTO>> createHCFileUploadPublic(
        @Valid @RequestBody List<HCFileUploadDTO> hcFileUploadDTOList)
        throws URISyntaxException {
        log.debug("REST request to save HCFileUpload : {}", hcFileUploadDTOList);
        List<HCFileUploadDTO> result = hCFileUploadService.createFileUpload(null, hcFileUploadDTOList);
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code PUT  /hc-file-uploads/:id} : Updates an existing hCFileUpload.
     *
     * @param id              the id of the hCFileUploadDTO to save.
     * @param hCFileUploadDTO the hCFileUploadDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hCFileUploadDTO,
     * or with status {@code 400 (Bad Request)} if the hCFileUploadDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hCFileUploadDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hc-file-uploads/{id}")
    public ResponseEntity<HCFileUploadDTO> updateHCFileUpload(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody HCFileUploadDTO hCFileUploadDTO
    ) throws URISyntaxException {
        log.debug("REST request to update HCFileUpload : {}, {}", id, hCFileUploadDTO);
        if (hCFileUploadDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hCFileUploadDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hCFileUploadRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        HCFileUploadDTO result = hCFileUploadService.save(hCFileUploadDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCFileUploadDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hc-file-uploads} : get all the hCFileUploads.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hCFileUploads in body.
     */
    @GetMapping("/hc-file-uploads")
    public List<HCFileUploadDTO> getAllHCFileUploads() {
        log.debug("REST request to get all HCFileUploads");
        return hCFileUploadService.findAll();
    }

    /**
     * {@code GET  /hc-file-uploads/:id} : get the "id" hCFileUpload.
     *
     * @param id the id of the hCFileUploadDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hCFileUploadDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hc-file-uploads/{id}")
    public ResponseEntity<HCFileUploadDTO> getHCFileUpload(@PathVariable Long id) {
        log.debug("REST request to get HCFileUpload : {}", id);
        Optional<HCFileUploadDTO> hCFileUploadDTO = hCFileUploadService.findOne(id);
        return ResponseUtil.wrapOrNotFound(hCFileUploadDTO);
    }

    /**
     * {@code DELETE  /hc-file-uploads/:id} : delete the "id" hCFileUpload.
     *
     * @param id the id of the hCFileUploadDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hc-file-uploads/{id}")
    public ResponseEntity<Void> deleteHCFileUpload(@PathVariable Long id) {
        log.debug("REST request to delete HCFileUpload : {}", id);
        hCFileUploadService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

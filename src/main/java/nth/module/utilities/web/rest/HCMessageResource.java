package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

import nth.lib.common.exception.BusinessException;
import nth.module.utilities.client.ContactManagerClient;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.repository.HCMessageRepository;
import nth.module.utilities.service.HCFileUploadService;
import nth.module.utilities.service.HCMessageService;
import nth.module.utilities.service.HCSupportBookService;
import nth.module.utilities.service.dto.HCMessageDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing {@link nth.module.utilities.domain.HCMessage}.
 */
@RestController
@RequestMapping("/api")
public class HCMessageResource {

    private final Logger log = LoggerFactory.getLogger(HCMessageResource.class);

    private static final String ENTITY_NAME = "hCMessage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HCMessageService hCMessageService;
    private final HCMessageRepository hCMessageRepository;
    private final HCFileUploadService hcFileUploadService;
    private final HCSupportBookService hcSupportBookService;
    private final UserClient userClient;

    public HCMessageResource(HCMessageService hCMessageService, HCMessageRepository hCMessageRepository, ContactManagerClient contactManagerClient, HCFileUploadService hcFileUploadService, HCSupportBookService hcSupportBookService, UserClient userClient) {
        this.hCMessageService = hCMessageService;
        this.hCMessageRepository = hCMessageRepository;
        this.hcFileUploadService = hcFileUploadService;
        this.hcSupportBookService = hcSupportBookService;
        this.userClient = userClient;
    }

    /**
     * {@code POST  /hc-messages} : Create a new hCMessage.
     *
     * @param hCMessageDTO the hCMessageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hCMessageDTO, or with status {@code 400 (Bad Request)} if the hCMessage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hc-messages")
    public ResponseEntity<HCMessageDTO> createHCMessage(@RequestBody HCMessageDTO hCMessageDTO) throws URISyntaxException {
        log.debug("REST request to save HCMessage : {}", hCMessageDTO);
        if (hCMessageDTO.getId() != null) {
            throw new BadRequestAlertException("A new hCMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HCMessageDTO result = hCMessageService.createMessage(hCMessageDTO);
        return ResponseEntity
            .created(new URI("/api/hc-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

//    @PostMapping("/authenticate/hc-messages")
//    public ResponseEntity<HCMessageDTO> createHCMessagePublic(@RequestBody HCMessageDTO hCMessageDTO) throws URISyntaxException {
//        log.debug("REST request to save HCMessage : {}", hCMessageDTO);
//        if (hCMessageDTO.getId() != null) {
//            throw new BadRequestAlertException("A new hCMessage cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        HCMessageDTO result = hCMessageService.createMessage(hCMessageDTO);
//        return ResponseEntity
//            .created(new URI("/api/hc-messages/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }

    /*
     * todo checking read.
     *  */
    @PostMapping(value = "/authenticate/hc-message/reader")
    public ResponseEntity<?> checkingReader(@RequestBody HashMap<String, String> body) {

        String roomId = body.get("roomId");
        String userId = body.get("userId");

        try {
            hCMessageService.checkingMessage(userId, roomId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }

    /**
     * {@code PUT  /hc-messages/:id} : Updates an existing hCMessage.
     *
     * @param id           the id of the hCMessageDTO to save.
     * @param hCMessageDTO the hCMessageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hCMessageDTO,
     * or with status {@code 400 (Bad Request)} if the hCMessageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hCMessageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hc-messages/{id}")
    public ResponseEntity<HCMessageDTO> updateHCMessage(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody HCMessageDTO hCMessageDTO
    ) throws URISyntaxException {
        log.debug("REST request to update HCMessage : {}, {}", id, hCMessageDTO);
        if (hCMessageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hCMessageDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hCMessageRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        HCMessageDTO result = hCMessageService.save(hCMessageDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCMessageDTO.getId().toString()))
            .body(result);
    }

    /*
     * todo find all mess of room
     *  */
    @GetMapping(value = "/hc-messages/room")
    public ResponseEntity<List<HCMessageDTO>> getAllHCMessageRoomId(Principal principal, Pageable pageable,
                                                                    @RequestParam(name = "roomId") String roomId,
                                                                    HttpServletRequest httpServletRequest) {


        String deviceId = httpServletRequest.getHeader("deviceid");

        Optional<HCSupportBookDTO> hcSupportBookDTOOptional = hcSupportBookService.findOneByRoomId(roomId, principal.getName());

        if (!hcSupportBookDTOOptional.isPresent() || !hcSupportBookDTOOptional.get().getDeviceId().equals(deviceId)) {
            throw new BusinessException("supportBookNotFound");
        }


        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("sendDate").ascending());

        List<HCMessageDTO> hcMessageDTOList = hCMessageService.findAllByRoomId(roomId, pageable);

        return ResponseEntity.ok().body(hcMessageDTOList);
    }
}

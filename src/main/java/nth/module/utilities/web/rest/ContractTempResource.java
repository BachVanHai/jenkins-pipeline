package nth.module.utilities.web.rest;

import feign.Response;
import nth.lib.common.exception.BusinessException;
import nth.module.utilities.service.ContractTempService;
import nth.module.utilities.service.dto.ContractTempDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api")
public class ContractTempResource {

    private final ContractTempService contractTempService;

    public ContractTempResource(ContractTempService contractTempService) {
        this.contractTempService = contractTempService;
    }

    @PostMapping(value = "/contract-temp")
    public ResponseEntity<List<ContractTempDTO>> createContractTemp(Principal principal,
                                                                    @RequestBody List<ContractTempDTO> request,
                                                                    @RequestParam(name = "contractType", required = false, defaultValue = "CC") String contractType){
        List<ContractTempDTO> response = contractTempService.createContractTemp(request, principal.getName(), contractType);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping(value = "/contract-temp/contract-by-code")
    public ResponseEntity<ContractTempDTO> getContractTempByCode(@RequestParam(name = "code") String code) {

        Optional<ContractTempDTO> contractTempDTOOptional = contractTempService.findByCode(code);
        if (!contractTempDTOOptional.isPresent()){
            throw new BusinessException("contractNotFound");
        }
        ContractTempDTO contractTempDTO = contractTempDTOOptional.get();
        return ResponseEntity.ok().body(contractTempDTO);
    }
}

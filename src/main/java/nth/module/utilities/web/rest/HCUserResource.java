package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import nth.lib.common.exception.BusinessException;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.repository.HCUserRepository;
import nth.module.utilities.service.HCUserService;
import nth.module.utilities.service.dto.HCUserDTO;
import nth.module.utilities.utils.RoleUtils;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing {@link nth.module.utilities.domain.HCUser}.
 */
@RestController
@RequestMapping("/api")
public class HCUserResource {

    private final Logger log = LoggerFactory.getLogger(HCUserResource.class);

    private static final String ENTITY_NAME = "hCUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HCUserService hCUserService;
    private final HCUserRepository hCUserRepository;
    private final UserClient userClient;

    public HCUserResource(HCUserService hCUserService, HCUserRepository hCUserRepository, UserClient userClient) {
        this.hCUserService = hCUserService;
        this.hCUserRepository = hCUserRepository;
        this.userClient = userClient;
    }

    /**
     * {@code POST  /hc-users} : Create a new hCUser.
     *
     * @param hCUserDTO the hCUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hCUserDTO, or with status {@code 400 (Bad Request)} if the hCUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hc-users")
    public ResponseEntity<HCUserDTO> createHCUser(Principal principal,
                                                  @RequestBody HCUserDTO hCUserDTO,
                                                  HttpServletRequest httpServletRequest) throws URISyntaxException {
        log.debug("REST request to save HCUser : {}", hCUserDTO);

        String deviceId = httpServletRequest.getHeader("deviceid");
        hCUserDTO.setDeviceId(deviceId);
        if (hCUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new hCUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            HCUserDTO response = hCUserService.createUserSupportBook(principal.getName(), hCUserDTO);

            return ResponseEntity
                .created(new URI("/api/hc-users/" + response.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, response.getId().toString()))
                .body(response);
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

//    @PostMapping("/authenticate/hc-users")
//    public ResponseEntity<HCUserDTO> createHCUserPublic(Principal principal,
//                                                        @RequestBody HCUserDTO hCUserDTO,
//                                                        HttpServletRequest httpServletRequest) throws URISyntaxException {
//        log.debug("REST request to save HCUser : {}", hCUserDTO);
//        if (hCUserDTO.getId() != null) {
//            throw new BadRequestAlertException("A new hCUser cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        String deviceId = httpServletRequest.getHeader("deviceid");
//        hCUserDTO.setDeviceId(deviceId);
//        HCUserDTO result = hCUserService.createUserSupportBook(null, hCUserDTO);
//        return ResponseEntity
//            .created(new URI("/api/hc-users/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }

    /**
     * {@code PUT  /hc-users/:id} : Updates an existing hCUser.
     *
     * @param id        the id of the hCUserDTO to save.
     * @param hCUserDTO the hCUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hCUserDTO,
     * or with status {@code 400 (Bad Request)} if the hCUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hCUserDTO couldn't be updated.
     */
    @PutMapping("/hc-users/{id}")
    public ResponseEntity<HCUserDTO> updateHCUser(Principal principal,
                                                  @PathVariable(value = "id", required = false) final Long id,
                                                  @RequestBody HCUserDTO hCUserDTO
    ) throws IllegalAccessException, InstantiationException {

        log.debug("REST request to update HCUser : {}, {}", id, hCUserDTO);
        if (hCUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if (!Objects.equals(id, hCUserDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hCUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        try  {
            Optional<HCUserDTO> response = hCUserService.updateUserSupportBook(principal == null ? "Guest" : principal.getName(), hCUserDTO);

            return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCUserDTO.getId().toString()))
                .body(response.get());
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

//    @PutMapping("/authenticate/hc-users/{id}")
//    public ResponseEntity<HCUserDTO> updateHCUserPublic(
//        @PathVariable(value = "id", required = false) final Long id,
//        @RequestBody HCUserDTO hCUserDTO
//    ) throws IllegalAccessException, InstantiationException {
//        log.debug("REST request to update HCUser : {}, {}", id, hCUserDTO);
//        if (hCUserDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        if (!Objects.equals(id, hCUserDTO.getId())) {
//            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
//        }
//
//        if (!hCUserRepository.existsById(id)) {
//            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
//        }
//
//        Optional<HCUserDTO> result = hCUserService.updateUserSupportBook(null, hCUserDTO);
//        return ResponseEntity
//            .ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCUserDTO.getId().toString()))
//            .body(result.get());
//    }

    /**
     * {@code GET  /hc-users} : get all the hCUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hCUsers in body.
     */
    @GetMapping("/hc-users")
    public ResponseEntity<List<HCUserDTO>> getAllHCUsers(Principal principal) {
        String roleGroup = RoleUtils.getRoleGroup(userClient, principal.getName());
        if (!RoleUtils.isAdmin(roleGroup)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        log.debug("REST request to get all HCUsers");
        return ResponseEntity.ok().body(hCUserService.findAll());
    }

    /**
     * {@code GET  /hc-users/:id} : get the "id" hCUser.
     *
     * @param id the id of the hCUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hCUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hc-users/{id}")
    public ResponseEntity<HCUserDTO> getHCUser(Principal principal, @PathVariable Long id) {
        log.debug("REST request to get HCUser : {}", id);
        String roleGroup = RoleUtils.getRoleGroup(this.userClient,principal.getName());
        if (principal.getName().equals(id + "") || RoleUtils.isAdmin(roleGroup)) {
            Optional<HCUserDTO> hCUserDTO = hCUserService.findOne(id);
            return ResponseUtil.wrapOrNotFound(hCUserDTO);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

    /**
     * {@code DELETE  /hc-users/:id} : delete the "id" hCUser.
     *
     * @param id the id of the hCUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hc-users/{id}")
    public ResponseEntity<Void> deleteHCUser(Principal principal,
                                             @PathVariable Long id) {
        log.debug("REST request to delete HCUser : {}", id);

        String roleGroup = RoleUtils.getRoleGroup(userClient, principal.getName());
        if (!RoleUtils.isAdmin(roleGroup)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        hCUserService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

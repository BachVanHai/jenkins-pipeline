package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.repository.HCDecentralizationRepository;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link nth.module.utilities.domain.HCDecentralization}.
 */
@RestController
@RequestMapping("/api")
public class HCDecentralizationResource {

    private final Logger log = LoggerFactory.getLogger(HCDecentralizationResource.class);

    private static final String ENTITY_NAME = "hCDecentralization";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HCDecentralizationService hCDecentralizationService;

    private final HCDecentralizationRepository hCDecentralizationRepository;

    private final UserClient userClient;

    public HCDecentralizationResource(
        HCDecentralizationService hCDecentralizationService,
        HCDecentralizationRepository hCDecentralizationRepository,
        UserClient userClient) {
        this.hCDecentralizationService = hCDecentralizationService;
        this.hCDecentralizationRepository = hCDecentralizationRepository;
        this.userClient = userClient;
    }

    /**
     * {@code POST  /hc-decentralizations} : Create a new hCDecentralization.
     *
     * @param hCDecentralizationDTO the hCDecentralizationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hCDecentralizationDTO, or with status {@code 400 (Bad Request)} if the hCDecentralization has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hc-decentralizations")
    public ResponseEntity<HCDecentralizationDTO> createHCDecentralization(Principal principal,
                                                                          @RequestBody HCDecentralizationDTO hCDecentralizationDTO)
        throws URISyntaxException, IllegalAccessException, InstantiationException {
        log.debug("REST request to save HCDecentralization : {}", hCDecentralizationDTO);
        if (hCDecentralizationDTO.getId() != null) {
            throw new BadRequestAlertException("A new hCDecentralization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HCDecentralizationDTO result = hCDecentralizationService.createDecentralization(principal.getName(), hCDecentralizationDTO);
        return ResponseEntity
            .created(new URI("/api/hc-decentralizations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hc-decentralizations/:id} : Updates an existing hCDecentralization.
     *
     * @param id                    the id of the hCDecentralizationDTO to save.
     * @param hCDecentralizationDTO the hCDecentralizationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hCDecentralizationDTO,
     * or with status {@code 400 (Bad Request)} if the hCDecentralizationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hCDecentralizationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hc-decentralizations/{id}")
    public ResponseEntity<HCDecentralizationDTO> updateHCDecentralization(Principal principal,
                                                                          @PathVariable(value = "id", required = false) final Long id,
                                                                          @RequestBody HCDecentralizationDTO hCDecentralizationDTO
    ) throws URISyntaxException, IllegalAccessException, InstantiationException {
        log.debug("REST request to update HCDecentralization : {}, {}", id, hCDecentralizationDTO);
        if (hCDecentralizationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hCDecentralizationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hCDecentralizationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        HCDecentralizationDTO result = hCDecentralizationService.updateDecentralization(principal.getName(), hCDecentralizationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCDecentralizationDTO.getId().toString()))
            .body(result);
    }

    /*
     * todo find all decentralization by department.
     * */
    @GetMapping(value = "/hc-decentralization/{department}")
    public ResponseEntity<List<HCDecentralizationDTO>> getAllDecentralizationByDepartment(Principal principal,
                                                                                          @PathVariable(name = "department") HCDepartment hcDepartment) {

        List<HCDecentralizationDTO> hcDecentralizationDTOList = hCDecentralizationService.findAllByDepartment(hcDepartment);
        for (HCDecentralizationDTO hcDecentralizationDTO : hcDecentralizationDTOList) {
            UsersDTO bVH = userClient.getUserInfoById(hcDecentralizationDTO.getUserId());
            hcDecentralizationDTO.setUserInOnName(bVH.getFullName());
        }

        return ResponseEntity.ok().body(hcDecentralizationDTOList);
    }

    /*
    * todo find all hc decentralization of user.
    *  */
    @GetMapping(value = "/hc-decentralization-user/{userId}")
    public ResponseEntity<List<HCDecentralizationDTO>> getAllDecentralizationByDepartment(Principal principal,
                                                                                          @PathVariable(name = "userId") String userId) {

        List<HCDecentralizationDTO> hcDecentralizationDTOList = hCDecentralizationService.findAllByUserId(userId);
        HCDecentralizationDTO hcDecentralizationDTO = hcDecentralizationDTOList.stream().findFirst().get();
        UsersDTO bVH = userClient.getUserInfoById(hcDecentralizationDTO.getUserId());
        for (HCDecentralizationDTO item : hcDecentralizationDTOList) {
            item.setUserInOnName(bVH.getFullName());
        }

        return ResponseEntity.ok().body(hcDecentralizationDTOList);
    }

    /**
     * {@code GET  /hc-decentralizations} : get all the hCDecentralizations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hCDecentralizations in body.
     */
    @GetMapping("/hc-decentralizations")
    public List<HCDecentralizationDTO> getAllHCDecentralizations() {
        log.debug("REST request to get all HCDecentralizations");
        List<HCDecentralizationDTO> hcDecentralizationDTOList = hCDecentralizationService.findAll();
        hcDecentralizationDTOList.forEach(item -> {
            UsersDTO bVH = userClient.getUserInfoById(item.getUserId());
            item.setUserInOnName(bVH.getFullName());
        });
        return hcDecentralizationDTOList;
    }

    /**
     * {@code GET  /hc-decentralizations/:id} : get the "id" hCDecentralization.
     *
     * @param id the id of the hCDecentralizationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hCDecentralizationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hc-decentralizations/{id}")
    public ResponseEntity<HCDecentralizationDTO> getHCDecentralization(@PathVariable Long id) {
        log.debug("REST request to get HCDecentralization : {}", id);
        Optional<HCDecentralizationDTO> hCDecentralizationDTO = hCDecentralizationService.findOne(id);
        UsersDTO bVH = userClient.getUserInfoById(hCDecentralizationDTO.get().getUserId());
        hCDecentralizationDTO.get().setUserInOnName(bVH.getFullName());
        return ResponseUtil.wrapOrNotFound(hCDecentralizationDTO);
    }

    /**
     * {@code DELETE  /hc-decentralizations/:id} : delete the "id" hCDecentralization.
     *
     * @param id the id of the hCDecentralizationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hc-decentralizations/{id}")
    public ResponseEntity<Void> deleteHCDecentralization(@PathVariable Long id) {
        log.debug("REST request to delete HCDecentralization : {}", id);
        hCDecentralizationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

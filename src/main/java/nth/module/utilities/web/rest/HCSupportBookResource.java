package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import nth.lib.common.exception.BusinessException;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.ContactManagerClient;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.repository.HCSupportBookRepository;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.HCSupportBookService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing {@link nth.module.utilities.domain.HCSupportBook}.
 */
@RestController
@RequestMapping("/api")
public class HCSupportBookResource {

    private final Logger log = LoggerFactory.getLogger(HCSupportBookResource.class);

    private static final String ENTITY_NAME = "hCSupportBook";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HCSupportBookService hCSupportBookService;

    private final HCSupportBookRepository hCSupportBookRepository;

    private final UserClient userClient;

    private final HCDecentralizationService hcDecentralizationService;

    public HCSupportBookResource(HCSupportBookService hCSupportBookService, HCSupportBookRepository hCSupportBookRepository, ContactManagerClient contactManagerClient, UserClient userClient, HCDecentralizationService hcDecentralizationService) {
        this.hCSupportBookService = hCSupportBookService;
        this.hCSupportBookRepository = hCSupportBookRepository;
        this.userClient = userClient;
        this.hcDecentralizationService = hcDecentralizationService;
    }

    /**
     * {@code POST  /hc-support-books} : Create a new hCSupportBook.
     *
     * @param hCSupportBookDTO the hCSupportBookDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hCSupportBookDTO, or with status {@code 400 (Bad Request)} if the hCSupportBook has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hc-support-books")
    public ResponseEntity<HCSupportBookDTO> createHCSupportBook(Principal principal, HttpServletRequest request,
                                                                @RequestBody HCSupportBookDTO hCSupportBookDTO) throws URISyntaxException {
        log.debug("REST request to save HCSupportBook : {}", hCSupportBookDTO);
        if (hCSupportBookDTO.getId() != null) {
            throw new BadRequestAlertException("A new hCSupportBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String deviceId = request.getHeader("deviceid");
        hCSupportBookDTO.setDeviceId(deviceId);
        try {
            HCSupportBookDTO result = hCSupportBookService.createSupportBook(principal == null ? "Guest" : principal.getName(), hCSupportBookDTO);
            return ResponseEntity
                .created(new URI("/api/hc-support-books/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

    }

    //todo create support book public.
//    @PostMapping("/authenticate/hc-support-books")
//    public ResponseEntity<HCSupportBookDTO> createHCSupportBookPublic(@RequestBody HCSupportBookDTO hCSupportBookDTO, HttpServletRequest request) throws URISyntaxException {
//        log.debug("REST request to save HCSupportBook : {}", hCSupportBookDTO);
//        if (hCSupportBookDTO.getId() != null) {
//            throw new BadRequestAlertException("A new hCSupportBook cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        String deviceId = request.getHeader("deviceid");
//        hCSupportBookDTO.setDeviceId(deviceId);
//        HCSupportBookDTO result = hCSupportBookService.createSupportBook(null, hCSupportBookDTO);
//        return ResponseEntity
//            .created(new URI("/api/hc-support-books/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }

    /**
     * {@code PUT  /hc-support-books/:id} : Updates an existing hCSupportBook.
     *
     * @param id               the id of the hCSupportBookDTO to save.
     * @param hCSupportBookDTO the hCSupportBookDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hCSupportBookDTO,
     * or with status {@code 400 (Bad Request)} if the hCSupportBookDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hCSupportBookDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hc-support-books/{id}")
    public ResponseEntity<HCSupportBookDTO> updateHCSupportBook(Principal principal,
                                                                @PathVariable(value = "id", required = false) final Long id,
                                                                @RequestBody HCSupportBookDTO hCSupportBookDTO
    ) throws URISyntaxException, IllegalAccessException, InstantiationException, JsonProcessingException {
        log.debug("REST request to update HCSupportBook : {}, {}", id, hCSupportBookDTO);
        if (hCSupportBookDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hCSupportBookDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hCSupportBookRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        try {
            HCSupportBookDTO result = hCSupportBookService.updateSupportBook(principal == null ? "Guest" : principal.getName(), hCSupportBookDTO);
            return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCSupportBookDTO.getId().toString()))
                .body(result);
        } catch (BusinessException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

    //todo update support book public.
//    @PutMapping("/authenticate/hc-support-books/{id}")
//    public ResponseEntity<HCSupportBookDTO> updateHCSupportBookPublic(
//        @PathVariable(value = "id", required = false) final Long id,
//        @RequestBody HCSupportBookDTO hCSupportBookDTO
//    ) throws URISyntaxException, IllegalAccessException, InstantiationException, JsonProcessingException {
//        log.debug("REST request to update HCSupportBook : {}, {}", id, hCSupportBookDTO);
//        if (hCSupportBookDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        if (!Objects.equals(id, hCSupportBookDTO.getId())) {
//            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
//        }
//
//        if (!hCSupportBookRepository.existsById(id)) {
//            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
//        }
//
//        HCSupportBookDTO result = hCSupportBookService.updateSupportBook(null, hCSupportBookDTO);
//        return ResponseEntity
//            .ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hCSupportBookDTO.getId().toString()))
//            .body(result);
//    }


    /*
     * todo get all support book created by self.
     *  */

    /**
     * {@code GET  /hc-support-books} : get all the hCSupportBooks.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hCSupportBooks in body.
     */
    @GetMapping("/hc-support-books")
    public ResponseEntity<List<HCSupportBookDTO>> getAllHCSupportBooks(Principal principal,
                                                                       Pageable pageable) {
        log.debug("REST request to get all HCSupportBooks");
        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        List<HCSupportBookDTO> hcSupportBookDTOList = hCSupportBookService.findAllMyHCSupportBook(principal.getName(), pageable);
        hcSupportBookDTOList.forEach(hcSupportBookDTO -> {
            if (!hcSupportBookDTO.getCreatedBy().equals("Guest")) {
                UsersDTO usersDTO = userClient.getUserInfoById(hcSupportBookDTO.getCreatedBy());
                hcSupportBookDTO.setCreateByName(usersDTO.getFullName());
            }
        });
        return ResponseEntity.ok().body(hcSupportBookDTOList);
    }

    /*
     * todo find all my HC Support book mamanger process.
     *  */
    @GetMapping(value = "/hc-support-book/manager")
    public ResponseEntity<List<HCSupportBookDTO>> getAllMySupportBookManager(Principal principal, Pageable pageable) {

        log.debug("REST request to get all my HCSupportBook manager.");

        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        Optional<HCDecentralizationDTO> hcDecentralizationDTOOptional = hcDecentralizationService
            .findAllByUserId(principal.getName())
            .stream()
            .filter(item -> item.getDecentralization().equals(HCDecentralizationType.AD_OP))
            .findFirst();

        List<HCSupportBookDTO> hcSupportBookDTOList;
        if (hcDecentralizationDTOOptional.isPresent()) {
            hcSupportBookDTOList = hCSupportBookService.findAll(pageable);
            hcSupportBookDTOList.forEach(hcSupportBookDTO -> {
                if (!hcSupportBookDTO.getCreatedBy().equals("Guest")) {
                    UsersDTO usersDTO = userClient.getUserInfoById(hcSupportBookDTO.getCreatedBy());
                    hcSupportBookDTO.setCreateByName(usersDTO.getFullName());
                }
            });
        } else {
            hcSupportBookDTOList = hCSupportBookService.findAllMySupportBookManager(principal.getName(), pageable);
            hcSupportBookDTOList.forEach(hcSupportBookDTO -> {
                if (!hcSupportBookDTO.getCreatedBy().equals("Guest")) {
                    UsersDTO usersDTO = userClient.getUserInfoById(hcSupportBookDTO.getCreatedBy());
                    hcSupportBookDTO.setCreateByName(usersDTO.getFullName());
                }
            });
        }
        return ResponseEntity.ok().body(hcSupportBookDTOList);
    }

    /**
     * {@code GET  /hc-support-books/:id} : get the "id" hCSupportBook.
     *
     * @param id the id of the hCSupportBookDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hCSupportBookDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hc-support-books/{id}")
    public ResponseEntity<HCSupportBookDTO> getHCSupportBook(Principal principal, @PathVariable Long id) {
        log.debug("REST request to get HCSupportBook : {}", id);
        Optional<HCSupportBookDTO> hCSupportBookDTO = hCSupportBookService.findOne(principal.getName(), id);
        if (!hCSupportBookDTO.get().getCreatedBy().equals("Guest")) {
            UsersDTO usersDTO = userClient.getUserInfoById(hCSupportBookDTO.get().getCreatedBy());
            hCSupportBookDTO.get().setCreateByName(usersDTO.getFullName());
        }

        return ResponseUtil.wrapOrNotFound(hCSupportBookDTO);
    }

    /**
     * {@code DELETE  /hc-support-books/:id} : delete the "id" hCSupportBook.
     *
     * @param id the id of the hCSupportBookDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hc-support-books/{id}")
    public ResponseEntity<Void> deleteHCSupportBook(@PathVariable Long id) {
        log.debug("REST request to delete HCSupportBook : {}", id);
        hCSupportBookService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

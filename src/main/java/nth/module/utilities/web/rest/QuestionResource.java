package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import nth.lib.common.exception.BusinessException;
import nth.lib.integration.model.FileInfoDTO;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.FileClient;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import nth.module.utilities.repository.QuestionRepository;
import nth.module.utilities.service.CategoryQuestionService;
import nth.module.utilities.service.QuestionService;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.service.dto.QuestionDTO;
import nth.module.utilities.utils.RoleUtils;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link nth.module.utilities.domain.Question}.
 */
@RestController
@RequestMapping("/api")
public class QuestionResource {

    private final Logger log = LoggerFactory.getLogger(QuestionResource.class);

    private static final String ENTITY_NAME = "question";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuestionService questionService;
    private final QuestionRepository questionRepository;
    private final UserClient userClient;
    private final FileClient fileClient;
    private final CategoryQuestionService categoryQuestionService;

    public QuestionResource(QuestionService questionService, QuestionRepository questionRepository, UserClient userClient, FileClient fileClient, CategoryQuestionService categoryQuestionService) {
        this.questionService = questionService;
        this.questionRepository = questionRepository;
        this.userClient = userClient;
        this.fileClient = fileClient;
        this.categoryQuestionService = categoryQuestionService;
    }

    /**
     * {@code POST  /questions} : Create a new question.
     *
     * @param questionDTO the questionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new questionDTO, or with status {@code 400 (Bad Request)} if the question has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/questions")
    public ResponseEntity<QuestionDTO> createQuestion(Principal principal,
                                                      @RequestParam(name = "categoryQuestionType") CategoryQuestionType categoryQuestionType,
                                                      @RequestParam(name = "docType") String docType,
                                                      @RequestBody QuestionDTO questionDTO) throws URISyntaxException, IllegalAccessException, InstantiationException {
        log.debug("REST request to save Question : {}", questionDTO);
        if (questionDTO.getId() != null) {
            throw new BadRequestAlertException("A new question cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<QuestionDTO> questionDTOOptional = questionService
            .createQuestion(questionDTO, categoryQuestionType, principal.getName(), userClient);

        if (!questionDTOOptional.isPresent()) {
            throw new BusinessException("createQuestionError");
        }
        QuestionDTO result = questionDTOOptional.get();
        return ResponseEntity
            .created(new URI("/api/questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /questions/:id} : Updates an existing question.
     *
     * @param id          the id of the questionDTO to save.
     * @param questionDTO the questionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated questionDTO,
     * or with status {@code 400 (Bad Request)} if the questionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the questionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/questions")
    public ResponseEntity<QuestionDTO> updateQuestion(Principal principal,
                                                      @RequestParam(value = "id", required = false) final Long id,
                                                      @RequestParam(name = "categoryQuestionType") CategoryQuestionType categoryQuestionType,
                                                      @RequestBody QuestionDTO questionDTO
    ) throws URISyntaxException, IllegalAccessException, InstantiationException {
        log.debug("REST request to update Question : {}, {}", id, questionDTO);
        if (questionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, questionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!questionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        Optional<QuestionDTO> optionalQuestionDTO = questionService.updateQuestion(questionDTO, principal.getName(), userClient, categoryQuestionType);
        if (!optionalQuestionDTO.isPresent()) {
            throw new BusinessException("createQuestionError");
        }
        QuestionDTO result = optionalQuestionDTO.get();
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, questionDTO.getId().toString()))
            .body(result);
    }

    /*
    * Update status question.
    * */
    @PutMapping(value = "/questions-status")
    public ResponseEntity<?> updateStatusQuestion(Principal principal,
                                                  @RequestParam(name = "questionId") String id,
                                                  @RequestParam(name = "status")StatusQuestion statusQuestion,
                                                  @RequestBody HashMap<String, String> body) {

        String roleGroup = RoleUtils.getRoleGroup(userClient, principal.getName());
        QuestionDTO questionDTO = questionService.findOne(Long.parseLong(id))
            .orElseThrow(() -> new BusinessException("questionNotFound"));
        if (RoleUtils.isAdmin(roleGroup)) {
            questionDTO.setStatus(statusQuestion);
            if (statusQuestion.equals(StatusQuestion.APPROVALED)){
                questionDTO.setEnableDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
                questionDTO.setStatus(statusQuestion);
            }
            if (statusQuestion.equals(StatusQuestion.REJECTED)){
                questionDTO.setReasonReject(body.get("rejectReason"));
                questionDTO.setStatus(statusQuestion);
            }
            questionDTO = questionService.save(questionDTO);
        } else {
            throw new BusinessException("accessDenied");
        }
        return ResponseEntity.ok().body(questionDTO);
    }

    /**
     * {@code GET  /questions} : get all the questions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of questions in body.
     */
    @GetMapping("/authenticate/questions")
    public ResponseEntity<?> getAllQuestionsPublic(Principal principal, Pageable pageable,
                                                   @RequestParam(name = "categoryQuestionType", required = false) Optional<CategoryQuestionType> categoryQuestionType,
                                                   @RequestParam(name = "applyFor", required = false) Optional<ApplyFor> applyFor,
                                                   @RequestParam(name = "searchKey", required = false) Optional<String> seachKey) throws JsonProcessingException {
        log.debug("REST request to get all Questions");
        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        if (seachKey.isPresent()) {
            HashMap<String, Object> questionDTOList = new HashMap<>();
            if (principal == null) {
                questionDTOList = questionService.searchDocuments(seachKey.get(), pageable, Optional.empty());
            } else {
                questionDTOList = questionService.searchDocuments(seachKey.get(), pageable, Optional.of(principal.getName()));
            }
            return ResponseEntity.ok().body(questionDTOList);
        } else if (categoryQuestionType.isPresent() && applyFor.isPresent()){
            List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionService.findAllCategoryQuestionByType(categoryQuestionType.get(), pageable);
            List<QuestionDTO> questionDTOList = questionService
                .findAllByCategoryQuestionAndApplyFor(categoryQuestionDTOList, applyFor.get(), null, categoryQuestionType.get(),pageable);
            return ResponseEntity.ok().body(new PageImpl<>(questionDTOList));
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/questions")
    public ResponseEntity<Page<QuestionDTO>> getAllQuestions(Principal principal, Pageable pageable,
                                                             @RequestParam(name = "categoryQuestionType") CategoryQuestionType categoryQuestionType) throws JsonProcessingException {
        log.debug("REST request to get all Questions");
        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionService.findAllCategoryQuestionByType(categoryQuestionType, pageable);
        List<QuestionDTO> questionDTOList = questionService.findAllByCategoryQuestionAndApplyFor(categoryQuestionDTOList, ApplyFor.PARTNER, principal.getName(), categoryQuestionType,pageable);
        return ResponseEntity.ok().body(new PageImpl<>(questionDTOList));
    }

    /*
    * get all documents manager.
    * */
    @GetMapping(value = "/manager/questions")
    public ResponseEntity<?> getAllQuestionManager (Principal principal, Pageable pageable) {

        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        String roleGroup = RoleUtils.getRoleGroup(userClient, principal.getName());
        HashMap<String, Object> result = new HashMap<>();
        if (RoleUtils.isAdmin(roleGroup)){
            result = questionService.getAllQuestionManager(principal.getName(), true, pageable);
        }else if (RoleUtils.isOperate(roleGroup)) {
            result = questionService.getAllQuestionManager(principal.getName(), false,pageable);
        } else {
            throw new BusinessException("accessDenied");
        }
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /questions/:id} : get the "id" question.
     *
     * @param id the id of the questionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the questionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/questions/{id}")
    public ResponseEntity<QuestionDTO> getQuestion(@PathVariable Long id) {
        log.debug("REST request to get Question : {}", id);
        Optional<QuestionDTO> questionDTOOptional = questionService.findOne(id);
        if (!questionDTOOptional.isPresent()){
            throw new BusinessException("questionNotFound");
        }
        QuestionDTO questionDTO = questionDTOOptional.get();
        if (questionDTO.getUpdateBy() == null) {
            UsersDTO usersDTO = userClient.getUserInfoById(questionDTO.getCreatedBy());
            questionDTO.setCreatedBy(usersDTO.getFullName());
        } else {
            UsersDTO usersDTO = userClient.getUserInfoById(questionDTO.getUpdateBy());
            questionDTO.setUpdateBy(usersDTO.getFullName());
        }
        questionDTO.setResultText(new String(questionDTO.getRsText(), StandardCharsets.UTF_8));
        return ResponseUtil.wrapOrNotFound(Optional.of(questionDTO));
    }

    /**
     * {@code DELETE  /questions/:id} : delete the "id" question.
     *
     * @param id the id of the questionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/questions/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) {
        log.debug("REST request to delete Question : {}", id);
        questionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package nth.module.utilities.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.Objects;
import java.util.Optional;

import nth.module.utilities.client.UserClient;
import nth.module.utilities.repository.CategoryQuestionRepository;
import nth.module.utilities.service.CategoryQuestionService;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link nth.module.utilities.domain.CategoryQuestion}.
 */
@RestController
@RequestMapping("/api")
public class CategoryQuestionResource {

    private final Logger log = LoggerFactory.getLogger(CategoryQuestionResource.class);

    private static final String ENTITY_NAME = "categoryQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryQuestionService categoryQuestionService;
    private final CategoryQuestionRepository categoryQuestionRepository;
    private final UserClient userClient;

    public CategoryQuestionResource(
        CategoryQuestionService categoryQuestionService,
        CategoryQuestionRepository categoryQuestionRepository,
        UserClient userClient) {
        this.categoryQuestionService = categoryQuestionService;
        this.categoryQuestionRepository = categoryQuestionRepository;
        this.userClient = userClient;
    }

    /**
     * {@code POST  /category-questions} : Create a new categoryQuestion.
     *
     * @param categoryQuestionDTO the categoryQuestionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categoryQuestionDTO, or with status {@code 400 (Bad Request)} if the categoryQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/category-questions")
    public ResponseEntity<CategoryQuestionDTO> createCategoryQuestion(@RequestBody CategoryQuestionDTO categoryQuestionDTO)
        throws URISyntaxException {
        log.debug("REST request to save CategoryQuestion : {}", categoryQuestionDTO);
        if (categoryQuestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new categoryQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategoryQuestionDTO result = categoryQuestionService.save(categoryQuestionDTO);
        return ResponseEntity
            .created(new URI("/api/category-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /category-questions/:id} : Updates an existing categoryQuestion.
     *
     * @param id the id of the categoryQuestionDTO to save.
     * @param categoryQuestionDTO the categoryQuestionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categoryQuestionDTO,
     * or with status {@code 400 (Bad Request)} if the categoryQuestionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categoryQuestionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/category-questions/{id}")
    public ResponseEntity<CategoryQuestionDTO> updateCategoryQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CategoryQuestionDTO categoryQuestionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CategoryQuestion : {}, {}", id, categoryQuestionDTO);
        if (categoryQuestionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, categoryQuestionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!categoryQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CategoryQuestionDTO result = categoryQuestionService.save(categoryQuestionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, categoryQuestionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /category-questions} : get all the categoryQuestions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categoryQuestions in body.
     */
    @GetMapping("/authenticate/category-questions")
    public ResponseEntity<Page<CategoryQuestionDTO>> getAllCategoryQuestions(Principal principal,
                                                                             Pageable pageable) {
        pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdBy").ascending());
        log.debug("REST request to get all CategoryQuestions");
        Page<CategoryQuestionDTO> categoryQuestionDTOPage;
        if (principal == null) {
            categoryQuestionDTOPage = new PageImpl<>( categoryQuestionService
                .findAllCategoryQuestionByUserId(null, pageable, userClient));
        } else {
            categoryQuestionDTOPage = new PageImpl<>( categoryQuestionService
                .findAllCategoryQuestionByUserId(principal.getName(), pageable, userClient));
        }
        return ResponseEntity.ok().body(categoryQuestionDTOPage);
    }

    /**
     * {@code GET  /category-questions/:id} : get the "id" categoryQuestion.
     *
     * @param id the id of the categoryQuestionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categoryQuestionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/authenticate/category-questions/{id}")
    public ResponseEntity<CategoryQuestionDTO> getCategoryQuestion(Principal principal,
                                                                   @PathVariable Long id) {
        log.debug("REST request to get CategoryQuestion : {}", id);
        Optional<CategoryQuestionDTO> categoryQuestionDTO;
        if (principal == null) {
            categoryQuestionDTO = categoryQuestionService.findOneCategoryQuestionByUserId(null,id,userClient);
        } else {
            categoryQuestionDTO = categoryQuestionService.findOneCategoryQuestionByUserId(principal.getName(),id,userClient);
        }
        return ResponseUtil.wrapOrNotFound(categoryQuestionDTO);
    }

    /**
     * {@code DELETE  /category-questions/:id} : delete the "id" categoryQuestion.
     *
     * @param id the id of the categoryQuestionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/category-questions/{id}")
    public ResponseEntity<Void> deleteCategoryQuestion(@PathVariable Long id) {
        log.debug("REST request to delete CategoryQuestion : {}", id);
        categoryQuestionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

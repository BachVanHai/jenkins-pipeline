package nth.module.utilities.web.rest;

import nth.module.utilities.service.AppConfigSystemService;
import nth.module.utilities.service.dto.AppConfigSystemDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class AppConfigSystemResource {

    private final AppConfigSystemService appConfigSystemService;

    public AppConfigSystemResource(AppConfigSystemService appConfigSystemService) {
        this.appConfigSystemService = appConfigSystemService;
    }

    @GetMapping(value = "/authenticate/app-config-system/insuranceBuyEnable")
    public ResponseEntity<List<AppConfigSystemDTO>> getConfigInsuranceBuyEnabled() {
        List<AppConfigSystemDTO> appConfigSystemDTOS = appConfigSystemService.findAllByKeyAndEnable("insuranceBuyEnable", true);
        return ResponseEntity.ok().body(appConfigSystemDTOS);
    }

    @GetMapping(value = "/authenticate/app-config-system/tp-bank/insuranceBuyEnable")
    public ResponseEntity<List<AppConfigSystemDTO>> getTPBankInsuranceBuyEnabled() {
        List<AppConfigSystemDTO> appConfigSystemDTOS = appConfigSystemService.findAllByKeyAndEnable("TPInsuranceBuyEnable", true);
        return ResponseEntity.ok().body(appConfigSystemDTOS);
    }

    @GetMapping(value = "/app-config-system/{key}")
    public ResponseEntity<List<AppConfigSystemDTO>> getConfigByKey(@PathVariable String key) {
        List<AppConfigSystemDTO> appConfigSystemDTOS = appConfigSystemService.findAllByKeyAndEnable(key, true);
        return ResponseEntity.ok().body(appConfigSystemDTOS);
    }
}

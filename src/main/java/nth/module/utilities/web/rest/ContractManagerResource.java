package nth.module.utilities.web.rest;

import nth.lib.integration.model.ContractDTO;
import nth.module.utilities.service.ContractManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ContractManagerResource {

    private final Logger log = LoggerFactory.getLogger(ContractManagerResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContractManagerService contractManagerService;

    public ContractManagerResource(ContractManagerService contractManagerService) {
        this.contractManagerService = contractManagerService;
    }

    @GetMapping(value = "/contract-manager/get-all-elite-contracts")
    public ResponseEntity<List<ContractDTO>> getAllEliteContracts(@RequestHeader(name = "appId") String appId,
                                                                  @RequestParam(name = "fromDate")Instant fromDate,
                                                                  @RequestParam(name = "toDate") Instant toDate) {

        List<ContractDTO> response = contractManagerService.getAllEliteContracts(appId, fromDate, toDate);

        return ResponseEntity.ok().body(response);
    }
}

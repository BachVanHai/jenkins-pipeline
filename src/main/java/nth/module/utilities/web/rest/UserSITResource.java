package nth.module.utilities.web.rest;

import nth.lib.common.exception.BusinessException;
import nth.module.utilities.service.UserSITService;
import nth.module.utilities.service.dto.UserSITDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api")
public class UserSITResource {

    private final UserSITService userSITService;

    public UserSITResource(UserSITService userSITService) {
        this.userSITService = userSITService;
    }

    @GetMapping(value = "/authenticate/login-sit")
    public ResponseEntity<UserSITDTO> loginSIT(@RequestParam(name = "userName") String userName,
                                               @RequestParam(name = "password") String password,
                                               HttpServletRequest httpServletRequest) {

        String deviceId = httpServletRequest.getHeader("deviceid");
        Optional<UserSITDTO> userSITDTOOptional = userSITService.findFirstByUserNameAndPassword(userName, password,deviceId);
        if (userSITDTOOptional.isPresent() && !userSITDTOOptional.get().isEnable()) {
            return ResponseEntity.notFound().build();
        }
        return userSITDTOOptional.map(userSITDTO -> ResponseEntity.ok().body(userSITDTO)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/authenticate/userSIT")
    public ResponseEntity<UserSITDTO> getsUserSITByUsername(@RequestParam(name = "username") String username){
        Optional<UserSITDTO> userSITDTOOptional = userSITService.findFirstByUserName(username);
        if (!userSITDTOOptional.isPresent()){
            throw new BusinessException("userNotFound");
        }
        UserSITDTO userSITDTO = userSITDTOOptional.get();
        return ResponseEntity.ok(userSITDTO);
    }
}

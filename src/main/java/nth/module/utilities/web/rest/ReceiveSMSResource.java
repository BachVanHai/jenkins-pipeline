package nth.module.utilities.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class ReceiveSMSResource {

    private final Logger log = LoggerFactory.getLogger(ReceiveSMSResource.class);

    @PostMapping(value = "/authenticate/receive-sms")
    public ResponseEntity<?> receiveSMS (HttpServletRequest request) {


        log.info("Request : {}", request.toString());

        return ResponseEntity.ok().build();
    }

}

package nth.module.utilities.repository;

import nth.module.utilities.domain.HCUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data SQL repository for the HCUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HCUserRepository extends JpaRepository<HCUser, Long> {

    Optional<HCUser> findOneByNameAndEmail(String name, String email);

    Optional<HCUser> findOneByUserIdInOn(String userIdInOn);
}

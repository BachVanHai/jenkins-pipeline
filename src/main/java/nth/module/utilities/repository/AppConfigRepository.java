package nth.module.utilities.repository;

import nth.module.utilities.domain.AppConfigSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppConfigRepository extends JpaRepository<AppConfigSystem, Long> {
    List<AppConfigSystem> findAllByKeyAndEnable(String key, Boolean enable);
}

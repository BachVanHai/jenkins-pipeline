package nth.module.utilities.repository;

import nth.module.utilities.domain.HCSupportBook;
import nth.module.utilities.domain.HCUser;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.domain.enumeration.HCSupportType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data SQL repository for the HCSupportBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HCSupportBookRepository extends JpaRepository<HCSupportBook, Long> {

    List<HCSupportBook> findAllBySupporterInOnId(String supporterInOnId);

    List<HCSupportBook> findAllBySupporterInOnIdAndStatusIn(String supporterInOnId, List<HCStatus>  hcStatus);

    List<HCSupportBook> findAllByTypeIn(List<HCSupportType> hcSupportTypeList);

    Optional<HCSupportBook> findOneByRoomIdOrRoomInOnId (String roomId, String roomInOnId);

    List<HCSupportBook> findAllBySupporterInOnIdAndCreatedDateBetween(String supporterInOnId, Instant fromDate, Instant toDate);

    List<HCSupportBook> findAllByhCUserAndDeletedIsFalse(HCUser hcUser);

}

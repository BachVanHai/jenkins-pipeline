package nth.module.utilities.repository;

import nth.module.utilities.domain.UserSIT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface UserSITRepository extends JpaRepository<UserSIT, Long> {

    Optional<UserSIT> findFirstByUserNameAndPassword(String userName, String password);
    Optional<UserSIT> findFirstByUserName(String username);
}

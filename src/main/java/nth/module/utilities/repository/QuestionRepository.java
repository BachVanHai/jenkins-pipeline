package nth.module.utilities.repository;

import nth.module.utilities.domain.CategoryQuestion;
import nth.module.utilities.domain.Question;
import nth.module.utilities.domain.enumeration.ApplyFor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByCategoryQuestionAndApplyForIn(CategoryQuestion categoryQuestion, List<ApplyFor> applyFor, Pageable pageable);

    List<Question> findAllByCategoryQuestion(CategoryQuestion categoryQuestion,  Pageable pageable);

    @Query(value = "SELECT * FROM question WHERE question LIKE %?1% AND category_question_id = ?2 ORDER BY created_date DESC", nativeQuery = true)
    List<Question> findAllByQuestionLikeAndCategoryQuestion(String question, String categoryQuestionId);
}

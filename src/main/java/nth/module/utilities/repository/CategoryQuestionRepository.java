package nth.module.utilities.repository;

import nth.module.utilities.domain.CategoryQuestion;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the CategoryQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryQuestionRepository extends JpaRepository<CategoryQuestion, Long> {

    List<CategoryQuestion> findAllByCategoryQuestionType(CategoryQuestionType categoryQuestionType, Pageable pageable);
}

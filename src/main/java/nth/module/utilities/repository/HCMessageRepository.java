package nth.module.utilities.repository;

import nth.module.utilities.domain.HCMessage;
import nth.module.utilities.domain.HCSupportBook;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the HCMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HCMessageRepository extends JpaRepository<HCMessage, Long> {

    List<HCMessage> findAllByRoomId(String roomId, Pageable pageable);

    List<HCMessage> findAllByRoomIdAndReceiveDateIsNull(String roomId);

    @Query(value = "SELECT * FROM hc_message WHERE hc_message.hcsupport_book_id = ?1", nativeQuery = true)
    List<HCMessage> findAllByHCSupportBook(String supportBookId);
}

package nth.module.utilities.repository;

import nth.module.utilities.domain.ContractTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface ContractTempRepository extends JpaRepository<ContractTemp, Long> {
    Optional<ContractTemp> findAllByCode(String code);
}

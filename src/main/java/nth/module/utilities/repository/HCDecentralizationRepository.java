package nth.module.utilities.repository;

import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCSupportType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data SQL repository for the HCDecentralization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HCDecentralizationRepository extends JpaRepository<HCDecentralization, Long> {

    List<HCDecentralization> findAllByUserId(String userId);

    List<HCDecentralization> findAllByDepartment(HCDepartment hcDepartment);

    Optional<HCDecentralization> findFirstByUserIdAndDepartment(String userId, HCDepartment hcDepartment);

    Optional<HCDecentralization> findFirstByDepartmentAndDecentralizationIn(HCDepartment hcDepartment, List<HCDecentralizationType> hcDecentralizationTypes);
}

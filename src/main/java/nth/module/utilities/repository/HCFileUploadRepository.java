package nth.module.utilities.repository;

import nth.module.utilities.domain.HCFileUpload;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the HCFileUpload entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HCFileUploadRepository extends JpaRepository<HCFileUpload, Long> {

    @Query(value = "SELECT * FROM hc_file_upload WHERE hc_file_upload.hcmessage_id = ?1", nativeQuery = true)
    List<HCFileUpload> findAllByHCMessageId(String hcMessageId);
}

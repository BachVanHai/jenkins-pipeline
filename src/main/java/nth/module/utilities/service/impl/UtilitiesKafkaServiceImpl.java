package nth.module.utilities.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import nth.lib.common.exception.BusinessException;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.enumeration.InsuranceCode;
import nth.lib.integration.enumeration.MailTemplate;
import nth.lib.integration.model.ContractDTO;
import nth.lib.integration.model.InsuranceDTO;
import nth.lib.integration.model.SendEmailDTO;
import nth.lib.integration.model.notification.ExpiredVehicleContract;
import nth.lib.integration.model.utilities.NotificationsDTO;
import nth.lib.integration.model.zns.ZNSVehicleContractExpiredRequest;
import nth.module.utilities.client.NotificationsClient;
import nth.module.utilities.client.VehicleInsuranceClient;
import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.repository.HCDecentralizationRepository;
import nth.module.utilities.security.jwt.TokenProvider;
import nth.module.utilities.service.*;
import nth.module.utilities.service.dto.ContractTempDTO;
import nth.module.utilities.service.dto.HCMessageDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.utils.DateTimeUtils;
import nth.module.utilities.utils.GenerateCodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UtilitiesKafkaServiceImpl {

    private final Logger log = LoggerFactory.getLogger(UtilitiesKafkaServiceImpl.class);

    private final HCDecentralizationRepository hcDecentralizationRepository;
    private final NotificationsClient notificationsClient;
    private final HCSupportBookService hcSupportBookService;
    private final HCMessageService hcMessageService;
    private final VehicleInsuranceClient vehicleInsuranceClient;
    private final TokenProvider tokenProvider;
    private final ContractTempService contractTempService;
    private final UtilitiesContractService utilitiesContractService;
    private final AppConfigSystemService appConfigSystemService;

    final ObjectMapper objectMapper = new ObjectMapper();

    public UtilitiesKafkaServiceImpl(HCDecentralizationRepository hcDecentralizationRepository, NotificationsClient notificationsClient, HCSupportBookService hcSupportBookService, HCMessageService hcMessageService, VehicleInsuranceClient vehicleInsuranceClient, TokenProvider tokenProvider, ContractTempService contractTempService, UtilitiesContractService utilitiesContractService, AppConfigSystemService appConfigSystemService) {
        this.hcDecentralizationRepository = hcDecentralizationRepository;
        this.notificationsClient = notificationsClient;
        this.hcSupportBookService = hcSupportBookService;
        this.hcMessageService = hcMessageService;
        this.vehicleInsuranceClient = vehicleInsuranceClient;
        this.tokenProvider = tokenProvider;
        this.contractTempService = contractTempService;
        this.utilitiesContractService = utilitiesContractService;
        this.appConfigSystemService = appConfigSystemService;
    }


    //    @KafkaListener(topics = "CHECK_NEW_HC_MESSAGE")
    public void sendNotificationNewHCMessage() {
        List<HCDecentralization> hcDecentralizationsList = hcDecentralizationRepository.findAll();
        Date startDate = Date.from(Instant.from(Instant.now().atZone(ZoneId.systemDefault()).minus(30, ChronoUnit.DAYS)));
        Date endDate = Date.from(Instant.from(Instant.now().atZone(ZoneId.systemDefault())));

        Instant fromDate = DateTimeUtils.atStartOfDay(startDate).toInstant();
        Instant todate = DateTimeUtils.atEndOfDay(endDate).toInstant();

        for (HCDecentralization hcDecentralization : hcDecentralizationsList) {

            List<HCSupportBookDTO> hcSupportBookDTOList = hcSupportBookService
                .findAllBySupportInOnIdMonth(hcDecentralization.getUserId(), fromDate, todate);

            long totalMessage = 0;

            for (HCSupportBookDTO hcSupportBookDTO : hcSupportBookDTOList) {
                List<HCMessageDTO> hcMessageDTOList = hcSupportBookDTO.getHcMessageDTOList()
                    .stream()
                    .filter(item -> (item.getReaderId() == null || !item.getReaderId().contains(hcSupportBookDTO.getSupporterInOnId()))
                        && Instant.now().minus(5, ChronoUnit.MINUTES).compareTo(item.getSendDate()) >= 0
                        && item.getNotification() <= 5
                        && Instant.now().minus(1, ChronoUnit.DAYS).compareTo(item.getUpdatedDate()) > 0)
                    .collect(Collectors.toList());

                for (HCMessageDTO hcMessageDTO : hcMessageDTOList) {
                    hcMessageDTO.setUpdatedDate(Instant.now());
                    hcMessageDTO.setNotification(hcMessageDTO.getNotification() + 1);
                    hcMessageService.save(hcMessageDTO);
                }

                if (hcMessageDTOList.size() > 0) {
                    totalMessage += hcMessageDTOList.size();
                }

            }

            if (totalMessage > 0) {
                NotificationsDTO notificationsDTO = new NotificationsDTO();
                notificationsDTO.setTitle("Bạn có tin nhắn mới.");
                notificationsDTO.setShortContent("Bạn có tin nhắn mới.");
                notificationsDTO.setContent(("Bạn có " + totalMessage + " tin nhắn mới.").getBytes());
                notificationsDTO.setUserId(Long.parseLong(hcDecentralization.getUserId()));
                notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);
            }
        }

    }

//    @KafkaListener(topics = "CHECK_CONTRACT_EXPIRED")
    @Async
    public void sentNotificationExpiredContract() throws IOException, ParseException {

        SimpleDateFormat HHmmddMMyyyy = new SimpleDateFormat("HH'h'mm'p' 'ngày' dd 'tháng' MM 'năm' yyyy");
        List<ContractDTO> contractDTOS = utilitiesContractService.finAllExpiredContract();

        for (ContractDTO contractDTO : contractDTOS) {

            try {
                ContractTempDTO contractTempDTO = new ContractTempDTO();
                contractTempDTO.setCode("CT" + System.currentTimeMillis());
                ContractDTO contractDTO1 = new ContractDTO();
                contractDTO1.setId(contractDTO.getId());
                contractTempDTO.setContractDescription(objectMapper.writeValueAsString(contractDTO1));
                contractTempDTO = contractTempService.save(contractTempDTO);
                SendEmailDTO sendEmailDTO = new SendEmailDTO();
                ExpiredVehicleContract expiredVehicleContract = new ExpiredVehicleContract();
                expiredVehicleContract.setLinkRenewalContract("https://apidev.inon.vn/insurance-renewal/" + contractTempDTO.getCode());
                expiredVehicleContract.setGreeting(contractDTO.getOwner().getFullName());
                expiredVehicleContract.setContractCode(contractDTO.getContractCode());
                expiredVehicleContract.setCustomerName(contractDTO.getOwner().getFullName());
                switch (contractDTO.getContractType()) {
                    case CC: {
                        expiredVehicleContract.setInsuranceType("Bảo hiểm xe ô tô");
                        sendEmailDTO.setTemplate(MailTemplate.INON_EXPIRED_VEHICLE_CONTRACT);
                        break;
                    }
                    case MC: {
                        expiredVehicleContract.setInsuranceType("Bảo hiểm xe máy");
                        sendEmailDTO.setTemplate(MailTemplate.INON_EXPIRED_VEHICLE_CONTRACT);
                        break;
                    }
                    case BC: {
                        expiredVehicleContract.setInsuranceType("sản phẩm bảo hiểm BEST CHOICE");
                        sendEmailDTO.setTemplate(MailTemplate.INON_EXPIRED_PERSONAL_CONTRACT);
                        break;
                    }
                    case FH: {
                        expiredVehicleContract.setInsuranceType("Bảo hiểm nhà tư nhân");
                        sendEmailDTO.setTemplate(MailTemplate.INON_EXPIRED_HOUSE_CONTRACT);
                        break;
                    }
                }
                switch (contractDTO.getCompanyId()) {
                    case "01": {
                        expiredVehicleContract.setInsuranceCompany("Công ty bảo hiểm BSH");
                        break;
                    }
                    case "02": {
                        expiredVehicleContract.setInsuranceCompany("Bảo hiểm ViettinBank");
                        break;
                    }
                    case "03": {
                        expiredVehicleContract.setInsuranceCompany("Bảo hiểm Hàng Không");
                        break;
                    }
                    case "04": {
                        expiredVehicleContract.setInsuranceCompany("Bảo hiểm Xuân Thành");
                        break;
                    }
                    case "06": {
                        expiredVehicleContract.setInsuranceCompany("Bảo hiểm Dầu Khí");
                        break;
                    }
                }
                InsuranceDTO insuranceEnable = null;
                for (InsuranceDTO insurance : contractDTO.getInsurances()) {
                    if (insurance.getStartValueDate() != null && insurance.getEndValueDate() != null) {
                        insuranceEnable = insurance;
                        break;
                    }
                }
                String emailDescriptionPattern = this.mapInsuranceWithTemplate(new ArrayList<>(contractDTO.getInsurances()), false, contractDTO.getContractType());
                String zaloDescriptionPattern = this.mapInsuranceWithTemplate(new ArrayList<>(contractDTO.getInsurances()), true, contractDTO.getContractType());

                if (emailDescriptionPattern.isEmpty() || zaloDescriptionPattern.isEmpty()) {
                    throw new BusinessException("insuranceConfigNotFound");
                }

                String emailDescription = emailDescriptionPattern.split("-")[0].trim();

                String zaloDescription = zaloDescriptionPattern.split("-")[0].trim();
                String zaloVehicleTemplate = zaloDescriptionPattern.split("-")[1].trim();
                if (insuranceEnable == null) {
                    throw new BusinessException("insuranceConfigNotFound");
                }
                expiredVehicleContract.setStartValueDate(HHmmddMMyyyy.format(Date.from(insuranceEnable.getStartValueDate())));
                expiredVehicleContract.setEndValueDate(HHmmddMMyyyy.format(Date.from(insuranceEnable.getEndValueDate())));
                expiredVehicleContract.setTotalFee(String.format("%1$,.0f", new BigDecimal(contractDTO.getTotalFeeInclVAT().intValue() + "")));
                expiredVehicleContract.setContractStatus("Thành công");
                expiredVehicleContract.setGreeting(contractDTO.getOwner().getFullName());
                expiredVehicleContract.setTemplateDescription(emailDescription);

                sendEmailDTO.setReceiverMail(contractDTO.getOwner().getEmail());
                sendEmailDTO.setMailVariables(expiredVehicleContract);
                sendEmailDTO.setSubject("(InOn) Thông báo bảo hiểm sắp hết hạn");
                this.tokenProvider.setFakeAuthentication();
                this.notificationsClient.sendExpiredContractNotification(sendEmailDTO);
                String templateId = "";
                if (zaloVehicleTemplate.equals("VC")) {
                    templateId = appConfigSystemService.findAllByKeyAndEnable("znsVehicleExpiredTemplateIdVc", true)
                        .stream().findFirst().orElseThrow(() -> new BusinessException("appConfigSystem")).getValue();
                } else if (zaloVehicleTemplate.equals("TNDS")) {
                    templateId = appConfigSystemService.findAllByKeyAndEnable("znsVehicleExpiredTemplateIdVc", true)
                        .stream().findFirst().orElseThrow(() -> new BusinessException("appConfigSystem")).getValue();
                }
                this.sentZaloNotification(contractDTO, Integer.parseInt(templateId), contractTempDTO.getCode(), HHmmddMMyyyy.format(Date.from(insuranceEnable.getEndValueDate())), zaloDescription);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }

    }

    private void sentZaloNotification(ContractDTO contractDTO, int zaloTemplateId, String renewalVehicleContractCode, String expiredContract, String templateDescription) {

        ZNSVehicleContractExpiredRequest znsVehicleContractExpiredRequest = new ZNSVehicleContractExpiredRequest();
        znsVehicleContractExpiredRequest.setPhone(contractDTO.getOwner().getPhoneNumber());
        znsVehicleContractExpiredRequest.setTemplateId(zaloTemplateId + "");
        znsVehicleContractExpiredRequest.setTrackingId(GenerateCodeUtils.generateCode(16));

        ZNSVehicleContractExpiredRequest.TemplateData templateData = new ZNSVehicleContractExpiredRequest.TemplateData();
        templateData.setCustomerName(contractDTO.getOwner().getFullName());
        templateData.setContractCode(contractDTO.getContractCode());
        templateData.setExprireDate(expiredContract);
        templateData.setInsuranceType(templateDescription);
        String insuranceCompanyName = "";
        switch (contractDTO.getInsurCompanyName()) {
            case "BSH": {
                insuranceCompanyName = "Bảo hiểm BSH";
                break;
            }
            case "XTI": {
                insuranceCompanyName = "Bảo hiểm Xuân Thành";
                break;
            }
            case "PVI": {
                insuranceCompanyName = "Bảo hiểm Dầu Khí";
                break;
            }
        }
        templateData.setInsuranceCompany(insuranceCompanyName);
        templateData.setNumberPlate(contractDTO.getVehicles().stream().findFirst()
            .orElseThrow(() -> new BusinessException("vehicleNotFound")).getNumberPlate());
        templateData.setTotalFee(String.format("%1$,.0f", new BigDecimal(contractDTO.getTotalFeeInclVAT().intValue() + "")));
        templateData.setCode(renewalVehicleContractCode);

        this.tokenProvider.setFakeAuthentication();
        notificationsClient.sentZnsVehicleExpired(znsVehicleContractExpiredRequest);
    }

    private String mapInsuranceWithTemplate(List<InsuranceDTO> insuranceDTOS, boolean isZaloNotification, ContractType contractType) {

        if (contractType.equals(ContractType.HC) && isZaloNotification) {
            return "Để đảm bảo quyền lợi , Quý khách có thể gia hạn hợp đồng bảo hiểm ngay.";
        }
        if (contractType.equals(ContractType.HC) && !isZaloNotification) {
            return "Quý khách hãy tham gia ngay để đảm bảo quyền lợi bảo hiểm không bị gián đoạn.";
        }
        boolean vcEnable = false;
        boolean tndsEnable = false;
        for (InsuranceDTO insuranceDTO : insuranceDTOS) {

            if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_TNDS) || insuranceDTO.getInsuranceCode().equals(InsuranceCode.MOTOR_TNDS)) {
                tndsEnable = true;
            }

            if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_VATCHAT)) {
                vcEnable = true;
            }
        }

        if (tndsEnable && !vcEnable && isZaloNotification) {
            return "Để đảm bảo tuân thủ quy định của Pháp luật khi tham gia giao thông, Quý khách có thể gia hạn hợp đồng bảo hiểm ngay.-TNDS";
        } else if (tndsEnable && !vcEnable && !isZaloNotification) {
            return "Quý khách hãy tham gia ngay để đảm bảo tuân thủ đúng quy định của pháp luật khi tham gia giao thông.-TNDS";
        } else if (!tndsEnable && vcEnable && isZaloNotification) {
            return "Để đảm bảo quyền lợi về xe của Quý khách trước những tổn thất, rủi ro khi di chuyển , Quý khách có thể gia hạn hợp đồng bảo hiểm ngay.-VC";
        } else if (!tndsEnable && vcEnable && !isZaloNotification) {
            return "Quý khách hãy tham gia ngay để đảm bảo quyền lợi về xe của Quý khách trước những tổn thất, rủi ro khi di chuyển.-VC";
        } else if (tndsEnable && vcEnable && isZaloNotification) {
            return "Để đảm bảo tuân thủ quy định của Pháp luật và  quyền lợi về xe của Quý khách trước những tổn thất, rủi ro khi tham gia giao thông, Quý khách có thể gia hạn hợp đồng bảo hiểm ngay.-VC";
        } else if (tndsEnable && vcEnable && !isZaloNotification) {
            return "Quý khách hãy tham gia ngay để đảm bảo tuân thủ quy định của Pháp luật và  quyền lợi về xe của Quý khách trước những tổn thất, rủi ro khi tham gia giao thông.-VC";
        }

        return "";
    }

}

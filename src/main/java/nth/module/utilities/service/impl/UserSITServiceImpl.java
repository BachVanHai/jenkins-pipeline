package nth.module.utilities.service.impl;

import nth.module.utilities.domain.UserSIT;
import nth.module.utilities.repository.UserSITRepository;
import nth.module.utilities.service.UserSITService;
import nth.module.utilities.service.dto.UserSITDTO;
import nth.module.utilities.service.mapper.UserSITMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@Transactional
public class UserSITServiceImpl implements UserSITService {

    private final UserSITRepository userSITRepository;
    private final UserSITMapper userSITMapper;

    public UserSITServiceImpl(UserSITRepository userSITRepository, UserSITMapper userSITMapper) {
        this.userSITRepository = userSITRepository;
        this.userSITMapper = userSITMapper;
    }


    @Override
    public Optional<UserSITDTO> findFirstByUserNameAndPassword(String userName, String password, String deviceId) {
        if (userName == null || userName.isEmpty() || password == null || password.isEmpty()){
            return Optional.empty();
        }
        Optional<UserSITDTO> userSITDTOOptional = userSITRepository
            .findFirstByUserNameAndPassword(userName, password)
            .map(userSITMapper::toDto);

        if (userSITDTOOptional.isPresent()){
            UserSITDTO userSITDTO = userSITDTOOptional.get();
            userSITDTO.setLastLoginSuccess(Instant.now());
            userSITDTO.setLastLoginSuccessDeviceId(deviceId);
            UserSIT userSIT = userSITMapper.toEntity(userSITDTO);
            userSITDTO = userSITMapper.toDto(userSITRepository.save(userSIT));
            return Optional.of(userSITDTO);
        }

        return userSITDTOOptional;
    }

    @Override
    public Optional<UserSITDTO> findFirstByUserName(String username) {
        return userSITRepository.findFirstByUserName(username).map(userSITMapper::toDto);
    }
}

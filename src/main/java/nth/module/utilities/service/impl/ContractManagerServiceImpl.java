package nth.module.utilities.service.impl;

import nth.lib.integration.model.ContractDTO;
import nth.module.utilities.client.HomeInsuranceClient;
import nth.module.utilities.client.PersonalInsuranceClient;
import nth.module.utilities.client.VehicleInsuranceClient;
import nth.module.utilities.service.ContractManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@Transactional
public class ContractManagerServiceImpl implements ContractManagerService {

    private final HomeInsuranceClient homeInsuranceClient;
    private final VehicleInsuranceClient vehicleInsuranceClient;
    private final PersonalInsuranceClient personalInsuranceClient;

    public ContractManagerServiceImpl(HomeInsuranceClient homeInsuranceClient, VehicleInsuranceClient vehicleInsuranceClient, PersonalInsuranceClient personalInsuranceClient) {
        this.homeInsuranceClient = homeInsuranceClient;
        this.vehicleInsuranceClient = vehicleInsuranceClient;
        this.personalInsuranceClient = personalInsuranceClient;
    }

    @Override
    public List<ContractDTO> getAllEliteContracts(String appId, Instant fromDate, Instant toDate) {

        List<ContractDTO> response = vehicleInsuranceClient.getAllEliteContract(appId, fromDate, toDate);
        response.addAll(homeInsuranceClient.getAllEliteContract(appId, fromDate, toDate));
        response.addAll(personalInsuranceClient.getAllEliteContract(appId, fromDate, toDate));
        return response;
    }
}

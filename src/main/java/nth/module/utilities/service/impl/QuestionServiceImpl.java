package nth.module.utilities.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nth.lib.common.exception.BusinessException;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.CategoryQuestion;
import nth.module.utilities.domain.Question;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import nth.module.utilities.repository.QuestionRepository;
import nth.module.utilities.service.CategoryQuestionService;
import nth.module.utilities.service.QuestionService;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.service.dto.QuestionDTO;
import nth.module.utilities.service.mapper.CategoryQuestionMapper;
import nth.module.utilities.service.mapper.QuestionMapper;
import nth.module.utilities.utils.GenerateCodeQuestion;
import nth.module.utilities.utils.ObjectUtils;
import nth.module.utilities.utils.RoleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Question}.
 */
@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionServiceImpl.class);

    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper;
    private final CategoryQuestionService categoryQuestionService;
    private final CategoryQuestionMapper categoryQuestionMapper;
    private final ObjectMapper objectMapper;
    private final UserClient userClient;

    @Value("${app-configs.upload-dir-questions}")
    public String uploadDirQuestions;

    @Value("${app-configs.upload-dir-manuals}")
    public String uploadDirManuals;

    @Value("${app-configs.upload-dir-documents}")
    public String uploadDirDocuments;

    public QuestionServiceImpl(QuestionRepository questionRepository, QuestionMapper questionMapper, CategoryQuestionService categoryQuestionService, CategoryQuestionMapper categoryQuestionMapper, ObjectMapper objectMapper, UserClient userClient) {
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
        this.categoryQuestionService = categoryQuestionService;
        this.categoryQuestionMapper = categoryQuestionMapper;
        this.objectMapper = objectMapper;
        this.userClient = userClient;
    }

    @Override
    public QuestionDTO save(QuestionDTO questionDTO) {
        log.debug("Request to save Question : {}", questionDTO);
        Question question = questionMapper.toEntity(questionDTO);
        question = questionRepository.save(question);
        return questionMapper.toDto(question);
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionDTO> findAll() {
        log.debug("Request to get all Questions");
        return questionRepository.findAll().stream().map(questionMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<QuestionDTO> findOne(Long id) {
        log.debug("Request to get Question : {}", id);
        QuestionDTO questionDTO = questionRepository
            .findById(id).map(questionMapper::toDto)
            .orElseThrow(() -> new BusinessException("questionNotFound"));
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionService
            .findOne(questionDTO.getCategoryQuestionId())
            .orElseThrow(() -> new BusinessException("categoryQuestionNotFound"));
        questionDTO.setCategoryQuestion(categoryQuestionDTO);
        return Optional.of(questionDTO);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Question : {}", id);
        QuestionDTO questionDTO = this.findOne(id)
            .orElseThrow(() -> new BusinessException("questionNotFound"));
        questionDTO.setDeleted(true);
        this.save(questionDTO);
    }

    @Override
    public Optional<QuestionDTO> createQuestion(QuestionDTO questionDTO, CategoryQuestionType categoryQuestionType, String userId, UserClient userClient) throws InstantiationException, IllegalAccessException {
        Optional<CategoryQuestionDTO> categoryQuestionDTOOptional = Optional.of(new CategoryQuestionDTO());
        Long categoryQuestionId = null;
        if (questionDTO.getCategoryQuestionName() != null) {
            CategoryQuestionDTO categoryQuestionDTO = categoryQuestionDTOOptional.get();
            categoryQuestionDTO.setName(questionDTO.getCategoryQuestionName());
            categoryQuestionDTO.setCreatedBy(userId);
            categoryQuestionDTO.setCategoryQuestionType(categoryQuestionType);
            categoryQuestionDTO.setCreatedDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
            categoryQuestionDTO = categoryQuestionService.save(categoryQuestionDTO);
            categoryQuestionId = categoryQuestionDTO.getId();
        }
        QuestionDTO question = ObjectUtils.cloneObject(questionDTO);
        if (categoryQuestionId != null) {
            question.setCategoryQuestionId(categoryQuestionId);
        }
        question.setRsText(question.getResultText().getBytes());
        question.setCreatedBy(userId);
        question.setCategoryQuestionType(categoryQuestionType);
        question.setCreatedDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        question = this.save(question);
        question.setCode(GenerateCodeQuestion.generateCodeQuestion(question, categoryQuestionType));
        String roleGroup = RoleUtils.getRoleGroup(userClient, userId);
        if (RoleUtils.isOperate(roleGroup)) {
            if (question.getStatus() == null) {
                question.setStatus(StatusQuestion.WAITTING_APPROVAL);
            }
        } else if (RoleUtils.isAdmin(roleGroup)) {
            if (question.getStatus() == null) {
                question.setStatus(StatusQuestion.APPROVALED);
                question.setEnableDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
            }
        }
        question = this.save(question);
        question.setResultText(new String(question.getRsText(), StandardCharsets.UTF_8));
        return Optional.of(question);
    }

    @Override
    public Optional<QuestionDTO> updateQuestion(QuestionDTO questionDTO, String userId, UserClient userClient, CategoryQuestionType categoryQuestionType) throws InstantiationException, IllegalAccessException {
        Optional<QuestionDTO> oldQuestionDTOOptional = this.findOne(questionDTO.getId());
        String roleGroup = RoleUtils.getRoleGroup(userClient, userId);
        Optional<CategoryQuestionDTO> categoryQuestionDTOOptional = Optional.of(new CategoryQuestionDTO());
        Long categoryQuestionId = null;
        if (questionDTO.getCategoryQuestionName() != null) {
            CategoryQuestionDTO categoryQuestionDTO = categoryQuestionDTOOptional.get();
            categoryQuestionDTO.setName(questionDTO.getCategoryQuestionName());
            categoryQuestionDTO.setCreatedBy(userId);
            categoryQuestionDTO.setCategoryQuestionType(categoryQuestionType);
            categoryQuestionDTO.setCreatedDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
            categoryQuestionDTO = categoryQuestionService.save(categoryQuestionDTO);
            categoryQuestionId = categoryQuestionDTO.getId();
        }
        if (categoryQuestionId != null) {
            questionDTO.setCategoryQuestionId(categoryQuestionId);
        }

        if (!oldQuestionDTOOptional.isPresent()) {
            QuestionDTO newQuestion = this.save(questionDTO);
            return Optional.of(newQuestion);
        }
        questionDTO.setRsText(questionDTO.getResultText().getBytes());
        ObjectUtils.mergeObjects(questionDTO, oldQuestionDTOOptional.get());
        questionDTO.setUpdateBy(userId);
        questionDTO.setUpdateDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        if (RoleUtils.isAdmin(roleGroup) || RoleUtils.isSPKDInOn(roleGroup)) {
            if (questionDTO.getStatus() == null) {
                questionDTO.setStatus(StatusQuestion.APPROVALED);
            }
        }
        if (RoleUtils.isOperate(roleGroup)) {
            if (questionDTO.getStatus() == null) {
                questionDTO.setStatus(StatusQuestion.WAITTING_APPROVAL);
            }
        }
        questionDTO = this.save(questionDTO);
        questionDTO.setResultText(new String(questionDTO.getRsText(), StandardCharsets.UTF_8));
        return Optional.of(questionDTO);
    }

    @Override
    public List<QuestionDTO> findAllByCategoryQuestionAndApplyFor(List<CategoryQuestionDTO> categoryQuestionDTOList, ApplyFor applyFor, String userId,CategoryQuestionType categoryQuestionType ,Pageable pageable) throws JsonProcessingException {
        List<ApplyFor> applyFors = Arrays.asList(ApplyFor.ALL, applyFor);
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        for (CategoryQuestionDTO categoryQuestionDTO : categoryQuestionDTOList) {
            CategoryQuestion categoryQuestion = categoryQuestionMapper.toEntity(categoryQuestionDTO);
            questionDTOList.addAll(questionRepository.findAllByCategoryQuestionAndApplyForIn(categoryQuestion, applyFors, pageable)
                .stream()
                .map(questionMapper::toDto)
                .filter(item -> !item.getDeleted() && item.getStatus().equals(StatusQuestion.APPROVALED) && item.getCategoryQuestionType().equals(categoryQuestionType))
                .collect(Collectors.toCollection(LinkedList::new)));
            questionDTOList.forEach(item -> {
                item.setResultText(new String(item.getRsText(), StandardCharsets.UTF_8));
                if (userId != null) {
                    if (item.getUpdateBy() == null) {
                        UsersDTO usersDTO = userClient.getUserInfoById(item.getCreatedBy());
                        item.setCreatedBy(usersDTO.getFullName());
                    } else {
                        UsersDTO usersDTO = userClient.getUserInfoById(item.getUpdateBy());
                        item.setUpdateBy(usersDTO.getFullName());
                    }
                }
            });
        }
        if (userId != null) {
            List<QuestionDTO> questionIdTmp = new ArrayList<>();
            for (QuestionDTO questionDTO : questionDTOList) {
                if (questionDTO.getSendTo() != null) {
                    String[] sendToList = questionDTO.getSendTo().split(",");
                    String roleGroup = RoleUtils.getRoleGroup(userClient, userId);
                    if (!Arrays.asList(sendToList).contains(roleGroup)) {
                        questionIdTmp.add(questionDTO);
                    }
                }
            }
            for (QuestionDTO item : questionIdTmp) {
                questionDTOList.remove(item);
            }
        }
        return questionDTOList;
    }

    @Override
    public HashMap<String, Object> searchDocuments(String searchKey, Pageable pageable, Optional<String> userId) {
        List<ApplyFor> applyFors = Arrays.asList(ApplyFor.ALL, ApplyFor.PARTNER);
        List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionService.findAll(pageable);
        HashMap<String, Object> results = new HashMap<>();
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        List<QuestionDTO> questionCHTG = new ArrayList<>();
        List<QuestionDTO> questionHDSD = new ArrayList<>();
        List<QuestionDTO> questionTLNV = new ArrayList<>();
        for (CategoryQuestionDTO categoryQuestionDTO : categoryQuestionDTOList) {
            questionDTOList.addAll(questionRepository
                .findAllByQuestionLikeAndCategoryQuestion(searchKey, categoryQuestionMapper.toEntity(categoryQuestionDTO).getId().toString())
                .stream()
                .map(questionMapper::toDto)
                .filter(item -> !item.getDeleted() && item.getStatus().equals(StatusQuestion.APPROVALED))
                .collect(Collectors.toCollection(LinkedList::new)));
            questionDTOList.forEach(item -> {
                item.setResultText(new String(item.getRsText(), StandardCharsets.UTF_8));
            });
        }
        if (userId.isPresent()) {
            questionDTOList = questionDTOList
                .stream()
                .filter(item -> applyFors.contains(item.getApplyFor()))
                .collect(Collectors.toCollection(LinkedList::new));
            List<QuestionDTO> questionIdTmp = new ArrayList<>();
            for (QuestionDTO questionDTO : questionDTOList) {
                if (questionDTO.getSendTo() != null && !questionDTO.getSendTo().isEmpty()) {
                    String roleGroup = RoleUtils.getRoleGroup(userClient, userId.get());
                    if (!questionDTO.getSendTo().contains(roleGroup)) {
                        questionIdTmp.add(questionDTO);
                    }
                }
            }
            for (QuestionDTO item : questionIdTmp) {
                questionDTOList.remove(item);
            }
        } else {
            questionDTOList = questionDTOList
                .stream()
                .filter(item -> item.getSendTo() == null)
                .collect(Collectors.toCollection(LinkedList::new));
        }
        questionDTOList.forEach(item -> {
            if (item.getCategoryQuestionType().equals(CategoryQuestionType.CHTG)) {
                questionCHTG.add(item);
            }
            if (item.getCategoryQuestionType().equals(CategoryQuestionType.HDSD)) {
                questionHDSD.add(item);
            }
            if (item.getCategoryQuestionType().equals(CategoryQuestionType.TLNV)) {
                questionTLNV.add(item);
            }
        });
        results.put(CategoryQuestionType.CHTG.toString(), questionCHTG);
        results.put(CategoryQuestionType.HDSD.toString(), questionHDSD);
        results.put(CategoryQuestionType.TLNV.toString(), questionTLNV);
        return results;
    }

    @Override
    public HashMap<String, Object> getAllQuestionManager(String userId, boolean isAdmin, Pageable pageable) {
        List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionService.findAll(pageable);
        List<StatusQuestion> statusQuestions = Arrays.asList(StatusQuestion.APPROVALED, StatusQuestion.WAITTING_APPROVAL, StatusQuestion.REJECTED, StatusQuestion.DISABLED);
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        HashMap<String, Object> results = new HashMap<>();
        List<QuestionDTO> questionCHTG = new ArrayList<>();
        List<QuestionDTO> questionHDSD = new ArrayList<>();
        List<QuestionDTO> questionTLNV = new ArrayList<>();
        for (CategoryQuestionDTO categoryQuestionDTO : categoryQuestionDTOList) {
            CategoryQuestion categoryQuestion = categoryQuestionMapper.toEntity(categoryQuestionDTO);
            questionDTOList.addAll(questionRepository.findAllByCategoryQuestion(categoryQuestion, pageable)
                .stream()
                .map(questionMapper::toDto)
                .filter(item -> !item.getDeleted())
                .collect(Collectors.toCollection(LinkedList::new)));
        }
//        questionDTOList.forEach(item -> {
//            item.setResultText(new String(item.getRsText(), StandardCharsets.UTF_8));
//            if (item.getUpdateBy() == null) {
//                UsersDTO usersDTO = userClient.getUserInfoById(item.getCreatedBy());
//                item.setCreatedBy(usersDTO.getFullName());
//            } else {
//                UsersDTO usersDTO = userClient.getUserInfoById(item.getUpdateBy());
//                item.setUpdateBy(usersDTO.getFullName());
//            }
//        });
        if (isAdmin) {
            questionDTOList.forEach(item -> {
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.CHTG)) {
                    if (!statusQuestions.contains(item.getStatus()) && item.getCreatedBy().equals(userId)) {
                        questionCHTG.add(item);
                    }
                    if (statusQuestions.contains(item.getStatus())) {
                        questionCHTG.add(item);
                    }
                }
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.HDSD)) {
                    if (!statusQuestions.contains(item.getStatus()) && item.getCreatedBy().equals(userId)) {
                        questionHDSD.add(item);
                    }
                    if (statusQuestions.contains(item.getStatus())) {
                        questionHDSD.add(item);
                    }

                }
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.TLNV)) {
                    if (!statusQuestions.contains(item.getStatus()) && item.getCreatedBy().equals(userId)) {
                        questionTLNV.add(item);
                    }
                    if (statusQuestions.contains(item.getStatus())) {
                        questionTLNV.add(item);
                    }

                }
            });
            List<QuestionDTO> questionDTOS = new ArrayList<>();
            questionDTOS.addAll(questionCHTG);
            questionDTOS.addAll(questionHDSD);
            questionDTOS.addAll(questionTLNV);

            questionDTOS.forEach(item -> {
                item.setResultText(new String(item.getRsText(), StandardCharsets.UTF_8));
                if (item.getUpdateBy() == null) {
                    UsersDTO usersDTO = userClient.getUserInfoById(item.getCreatedBy());
                    item.setCreatedBy(usersDTO.getFullName());
                } else {
                    UsersDTO usersDTO = userClient.getUserInfoById(item.getUpdateBy());
                    item.setUpdateBy(usersDTO.getFullName());
                }
            });

            results.put(CategoryQuestionType.CHTG.toString(), questionCHTG);
            results.put(CategoryQuestionType.HDSD.toString(), questionHDSD);
            results.put(CategoryQuestionType.TLNV.toString(), questionTLNV);
            return results;
        } else {
            questionDTOList.forEach(item -> {
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.CHTG)
                    || (item.getCreatedBy().equals(userId) && item.getStatus().equals(StatusQuestion.DRAFT))) {
                    questionCHTG.add(item);
                }
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.HDSD)
                    || (item.getCreatedBy().equals(userId) && item.getStatus().equals(StatusQuestion.DRAFT))) {
                    questionHDSD.add(item);
                }
                if (item.getCategoryQuestionType().equals(CategoryQuestionType.TLNV)
                    || (item.getCreatedBy().equals(userId) && item.getStatus().equals(StatusQuestion.DRAFT))) {
                    questionTLNV.add(item);
                }
            });
            List<QuestionDTO> questionDTOS = new ArrayList<>();
            questionDTOS.addAll(questionCHTG);
            questionDTOS.addAll(questionHDSD);
            questionDTOS.addAll(questionTLNV);

            questionDTOS.forEach(item -> {
                item.setResultText(new String(item.getRsText(), StandardCharsets.UTF_8));
                if (item.getUpdateBy() == null) {
                    UsersDTO usersDTO = userClient.getUserInfoById(item.getCreatedBy());
                    item.setCreatedBy(usersDTO.getFullName());
                } else {
                    UsersDTO usersDTO = userClient.getUserInfoById(item.getUpdateBy());
                    item.setUpdateBy(usersDTO.getFullName());
                }
            });

            results.put(CategoryQuestionType.CHTG.toString(), questionCHTG);
            results.put(CategoryQuestionType.HDSD.toString(), questionHDSD);
            results.put(CategoryQuestionType.TLNV.toString(), questionTLNV);
            return results;
        }
    }
}

package nth.module.utilities.service.impl;

import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import nth.lib.common.exception.BusinessException;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.domain.enumeration.HCSupportType;
import nth.module.utilities.repository.HCDecentralizationRepository;
import nth.module.utilities.repository.HCSupportBookRepository;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.service.mapper.HCDecentralizationMapper;
import nth.module.utilities.service.mapper.HCSupportBookMapper;
import nth.module.utilities.utils.DecentralizationUtils;
import nth.module.utilities.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HCDecentralization}.
 */
@Service
@Transactional
public class HCDecentralizationServiceImpl implements HCDecentralizationService {

    private final Logger log = LoggerFactory.getLogger(HCDecentralizationServiceImpl.class);

    private final HCDecentralizationRepository hCDecentralizationRepository;
    private final HCDecentralizationMapper hCDecentralizationMapper;
    private final HCSupportBookRepository hcSupportBookRepository;
    private final HCSupportBookMapper hcSupportBookMapper;
    private final UserClient userClient;

    public HCDecentralizationServiceImpl(
        HCDecentralizationRepository hCDecentralizationRepository,
        HCDecentralizationMapper hCDecentralizationMapper,
        HCSupportBookRepository hcSupportBookRepository, HCSupportBookMapper hcSupportBookMapper, UserClient userClient) {
        this.hCDecentralizationRepository = hCDecentralizationRepository;
        this.hCDecentralizationMapper = hCDecentralizationMapper;
        this.hcSupportBookRepository = hcSupportBookRepository;
        this.hcSupportBookMapper = hcSupportBookMapper;
        this.userClient = userClient;
    }

    @Override
    public HCDecentralizationDTO save(HCDecentralizationDTO hCDecentralizationDTO) {
        log.debug("Request to save HCDecentralization : {}", hCDecentralizationDTO);
        HCDecentralization hCDecentralization = hCDecentralizationMapper.toEntity(hCDecentralizationDTO);
        hCDecentralization = hCDecentralizationRepository.save(hCDecentralization);
        return hCDecentralizationMapper.toDto(hCDecentralization);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HCDecentralizationDTO> findAll() {
        log.debug("Request to get all HCDecentralizations");
        return hCDecentralizationRepository
            .findAll()
            .stream()
            .filter(HCDecentralization::getEnable)
            .map(hCDecentralizationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HCDecentralizationDTO> findOne(Long id) {
        log.debug("Request to get HCDecentralization : {}", id);
        return hCDecentralizationRepository.findById(id).map(hCDecentralizationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HCDecentralization : {}", id);
        HCDecentralization hcDecentralization = hCDecentralizationRepository
            .findById(id)
            .orElseThrow(() -> new BusinessException("hcDecentralization"));
        hcDecentralization.setEnable(false);
        hCDecentralizationRepository.save(hcDecentralization);
    }

    @Override
    public List<HCDecentralizationDTO> findAllByUserId(String userId) {
        return hCDecentralizationRepository
            .findAllByUserId(userId)
            .stream()
            .filter(HCDecentralization::getEnable)
            .map(hCDecentralizationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<HCDecentralizationDTO> findAllByDepartment(HCDepartment hcDepartment) {
        return hCDecentralizationRepository
            .findAllByDepartment(hcDepartment)
            .stream()
            .filter(HCDecentralization::getEnable)
            .map(hCDecentralizationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public HCDecentralizationDTO createDecentralization(String userId, HCDecentralizationDTO hcDecentralizationDTO) throws InstantiationException, IllegalAccessException {

        if (hcDecentralizationDTO.getId() != null) {
            HCDecentralizationDTO hcDecentralizationCurrent = this.findOne(hcDecentralizationDTO.getId())
                .orElseThrow(() -> new BusinessException("decentralizationNotFound"));

            ObjectUtils.mergeObjects(hcDecentralizationDTO, hcDecentralizationCurrent);
            hcDecentralizationDTO.setUpdateBy(userId);
            hcDecentralizationDTO.setUpdatedDate(Instant.now());
        } else {
            Optional<HCDecentralizationDTO>  hcDecentralizationCurrent = this
                .findFirstByUserIdAndDepartment(hcDecentralizationDTO.getUserId(), hcDecentralizationDTO.getDepartment());
            if (hcDecentralizationCurrent.isPresent()) {
                ObjectUtils.mergeObjects(hcDecentralizationDTO, hcDecentralizationCurrent);
            }
            hcDecentralizationDTO.setCreatedBy(userId);
            hcDecentralizationDTO.setCreatedDate(Instant.now());
        }
        hcDecentralizationDTO.setEnable(true);
        hcDecentralizationDTO = this.save(hcDecentralizationDTO);
        List<HCSupportType> hcSupportTypeList = DecentralizationUtils.autoSelectHCSupportType(hcDecentralizationDTO.getDepartment());
        try{
            List<HCSupportBookDTO> hcSupportBookDTOList = hcSupportBookRepository
                .findAllByTypeIn(hcSupportTypeList)
                .stream()
                .map(hcSupportBookMapper::toDto)
                .collect(Collectors.toList());
            for (HCSupportBookDTO hcSupportBookDTO : hcSupportBookDTOList) {
                hcSupportBookDTO.setFlowerInOnId(hcSupportBookDTO.getFlowerInOnId().concat(",").concat(hcDecentralizationDTO.getUserId()));
                hcSupportBookRepository.save(hcSupportBookMapper.toEntity(hcSupportBookDTO));
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return hcDecentralizationDTO;
    }

    @Override
    public HCDecentralizationDTO updateDecentralization(String userId, HCDecentralizationDTO hcDecentralizationDTO) throws InstantiationException, IllegalAccessException {
        Optional<HCDecentralizationDTO>  oldHCDecentralizationsOptional = this.findOne(hcDecentralizationDTO.getId());
        if (!oldHCDecentralizationsOptional.isPresent()){
            throw new BusinessException("decentralizationNotFound");
        }
        if (!hcDecentralizationDTO.getEnable()) {

            hcDecentralizationDTO.setProcessing(0L);
            this.save(hcDecentralizationDTO);

            List<HCStatus> hcStatusList = Arrays.asList(HCStatus.TODO, HCStatus.DOING);

            List<HCDecentralizationDTO> hcDecentralizationDTOList = hCDecentralizationRepository
                .findAllByDepartment(hcDecentralizationDTO.getDepartment())
                .stream()
                .filter(HCDecentralization::getEnable)
                .map(hCDecentralizationMapper::toDto)
                .collect(Collectors.toList());

            /*
            * todo auto select AD_OP
            *  */
            HCDecentralizationDTO adOpDepartment;
            Optional<HCDecentralizationDTO>  adOpDepartmentOptional = hcDecentralizationDTOList
                .stream()
                .filter(item -> item.getDecentralization().equals(HCDecentralizationType.AD_OP) && item.getEnable())
                .collect(Collectors.toList())
                .stream().findFirst();

            adOpDepartment = adOpDepartmentOptional.orElseGet(() -> hcDecentralizationDTOList
                .stream()
                .findFirst()
                .orElseThrow(() -> new BusinessException("decentralizationNotFound")));

            UsersDTO usersDTO = userClient.getUserInfoById(adOpDepartment.getUserId());


            List<HCSupportBookDTO> hcSupportBookDTOList = hcSupportBookRepository
                .findAllBySupporterInOnIdAndStatusIn(hcDecentralizationDTO.getUserId(),hcStatusList)
                .stream()
                .map(hcSupportBookMapper::toDto)
                .collect(Collectors.toList());

            for (HCSupportBookDTO hcSupportBookDTO : hcSupportBookDTOList) {
                hcSupportBookDTO.setSupporterInOnId(adOpDepartment.getUserId());
                hcSupportBookDTO.setSupporterInOnIdName(usersDTO.getFullName());
                hcSupportBookRepository.save(hcSupportBookMapper.toEntity(hcSupportBookDTO));
            }

            adOpDepartment.setProcessing((long) hcSupportBookDTOList.size());
            this.save(adOpDepartment);
        }
        ObjectUtils.mergeObjects(hcDecentralizationDTO, oldHCDecentralizationsOptional.get());
        hcDecentralizationDTO.setUpdatedDate(Instant.now());
        hcDecentralizationDTO.setUpdateBy(userId);
        hcDecentralizationDTO = this.save(hcDecentralizationDTO);
        return hcDecentralizationDTO;
    }

    @Override
    public Optional<HCDecentralizationDTO> findFirstByUserIdAndDepartment(String userId, HCDepartment hcDepartment) {
        return hCDecentralizationRepository.findFirstByUserIdAndDepartment(userId, hcDepartment)
            .map(hCDecentralizationMapper::toDto);
    }
}

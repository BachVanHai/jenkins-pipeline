package nth.module.utilities.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.CategoryQuestion;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import nth.module.utilities.repository.CategoryQuestionRepository;
import nth.module.utilities.repository.QuestionRepository;
import nth.module.utilities.service.CategoryQuestionService;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.service.dto.QuestionDTO;
import nth.module.utilities.service.mapper.CategoryQuestionMapper;
import nth.module.utilities.service.mapper.QuestionMapper;
import nth.module.utilities.utils.RoleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.BeanDefinitionDsl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CategoryQuestion}.
 */
@Service
@Transactional
public class CategoryQuestionServiceImpl implements CategoryQuestionService {

    private final Logger log = LoggerFactory.getLogger(CategoryQuestionServiceImpl.class);

    private final CategoryQuestionRepository categoryQuestionRepository;

    private final CategoryQuestionMapper categoryQuestionMapper;

    private final QuestionRepository questionRepository;

    private final QuestionMapper questionMapper;


    public CategoryQuestionServiceImpl(
        CategoryQuestionRepository categoryQuestionRepository,
        CategoryQuestionMapper categoryQuestionMapper, QuestionRepository questionRepository, QuestionMapper questionMapper) {
        this.categoryQuestionRepository = categoryQuestionRepository;
        this.categoryQuestionMapper = categoryQuestionMapper;
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
    }

    @Override
    public CategoryQuestionDTO save(CategoryQuestionDTO categoryQuestionDTO) {
        log.debug("Request to save CategoryQuestion : {}", categoryQuestionDTO);
        CategoryQuestion categoryQuestion = categoryQuestionMapper.toEntity(categoryQuestionDTO);
        categoryQuestion = categoryQuestionRepository.save(categoryQuestion);
        return categoryQuestionMapper.toDto(categoryQuestion);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryQuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CategoryQuestions");
        List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionRepository
            .findAll(pageable)
            .stream()
            .map(categoryQuestionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
        categoryQuestionDTOList.forEach(item -> {
            List<QuestionDTO> questionDTOList = questionRepository
                .findAllByCategoryQuestion(categoryQuestionMapper.toEntity(item), pageable)
                .stream()
                .map(questionMapper::toDto)
                .filter(value -> value.getStatus().equals(StatusQuestion.APPROVALED))
                .collect(Collectors.toCollection(LinkedList::new));
            questionDTOList.forEach(value -> {
                value.setResultText(value.getRsText() == null ? null : new String(value.getRsText(), StandardCharsets.UTF_8));
            });
            item.setQuestionDTOList(questionDTOList);
        });
        return categoryQuestionDTOList;
    }

    @Override
    public List<CategoryQuestionDTO> findAllCategoryQuestionByUserId(String userId, Pageable pageable, UserClient userClient) {
        log.debug("Request to get all CategoryQuestions");
        String roleGroup = null;
        if (userId != null) {
            roleGroup = RoleUtils.getRoleGroup(userClient, userId);
        }
        List<CategoryQuestionDTO> categoryQuestionDTOList = categoryQuestionRepository
            .findAll(pageable)
            .stream()
            .map(categoryQuestionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
        String finalRoleGroup = roleGroup;
        categoryQuestionDTOList.forEach(item -> {
            List<QuestionDTO> questionDTOListTmp = new ArrayList<>();
            List<QuestionDTO> questionDTOList = questionRepository
                .findAllByCategoryQuestion(categoryQuestionMapper.toEntity(item), pageable)
                .stream()
                .map(questionMapper::toDto)
                .filter(value -> value.getStatus().equals(StatusQuestion.APPROVALED) && !value.getDeleted())
                .collect(Collectors.toCollection(LinkedList::new));

            if (questionDTOList.size() == 0) {
                return;
            }

            for (QuestionDTO questionDTO : questionDTOList) {
                if (questionDTO.getSendTo() != null && !questionDTO.getSendTo().isEmpty()) {
                    if (finalRoleGroup == null) {
                        questionDTOListTmp.add(questionDTO);
                    } else {
                        if ((!questionDTO.getSendTo().contains(finalRoleGroup))) {
                            questionDTOListTmp.add(questionDTO);
                        }
                    }
                }
            }

            for (QuestionDTO questionDTO : questionDTOListTmp) {
                questionDTOList.remove(questionDTO);
            }

            questionDTOList.forEach(value -> {
                value.setResultText(value.getRsText() == null ? null : new String(value.getRsText(), StandardCharsets.UTF_8));
            });
            item.setQuestionDTOList(questionDTOList);
        });
        return categoryQuestionDTOList;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryQuestionDTO> findOne(Long id) {
        log.debug("Request to get CategoryQuestion : {}", id);
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        Optional<CategoryQuestionDTO> categoryQuestionDTOOptional = categoryQuestionRepository
            .findById(id).map(categoryQuestionMapper::toDto);
        List<QuestionDTO> questionDTOList = questionRepository
            .findAllByCategoryQuestion(categoryQuestionMapper.toEntity(categoryQuestionDTOOptional.get()), pageable)
            .stream()
            .map(questionMapper::toDto)
            .filter(value -> value.getStatus().equals(StatusQuestion.APPROVALED))
            .collect(Collectors.toCollection(LinkedList::new));
        questionDTOList.forEach(value -> {
            value.setResultText(value.getRsText() == null ? null : new String(value.getRsText(), StandardCharsets.UTF_8));
        });
        categoryQuestionDTOOptional.get().setQuestionDTOList(questionDTOList);
        return categoryQuestionDTOOptional;
    }

    @Override
    public Optional<CategoryQuestionDTO> findOneCategoryQuestionByUserId(String userId, Long id, UserClient userClient) {
        log.debug("Request to get CategoryQuestion : {}", id);
        String roleGroup = RoleUtils.getRoleGroup(userClient, userId);
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by("createdDate").descending());
        Optional<CategoryQuestionDTO> categoryQuestionDTOOptional = categoryQuestionRepository
            .findById(id).map(categoryQuestionMapper::toDto);
        List<QuestionDTO> questionDTOList = questionRepository
            .findAllByCategoryQuestion(categoryQuestionMapper.toEntity(categoryQuestionDTOOptional.get()), pageable)
            .stream()
            .map(questionMapper::toDto)
            .filter(value -> value.getStatus().equals(StatusQuestion.APPROVALED) && !value.getDeleted())
            .collect(Collectors.toCollection(LinkedList::new));

        List<QuestionDTO> questionDTOListTmp = new ArrayList<>();

        for (QuestionDTO questionDTO : questionDTOList) {
            if (questionDTO.getSendTo() != null && !questionDTO.getSendTo().isEmpty()) {
                if (roleGroup == null) {
                    questionDTOListTmp.add(questionDTO);
                } else {
                    if ((!questionDTO.getSendTo().contains(roleGroup))) {
                        questionDTOListTmp.add(questionDTO);
                    }
                }
            }
        }
        for (QuestionDTO questionDTO : questionDTOListTmp) {
            questionDTOList.remove(questionDTO);
        }
        questionDTOList.forEach(value -> {
            value.setResultText(value.getRsText() == null ? null : new String(value.getRsText(), StandardCharsets.UTF_8));
        });
        categoryQuestionDTOOptional.get().setQuestionDTOList(questionDTOList);
        return categoryQuestionDTOOptional;
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CategoryQuestion : {}", id);
        categoryQuestionRepository.deleteById(id);
    }

    @Override
    public List<CategoryQuestionDTO> findAllCategoryQuestionByType(CategoryQuestionType categoryQuestionType, Pageable pageable) {
        return categoryQuestionRepository.findAllByCategoryQuestionType(categoryQuestionType, pageable)
            .stream()
            .map(categoryQuestionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}

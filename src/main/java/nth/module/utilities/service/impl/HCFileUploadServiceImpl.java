package nth.module.utilities.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import nth.module.utilities.domain.HCFileUpload;
import nth.module.utilities.repository.HCFileUploadRepository;
import nth.module.utilities.service.HCFileUploadService;
import nth.module.utilities.service.dto.HCFileUploadDTO;
import nth.module.utilities.service.mapper.HCFileUploadMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HCFileUpload}.
 */
@Service
@Transactional
public class HCFileUploadServiceImpl implements HCFileUploadService {

    private final Logger log = LoggerFactory.getLogger(HCFileUploadServiceImpl.class);

    private final HCFileUploadRepository hCFileUploadRepository;

    private final HCFileUploadMapper hCFileUploadMapper;

    public HCFileUploadServiceImpl(HCFileUploadRepository hCFileUploadRepository, HCFileUploadMapper hCFileUploadMapper) {
        this.hCFileUploadRepository = hCFileUploadRepository;
        this.hCFileUploadMapper = hCFileUploadMapper;
    }

    @Override
    public HCFileUploadDTO save(HCFileUploadDTO hCFileUploadDTO) {
        log.debug("Request to save HCFileUpload : {}", hCFileUploadDTO);
        HCFileUpload hCFileUpload = hCFileUploadMapper.toEntity(hCFileUploadDTO);
        hCFileUpload = hCFileUploadRepository.save(hCFileUpload);
        return hCFileUploadMapper.toDto(hCFileUpload);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HCFileUploadDTO> findAll() {
        log.debug("Request to get all HCFileUploads");
        return hCFileUploadRepository.findAll().stream().map(hCFileUploadMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HCFileUploadDTO> findOne(Long id) {
        log.debug("Request to get HCFileUpload : {}", id);
        return hCFileUploadRepository.findById(id).map(hCFileUploadMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HCFileUpload : {}", id);
        hCFileUploadRepository.deleteById(id);
    }

    @Override
    public List<HCFileUploadDTO> createFileUpload(String userId, List<HCFileUploadDTO> fileUploadDTOList) {
        List<HCFileUploadDTO> result = new ArrayList<>();
        for (HCFileUploadDTO hcFileUploadDTO : fileUploadDTOList) {
            if (userId == null) {
                hcFileUploadDTO.setCreatedBy("Guest");
            } else {
                hcFileUploadDTO.setCreatedBy(userId);
            }
            hcFileUploadDTO.setCreatedDate(Instant.now());
            hcFileUploadDTO = this.save(hcFileUploadDTO);
            result.add(hcFileUploadDTO);
        }
        return result;
    }

    @Override
    public List<HCFileUploadDTO> findAllByHCMessageId(String hcMessageId) {
        return hCFileUploadRepository.findAllByHCMessageId(hcMessageId)
            .stream()
            .map(hCFileUploadMapper::toDto)
            .collect(Collectors.toList());
    }
}

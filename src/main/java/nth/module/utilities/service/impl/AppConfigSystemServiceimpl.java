package nth.module.utilities.service.impl;

import nth.module.utilities.repository.AppConfigRepository;
import nth.module.utilities.service.AppConfigSystemService;
import nth.module.utilities.service.dto.AppConfigSystemDTO;
import nth.module.utilities.service.mapper.AppConfigSystemMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppConfigSystemServiceimpl implements AppConfigSystemService {

    private final AppConfigSystemMapper appConfigSystemMapper;
    private final AppConfigRepository appConfigRepository;

    public AppConfigSystemServiceimpl(AppConfigSystemMapper appConfigSystemMapper, AppConfigRepository appConfigRepository) {
        this.appConfigSystemMapper = appConfigSystemMapper;
        this.appConfigRepository = appConfigRepository;
    }

    @Override
    public List<AppConfigSystemDTO> findAll() {
        return appConfigRepository
            .findAll()
            .stream()
            .map(appConfigSystemMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<AppConfigSystemDTO> findAllByKeyAndEnable(String key, Boolean enable) {
        return appConfigRepository
            .findAllByKeyAndEnable(key,enable)
            .stream()
            .map(appConfigSystemMapper::toDto)
            .collect(Collectors.toList());
    }
}

package nth.module.utilities.service.impl;

import nth.lib.common.utils.DateTimeUtils;
import nth.lib.integration.enumeration.ApprovalStatus;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.model.ContractDTO;
import nth.module.utilities.client.HomeInsuranceClient;
import nth.module.utilities.client.PersonalInsuranceClient;
import nth.module.utilities.client.VehicleInsuranceClient;
import nth.module.utilities.security.jwt.TokenProvider;
import nth.module.utilities.service.UtilitiesContractService;
import nth.module.utilities.utils.ObjectUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;

@Service
public class UtilitiesContractServiceImpl implements UtilitiesContractService {

    private final VehicleInsuranceClient vehicleInsuranceClient;

    private final HomeInsuranceClient homeInsuranceClient;

    private final PersonalInsuranceClient personalInsuranceClient;

    private final TokenProvider tokenProvider;

    private final RestHighLevelClient restClient;

    public UtilitiesContractServiceImpl(VehicleInsuranceClient vehicleInsuranceClient, HomeInsuranceClient homeInsuranceClient, PersonalInsuranceClient personalInsuranceClient, TokenProvider tokenProvider, RestHighLevelClient restClient) {
        this.vehicleInsuranceClient = vehicleInsuranceClient;
        this.homeInsuranceClient = homeInsuranceClient;
        this.personalInsuranceClient = personalInsuranceClient;
        this.tokenProvider = tokenProvider;
        this.restClient = restClient;
    }

    @Override
    public void checkPaidContract(String contractCode, BigDecimal totalFeeTranserred) {
        ContractType contractType = getContractType(contractCode);
        ContractDTO contractDTO;
        String jwt = tokenProvider.createFakeToken("openapi", true);
        String token = "Bearer " + jwt;
        switch (Objects.requireNonNull(contractType)) {
            case MC:
            case CC:
                contractDTO = vehicleInsuranceClient.getContractByContractCode(contractCode);
                if (totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) == 0 || totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) > 0) {
                    List<String> contractIds = Collections.singletonList(contractDTO.getId());
                    vehicleInsuranceClient.approvalContract(token, contractType, ApprovalStatus.APPROVED, contractIds);
                }
                break;
            case FH:
                contractDTO = homeInsuranceClient.getContractsByContractCode(contractCode);
                if (totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) == 0 || totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) > 0) {
                    List<String> contractIds = Collections.singletonList(contractDTO.getId());
                    homeInsuranceClient.approvalContract(token, contractType, nth.lib.common.status.ApprovalStatus.APPROVED, contractIds);
                }
                break;
            case HC:
            case VTA:
                contractDTO = personalInsuranceClient.getContractByContractCode(contractCode);
                if (totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) == 0 || totalFeeTranserred.compareTo(contractDTO.getTotalFeeInclVAT()) > 0) {
                    List<String> contractIds = Collections.singletonList(contractDTO.getId());
                    personalInsuranceClient.approvalContract(token, contractType, nth.lib.common.status.ApprovalStatus.APPROVED, contractIds);
                }
                break;
        }
    }

    private ContractType getContractType(String contractCode) {
        for (ContractType contractType : ContractType.values()) {
            if (contractCode.startsWith(contractType.name())) {
                return contractType;
            }
        }
        return null;
    }

    @Override
    public List<ContractDTO> finAllExpiredContract() throws IOException, ParseException {

        List<ContractDTO> result = new ArrayList<>();

        Instant atStartExpiredDate = DateTimeUtils.atStartOfDay(Date.from(Instant.now())).toInstant();
        Instant endExpiredDate = DateTimeUtils.atEndOfDay(Date.from(Instant.now())).toInstant();

        SearchRequest searchRequest = new SearchRequest("contracts");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        boolQuery.must().add(QueryBuilders.matchQuery("latest_approval_status", "FINISH"));
        boolQuery.must().add(QueryBuilders.rangeQuery("end_value_date").gte(atStartExpiredDate).lte(endExpiredDate));
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(10000);
        searchSourceBuilder.sort("update_date", SortOrder.DESC);
        searchSourceBuilder.query(boolQuery);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = this.restClient.search(searchRequest, RequestOptions.DEFAULT);
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            Map<String, Object> map = hit.getSourceAsMap();
            map.put("id", hit.getId());
            result.add(ObjectUtils.mapESToObject(map));
        }
        return result;
    }
}

package nth.module.utilities.service.impl;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import nth.lib.common.exception.BusinessException;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.ContactManagerClient;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.HCUser;
import nth.module.utilities.repository.HCUserRepository;
import nth.module.utilities.security.jwt.TokenProvider;
import nth.module.utilities.service.HCUserService;
import nth.module.utilities.service.dto.HCUserDTO;
import nth.module.utilities.service.mapper.HCUserMapper;
import nth.module.utilities.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HCUser}.
 */
@Service
@Transactional
public class HCUserServiceImpl implements HCUserService {

    private final Logger log = LoggerFactory.getLogger(HCUserServiceImpl.class);

    private final HCUserRepository hCUserRepository;
    private final HCUserMapper hCUserMapper;
    private final UserClient userClient;
    private final ContactManagerClient contactManagerClient;
    private final TokenProvider tokenProvider;

    public HCUserServiceImpl(HCUserRepository hCUserRepository, HCUserMapper hCUserMapper, UserClient userClient, ContactManagerClient contactManagerClient, TokenProvider tokenProvider) {
        this.hCUserRepository = hCUserRepository;
        this.hCUserMapper = hCUserMapper;
        this.userClient = userClient;
        this.contactManagerClient = contactManagerClient;
        this.tokenProvider = tokenProvider;
    }

    @Override
    public HCUserDTO save(HCUserDTO hCUserDTO) {
        log.debug("Request to save HCUser : {}", hCUserDTO);
        HCUser hCUser = hCUserMapper.toEntity(hCUserDTO);
        hCUser = hCUserRepository.save(hCUser);
        return hCUserMapper.toDto(hCUser);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HCUserDTO> findAll() {
        log.debug("Request to get all HCUsers");
        return hCUserRepository
            .findAll()
            .stream()
            .map(hCUserMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HCUserDTO> findOne(Long id) {
        log.debug("Request to get HCUser : {}", id);
        Optional<HCUserDTO> hcUserDTOOptional = hCUserRepository.findById(id).map(hCUserMapper::toDto);
        hcUserDTOOptional.ifPresent(hcUserDTO -> hcUserDTO.setUserIdInOn(null));
        return hCUserRepository.findById(id).map(hCUserMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HCUser : {}", id);
        hCUserRepository.deleteById(id);
    }

    @Override
    public HCUserDTO createUserSupportBook(String userId,HCUserDTO hCUserDTO) {


        Optional<HCUserDTO> userCurrentHC;
        HCUserDTO hcUserDTONew = new HCUserDTO();

        String token = "Bearer " + tokenProvider.createAdminFakeToken();

        Optional<UsersDTO> usersDTOOptional;
        if (hCUserDTO.getUserIdInOn() != null && !hCUserDTO.getUserIdInOn().isEmpty()) {
            usersDTOOptional = Optional.of(userClient.getUserInfoById(hCUserDTO.getUserIdInOn(), token));
        } else {
            usersDTOOptional = userClient.getUsersByPhoneNumberAndEmail(hCUserDTO.getPhoneNumber(), hCUserDTO.getEmail());
        }

        if (usersDTOOptional.isPresent()) {
            userCurrentHC = hCUserRepository
                .findOneByUserIdInOn(usersDTOOptional.get().getId().toString())
                .map(hCUserMapper::toDto);
            if (userCurrentHC.isPresent()){
                userCurrentHC.get().setAvatar(hCUserDTO.getAvatar());
                return userCurrentHC.get();
            } else {
                hcUserDTONew.setDeviceId(hCUserDTO.getDeviceId());
                hcUserDTONew.setUserIdInOn(usersDTOOptional.get().getId().toString());
                hcUserDTONew.setName(usersDTOOptional.get().getFullName());
                hcUserDTONew.setEmail(usersDTOOptional.get().getEmail());
                hcUserDTONew.setPhoneNumber(usersDTOOptional.get().getPhoneNumber());
                hcUserDTONew.setCreatedDate(Instant.now());
                hcUserDTONew.setCreatedBy(userId);
                hcUserDTONew.setUpdateBy(userId);
                hcUserDTONew.setUpdatedDate(Instant.now());
                hcUserDTONew.setAvatar(hCUserDTO.getAvatar());
                hcUserDTONew = this.save(hcUserDTONew);
                return hcUserDTONew;
            }
        } else {
            userCurrentHC = hCUserRepository
                .findOneByNameAndEmail(hCUserDTO.getName(), hCUserDTO.getEmail())
                .map(hCUserMapper::toDto);
        }

        if (userCurrentHC.isPresent()) {
            hcUserDTONew = userCurrentHC.get();
            return hcUserDTONew;
        }
        hcUserDTONew.setName(hCUserDTO.getName());
        hcUserDTONew.setEmail(hCUserDTO.getEmail());
        hcUserDTONew.setPhoneNumber(hCUserDTO.getPhoneNumber());
        hcUserDTONew.setDeviceId(hCUserDTO.getDeviceId());
        hcUserDTONew.setAvatar(hCUserDTO.getAvatar());
        hcUserDTONew.setCreatedDate(Instant.now());
        hcUserDTONew.setCreatedBy(userId);
        hcUserDTONew = this.save(hcUserDTONew);
        return hcUserDTONew;
    }

    @Override
    public HashMap<String, List<UsersDTO>> findAllUserInDepartment() {
        HashMap<String, List<UsersDTO>> result = new HashMap<>();
        List<String> roles = Arrays.asList("VH.IO","IT.IO","KT.IO");
        for (String role : roles) {
            List<UsersDTO> usersDTOList = userClient.getListUserOfGroupRole(role);
            result.put("role", usersDTOList);
        }
        return result;
    }

    @Override
    public Optional<HCUserDTO> updateUserSupportBook(String userId, HCUserDTO hcUserDTO) throws InstantiationException, IllegalAccessException {

        if (!userId.equals(hcUserDTO.getUserIdInOn())) {
            throw new BusinessException("accessDenied");
        }

        Optional<HCUserDTO> hcUserDTOOptional = this.findOne(hcUserDTO.getId());

        if (!hcUserDTOOptional.isPresent()) {
            return Optional.empty();
        } else if(hcUserDTOOptional.get().getUserIdInOn() != null) {
            return hcUserDTOOptional;
        }else {
            if (!userId.equals(hcUserDTO.getCreatedBy())) {
                throw new BusinessException("accessDenied");
            }
            ObjectUtils.mergeObjects(hcUserDTO, hcUserDTOOptional.get());
            hcUserDTO.setUpdateBy(userId);
            hcUserDTO.setUpdatedDate(Instant.now());
            hcUserDTO = this.save(hcUserDTO);
            return Optional.of(hcUserDTO);
        }
    }

    @Override
    public HCUserDTO checkExistHCUser(String userId) {
        Optional<HCUserDTO> hcUserDTOOptional = this.hCUserRepository.findOneByUserIdInOn(userId)
            .map(hCUserMapper::toDto);

        if (!hcUserDTOOptional.isPresent()){
            UsersDTO usersDTO = this.userClient.getUserInfoById(userId);
            HCUserDTO hcUserDTO = new HCUserDTO();
            hcUserDTO.setUserIdInOn(usersDTO.getId() + "");
            hcUserDTO.setName(usersDTO.getFullName());
            hcUserDTO.setPhoneNumber(usersDTO.getPhoneNumber());
            hcUserDTO.setEmail(usersDTO.getEmail());
            hcUserDTO.setCreatedBy(userId);
            hcUserDTO.setCreatedDate(Instant.now());
            hcUserDTO.setUpdatedDate(Instant.now());
            hcUserDTO.setUpdateBy(userId);
            hcUserDTO = this.save(hcUserDTO);
            return hcUserDTO;
        } else {
            return hcUserDTOOptional.get();
        }
    }

    @Override
    public Optional<HCUserDTO> findOneByUserInOnId(Long id) {
        return this.hCUserRepository.findOneByUserIdInOn(id + "")
            .map(hCUserMapper::toDto);
    }
}

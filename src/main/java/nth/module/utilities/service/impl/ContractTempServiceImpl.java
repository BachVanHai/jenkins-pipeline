package nth.module.utilities.service.impl;

import nth.module.utilities.domain.ContractTemp;
import nth.module.utilities.repository.ContractTempRepository;
import nth.module.utilities.service.ContractTempService;
import nth.module.utilities.service.dto.ContractTempDTO;
import nth.module.utilities.service.mapper.ContractTempMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContractTempServiceImpl implements ContractTempService {

    private final ContractTempRepository contractTempRepository;
    private final ContractTempMapper contractTempMapper;

    public ContractTempServiceImpl(ContractTempRepository contractTempRepository, ContractTempMapper contractTempMapper) {
        this.contractTempRepository = contractTempRepository;
        this.contractTempMapper = contractTempMapper;
    }

    @Override
    public ContractTempDTO save(ContractTempDTO contractTempDTO) {
        ContractTemp contractTemp = contractTempMapper.toEntity(contractTempDTO);
        contractTemp = contractTempRepository.save(contractTemp);
        return contractTempMapper.toDto(contractTemp);
    }

    @Override
    public Optional<ContractTempDTO> findOne(Long id) {
        return contractTempRepository.findById(id).map(contractTempMapper::toDto);
    }

    @Override
    public Optional<ContractTempDTO> findByCode(String code) {
        return contractTempRepository.findAllByCode(code).map(contractTempMapper::toDto);
    }

    @Override
    public List<ContractTempDTO> createContractTemp(List<ContractTempDTO> contractTempDTO, String userId, String contractType) {
        List<ContractTempDTO> result = new ArrayList<>();
        for (ContractTempDTO tempDTO : contractTempDTO) {
            tempDTO.setCode(contractType + System.currentTimeMillis());
            tempDTO.setCreatedBy(userId);
            tempDTO.setCreatedDate(Instant.now());
            tempDTO = this.save(tempDTO);
            result.add(tempDTO);
        }
        return result;
    }
}

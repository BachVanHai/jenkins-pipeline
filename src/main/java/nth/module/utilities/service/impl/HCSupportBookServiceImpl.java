package nth.module.utilities.service.impl;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import nth.lib.common.exception.BusinessException;
import nth.lib.common.utils.XSSFilter;
import nth.lib.integration.enumeration.MailTemplate;
import nth.lib.integration.model.SendEmailDTO;
import nth.lib.integration.model.UsersDTO;
import nth.lib.integration.model.utilities.NotificationsDTO;
import nth.module.utilities.client.GatewayClient;
import nth.module.utilities.client.NotificationsClient;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.HCSupportBook;
import nth.module.utilities.domain.HCUser;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.domain.enumeration.TypeMessage;
import nth.module.utilities.repository.HCDecentralizationRepository;
import nth.module.utilities.repository.HCSupportBookRepository;
import nth.module.utilities.repository.HCUserRepository;
import nth.module.utilities.security.jwt.TokenProvider;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.HCMessageService;
import nth.module.utilities.service.HCSupportBookService;
import nth.module.utilities.service.HCUserService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import nth.module.utilities.service.dto.HCMessageDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.service.dto.HCUserDTO;
import nth.module.utilities.service.mapper.HCDecentralizationMapper;
import nth.module.utilities.service.mapper.HCSupportBookMapper;
import nth.module.utilities.service.mapper.HCUserMapper;
import nth.module.utilities.utils.DecentralizationUtils;
import nth.module.utilities.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HCSupportBook}.
 */
@Service
@Transactional
public class HCSupportBookServiceImpl implements HCSupportBookService {

    private final Logger log = LoggerFactory.getLogger(HCSupportBookServiceImpl.class);

    private final HCSupportBookRepository hCSupportBookRepository;
    private final HCSupportBookMapper hCSupportBookMapper;
    private final HCMessageService hcMessageService;
    private final UserClient userClient;
    private final HCDecentralizationRepository hcDecentralizationRepository;
    private final HCDecentralizationMapper hcDecentralizationMapper;
    private final NotificationsClient notificationsClient;
    private final TokenProvider tokenProvider;
    private final HCDecentralizationService hcDecentralizationService;
    private final GatewayClient gatewayClient;
    private final HCUserService hcUserService;
    private final HCUserMapper hcUserMapper;

    @Value("${app-configs.url-support-book}")
    public String urlSupportBook;

    public HCSupportBookServiceImpl(HCSupportBookRepository hCSupportBookRepository, HCSupportBookMapper hCSupportBookMapper, @Lazy HCMessageService hcMessageService, UserClient userClient, HCUserRepository hcUserRepository, DecentralizationUtils decentralizationUtils, HCDecentralizationRepository hcDecentralizationRepository, HCDecentralizationMapper hcDecentralizationMapper, NotificationsClient notificationsClient, TokenProvider tokenProvider, HCDecentralizationService hcDecentralizationService, HCDecentralizationService hcDecentralizationService1, GatewayClient gatewayClient, HCUserService hcUserService, HCUserMapper hcUserMapper) {
        this.hCSupportBookRepository = hCSupportBookRepository;
        this.hCSupportBookMapper = hCSupportBookMapper;
        this.hcMessageService = hcMessageService;
        this.userClient = userClient;
        this.hcDecentralizationRepository = hcDecentralizationRepository;
        this.hcDecentralizationMapper = hcDecentralizationMapper;
        this.notificationsClient = notificationsClient;
        this.tokenProvider = tokenProvider;
        this.hcDecentralizationService = hcDecentralizationService1;
        this.gatewayClient = gatewayClient;
        this.hcUserService = hcUserService;
        this.hcUserMapper = hcUserMapper;
    }

    @Override
    public HCSupportBookDTO save(HCSupportBookDTO hCSupportBookDTO) {
        log.debug("Request to save HCSupportBook : {}", hCSupportBookDTO);
        HCSupportBook hCSupportBook = hCSupportBookMapper.toEntity(hCSupportBookDTO);
        hCSupportBook = hCSupportBookRepository.save(hCSupportBook);
        return hCSupportBookMapper.toDto(hCSupportBook);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HCSupportBookDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HCSupportBooks");

        return hCSupportBookRepository.findAll(pageable)
            .stream()
            .map(hCSupportBookMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HCSupportBookDTO> findOne(String userId,Long id) {
        log.debug("Request to get HCSupportBook : {}", id);
        Optional<HCSupportBookDTO> hcSupportBookDTOOptional = hCSupportBookRepository.findById(id).map(hCSupportBookMapper::toDto);
        if (!hcSupportBookDTOOptional.isPresent()) {
            return Optional.empty();
        } else {
            HCSupportBookDTO result = hcSupportBookDTOOptional.get();
            List<UsersDTO> usersDTOList = new ArrayList<>();
            HCUserDTO hcUserDTO = this.hcUserService.findOne(result.gethCUserId())
                .orElseThrow(() -> new BusinessException("accessDenied"));

            String[] followerId = result.getFlowerInOnId().split(",");
            if (!Arrays.stream(followerId).collect(Collectors.toList()).contains(userId) && !userId.equals(hcUserDTO.getUserIdInOn())) {
                throw new BusinessException("accessDenied");
            }
            String token = "Bearer " + tokenProvider.createAdminFakeToken();
            for (String s : followerId) {
                UsersDTO usersDTO = userClient.getUserInfoById(s,token);
                UsersDTO follower = new UsersDTO();
                follower.setId(usersDTO.getId());
                follower.setFullName(usersDTO.getFullName());
                usersDTOList.add(follower);
            }
            result.setFollowerInOn(usersDTOList);

            return Optional.of(result);
        }
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HCSupportBook : {}", id);
        HCSupportBook hcSupportBook = hCSupportBookRepository.findById(id)
            .orElseThrow(() -> new BusinessException("hcSupportBookNotFound"));
        hcSupportBook.setDeleted(false);
        hCSupportBookRepository.save(hcSupportBook);
    }

    @Override
    public HCSupportBookDTO createSupportBook(String userId, HCSupportBookDTO hcSupportBookDTO) {

        if (userId.equals("Guest")) {
            this.tokenProvider.setFakeAuthentication();
        }

        HCUserDTO hcUserDTO = this.hcUserService.checkExistHCUser(userId);

        hcSupportBookDTO.setContentText(XSSFilter.stripXSS(hcSupportBookDTO.getContentText()));

        // check nguoi ho cho

        HashMap<String, Object>  supporterInOnInfo = DecentralizationUtils.autoMappingSupporterInOn(hcSupportBookDTO.getType(), hcDecentralizationService);

        List<String> userAuthIdList = gatewayClient.getUserAuths((List<String>) supporterInOnInfo.get("userIds"));

        supporterInOnInfo.put("userAuthId", userAuthIdList.get(0));

        UsersDTO supporterInOn = userClient.getUserInfoByIdAuthor(supporterInOnInfo.get("userAuthId").toString(), null);

        // check phan quyen
        Optional<HCDecentralizationDTO> hcDecentralizationDTOOptional = hcDecentralizationRepository
            .findFirstByUserIdAndDepartment(supporterInOnInfo.get("userAuthId").toString(), HCDepartment.valueOf(supporterInOnInfo.get("hcDepartment").toString()))
            .map(hcDecentralizationMapper::toDto);

        if (!hcDecentralizationDTOOptional.isPresent()) {
            throw new BusinessException("hcDecentralizationNotFound");
        }
        HCDecentralizationDTO hcDecentralizationDTO = hcDecentralizationDTOOptional.get();
        hcDecentralizationDTO.setProcessing(hcDecentralizationDTO.getProcessing() == null ? 1 : hcDecentralizationDTO.getProcessing() + 1);
        hcDecentralizationRepository.save(hcDecentralizationMapper.toEntity(hcDecentralizationDTO));

        HCMessageDTO hcMessageDTO = new HCMessageDTO();
        hcMessageDTO.setStartedDate(Instant.now());
        hcMessageDTO.setType(TypeMessage.TEXT);
        hcMessageDTO.setContent(hcSupportBookDTO.getContentText() == null ? null : hcSupportBookDTO.getContentText().getBytes());
        hcMessageDTO.setFromUser(hcUserDTO.getId() + "");

        if (userId == null) {
            hcSupportBookDTO.setCreatedBy("Guest");
        } else {
            hcSupportBookDTO.setCreatedBy(userId);
        }
        hcSupportBookDTO.setCreatedDate(Instant.now());
        hcSupportBookDTO.setTitle(hcSupportBookDTO.getTitleText() == null ? null : hcSupportBookDTO.getTitleText().getBytes());
        hcSupportBookDTO.setContent(hcSupportBookDTO.getContentText() == null ? null : hcSupportBookDTO.getContentText().getBytes());
        hcSupportBookDTO.setFlowerInOnId(DecentralizationUtils.generateFlowerInOn(hcSupportBookDTO.getType(), hcDecentralizationService, gatewayClient));
        hcSupportBookDTO = this.save(hcSupportBookDTO);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMM");
        String codeHC = "CS" + simpleDateFormat.format(Date.from(Instant.now())) + String.format("%03d", hcSupportBookDTO.getId());
        hcSupportBookDTO.setCode(codeHC);
        hcSupportBookDTO.setRoomId(codeHC);
        hcSupportBookDTO.setRoomInOnId(codeHC + "_InOn");
        hcSupportBookDTO.setStatus(HCStatus.TODO);
        hcSupportBookDTO.setSupporterInOnId(supporterInOnInfo.get("userAuthId").toString());
        hcSupportBookDTO.setSupporterInOnIdName(supporterInOn.getFullName());
        hcSupportBookDTO.sethCUserId(hcUserDTO.getId());
        hcSupportBookDTO = this.save(hcSupportBookDTO);


        hcMessageDTO.setIsInOn(false);
        hcMessageDTO.setSendDate(Instant.now());
        hcMessageDTO.setRoomId(codeHC);
        hcMessageDTO.setSendDate(Instant.now());
        hcMessageDTO.sethCSupportBookId(hcSupportBookDTO.getId());
        hcMessageDTO = this.hcMessageService.save(hcMessageDTO);

        try {
            this.sendNotifications(hcSupportBookDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }


        hcSupportBookDTO.setHcMessageId(hcMessageDTO.getId());
        hcSupportBookDTO.setCreatedBy(null);
        hcSupportBookDTO.setUpdateBy(null);

        return hcSupportBookDTO;
    }

    @Override
    public HCSupportBookDTO updateSupportBook(String userId, HCSupportBookDTO hcSupportBookDTO) throws
        InstantiationException, IllegalAccessException {

        Optional<HCSupportBookDTO> hcSupportBookDTOOptional = this.findOne(userId,hcSupportBookDTO.getId());
        if (!hcSupportBookDTOOptional.isPresent()) {
            throw new BusinessException("hcSupportNotFound");
        }

        HCSupportBookDTO oldHCSupportBook = hcSupportBookDTOOptional.get();

        if (!hcSupportBookDTO.gethCUserId().equals(oldHCSupportBook.gethCUserId())) {
            throw new BusinessException("hcSupportNotFound");
        }

        if (hcSupportBookDTO.getFlowerInOnId() != null) {
            List<String> followerIdCurrent = Arrays.asList(oldHCSupportBook.getFlowerInOnId().split(","));
            String[] followerId = hcSupportBookDTO.getFlowerInOnId().split(",");

            for (String s : followerId) {
                if (followerIdCurrent.contains(s)) {
                    try {
                        NotificationsDTO notificationsDTO = new NotificationsDTO();
                        notificationsDTO.setTitle("Theo dõi yêu cầu hỗ trợ");
                        notificationsDTO.setShortContent("Theo dõi yêu cầu hỗ trợ mã số: " + oldHCSupportBook.getCode());
                        notificationsDTO.setContent(("Bạn được thêm vào theo dõi Phiếu yêu cầu hỗ trợ: <b>" + oldHCSupportBook.getCode() + "</b>. Xem chi tiết " + " <a href='" + urlSupportBook + hcSupportBookDTO.getId() + "'" + " target='_blank'>tại đây.</a>").getBytes());
                        notificationsDTO.setUserId(Long.parseLong(s));
                        notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        /*
         * todo chỉ định người xử lý mới.
         *  */
        if (hcSupportBookDTO.getSupporterInOnId() != null && !hcSupportBookDTO.getSupporterInOnId().equals(oldHCSupportBook.getSupporterInOnId())) {
            HCDepartment hcDepartment = DecentralizationUtils.autoSelectDepartment(hcSupportBookDTO.getType());
            Optional<HCDecentralizationDTO> bVH = hcDecentralizationRepository
                .findFirstByUserIdAndDepartment(hcSupportBookDTO.getSupporterInOnId(), hcDepartment)
                .map(hcDecentralizationMapper::toDto);
            if (bVH.isPresent()) {
                try {
                    bVH.get().setProcessing(bVH.get().getProcessing() == null ? 1 : bVH.get().getProcessing() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hcDecentralizationRepository.save(hcDecentralizationMapper.toEntity(bVH.get()));
            }


            Optional<HCDecentralizationDTO> bVHCurrent = hcDecentralizationRepository
                .findFirstByUserIdAndDepartment(oldHCSupportBook.getSupporterInOnId(), hcDepartment)
                .map(hcDecentralizationMapper::toDto);
            if (bVHCurrent.isPresent()) {
                try {
                    if (bVHCurrent.get().getProcessing() == null || bVHCurrent.get().getProcessing() == 0) {
                        bVHCurrent.get().setProcessing(0L);
                    } else {
                        bVHCurrent.get().setProcessing(bVHCurrent.get().getProcessing() - 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                hcDecentralizationRepository.save(hcDecentralizationMapper.toEntity(bVHCurrent.get()));
            }

            try {
                SendEmailDTO sendEmailDTO = new SendEmailDTO();
                HashMap<String, String> mailVariable = new HashMap<>();
                mailVariable.put("code", oldHCSupportBook.getCode());
                mailVariable.put("fullName", oldHCSupportBook.gethCUser().getName());
                mailVariable.put("phoneNumber", oldHCSupportBook.gethCUser().getPhoneNumber());
                mailVariable.put("email", oldHCSupportBook.gethCUser().getEmail());
                mailVariable.put("title", oldHCSupportBook.getTitle());
                mailVariable.put("content", oldHCSupportBook.getContent());
                String pathMySupportBook = urlSupportBook + hcSupportBookDTO.getId();
                mailVariable.put("link", pathMySupportBook);

                sendEmailDTO.setMailVariables(mailVariable);
                ObjectMapper objectMapper = new ObjectMapper();
                log.info("request send email {}", objectMapper.writeValueAsString(sendEmailDTO));
                String userInOnId = hcSupportBookDTO.getSupporterInOnId();
                String token = "Bearer " + tokenProvider.createFakeToken("openapi", false);
                UsersDTO newSupporterInOn = userClient.getUserInfoByIdAuthor(userInOnId, token);

                sendEmailDTO.setMailVariables(mailVariable);
                sendEmailDTO.setReceiverMail(newSupporterInOn.getEmail());
                sendEmailDTO.setTemplate(MailTemplate.INON_NEW_SUPPORT_BOOK_PROCESS);
                sendEmailDTO.setSubject("(InOn) phiếu yêu cầu hỗ trợ " + oldHCSupportBook.getCode() + " được chỉ định.");
                notificationsClient.sendMail(sendEmailDTO);

                /*
                 * todo send notification.
                 *  */
                NotificationsDTO notificationsDTO = new NotificationsDTO();
                notificationsDTO.setTitle("Yêu cầu hỗ trợ mới");
                notificationsDTO.setShortContent("Yêu cầu hỗ trợ mới mã số: " + oldHCSupportBook.getCode());
                notificationsDTO.setContent(("Bạn có yêu cầu mới cần xử lý. Mã số yêu cầu là <b>" + oldHCSupportBook.getCode()
                    + "</b>. Vui lòng " + " <a href='" + urlSupportBook + oldHCSupportBook.getId() + "'"
                    + " target='_blank'>truy cập</a> và xử lý.").getBytes());
                notificationsDTO.setUserId(Long.parseLong(hcSupportBookDTO.getSupporterInOnId()));
                notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        /*
         * todo xử lý thành công.
         *  */
        if (hcSupportBookDTO.getStatus().equals(HCStatus.DONE)) {
            HCDepartment hcDepartment = DecentralizationUtils.autoSelectDepartment(hcSupportBookDTO.getType());
            Optional<HCDecentralizationDTO> bVHCurrent = hcDecentralizationRepository
                .findFirstByUserIdAndDepartment(hcSupportBookDTO.getSupporterInOnId(), hcDepartment)
                .map(hcDecentralizationMapper::toDto);
            if (bVHCurrent.isPresent()) {
                try {
                    if (bVHCurrent.get().getProcessing() == null || bVHCurrent.get().getProcessing() == 0) {
                        bVHCurrent.get().setProcessing(0L);
                    } else {
                        bVHCurrent.get().setProcessing(bVHCurrent.get().getProcessing() - 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                hcDecentralizationRepository.save(hcDecentralizationMapper.toEntity(bVHCurrent.get()));
                /*
                 * todo send email completed support book.
                 *  */
                try {

                    NotificationsDTO notificationsDTO = new NotificationsDTO();
                    notificationsDTO.setTitle("Hoàn tất xử lý yêu cầu hỗ trợ");
                    notificationsDTO.setShortContent("Yêu cầu hỗ trợ <b>" + oldHCSupportBook.getCode() + "</b> đã hoàn tất.");
                    notificationsDTO.setContent(("Yêu cầu hỗ trợ <b>" + oldHCSupportBook.getCode() + "</b> đã hoàn tất. Vui lòng " + " <a href='" + urlSupportBook + hcSupportBookDTO.getId() + "'" + " target='_blank'>Truy cập</a> để đánh giá. Bỏ qua nếu bạn đã thực hiện bước này.").getBytes());
                    notificationsDTO.setUserId(Long.parseLong(hcSupportBookDTO.gethCUser().getUserIdInOn()));
                    notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);

                    SendEmailDTO sendEmailDTO = new SendEmailDTO();
                    sendEmailDTO.setReceiverMail(oldHCSupportBook.gethCUser().getEmail());
                    sendEmailDTO.setTemplate(MailTemplate.INON_NEW_SUPPORT_BOOK_COMPLETE);
                    sendEmailDTO.setSubject(null);
                    HashMap<String, String> mailVariable = new HashMap<>();
                    mailVariable.put("fullName", oldHCSupportBook.gethCUser().getName());
                    mailVariable.put("code", oldHCSupportBook.getCode());
                    mailVariable.put("supportBookId", oldHCSupportBook.getId().toString());
                    mailVariable.put("link", urlSupportBook + oldHCSupportBook.getId());
                    sendEmailDTO.setMailVariables(mailVariable);
                    sendEmailDTO.setSubject("(InOn) phiếu yêu cầu hỗ trợ " + oldHCSupportBook.getCode() + " đã hoàn tất.");
                    this.notificationsClient.sendMail(sendEmailDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        }

        ObjectUtils.mergeObjects(hcSupportBookDTO, oldHCSupportBook);
        hcSupportBookDTO.setContentText(XSSFilter.stripXSS(hcSupportBookDTO.getContentText()));
        hcSupportBookDTO.setUpdatedDate(Instant.now());
        if (userId == null) {
            hcSupportBookDTO.setUpdateBy("Guest");
        } else {
            hcSupportBookDTO.setUpdateBy(userId);
        }
        hcSupportBookDTO.setTitle(hcSupportBookDTO.getTitleText() == null ? null : hcSupportBookDTO.getTitleText().getBytes());
        hcSupportBookDTO.setContent(hcSupportBookDTO.getContentText() == null ? null : hcSupportBookDTO.getContentText().getBytes());
        hcSupportBookDTO = this.save(hcSupportBookDTO);
        return hcSupportBookDTO;
    }

    @Override
    public List<HCSupportBookDTO> findAllByhCUserAndDeletedIsFalse(HCUserDTO hcUserDTO) {
        HCUser hcUser = hcUserMapper.toEntity(hcUserDTO);
        return this.hCSupportBookRepository.findAllByhCUserAndDeletedIsFalse(hcUser)
            .stream()
            .map(hCSupportBookMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<HCSupportBookDTO> findAllMyHCSupportBook(String userId, Pageable pageable) {

        Optional<HCUserDTO> hcUserDTOOptional = this.hcUserService.findOneByUserInOnId(Long.parseLong(userId));
        if (!hcUserDTOOptional.isPresent()) {
            return new ArrayList<>();
        }
        HCUserDTO hcUserDTO = hcUserDTOOptional.get();

        List<HCSupportBookDTO> hcSupportBookDTOList = this.findAllByhCUserAndDeletedIsFalse(hcUserDTO);


        try {
            for (HCSupportBookDTO hcSupportBookDTO : hcSupportBookDTOList) {
                if (hcSupportBookDTO.getFlowerInOnId() != null && !hcSupportBookDTO.getFlowerInOnId().isEmpty()) {
                    List<UsersDTO> usersDTOList = new ArrayList<>();
                    String[] followerId = hcSupportBookDTO.getFlowerInOnId().split(",");
                    for (String s : followerId) {
                        UsersDTO usersDTO = userClient.getUserInfoById(s);
                        UsersDTO follower = new UsersDTO();
                        follower.setId(usersDTO.getId());
                        follower.setFullName(usersDTO.getFullName());
                        usersDTOList.add(follower);
                    }
                    hcSupportBookDTO.setFollowerInOn(usersDTOList);
                }
            }
        } catch (Exception e) {
            log.error(String.valueOf(e));
        }

        return hcSupportBookDTOList;
    }

    @Override
    public List<HCSupportBookDTO> findAllMySupportBookManager(String userId, Pageable pageable) {

        List<HCSupportBookDTO> hcSupportBookDTOList = this.findAll(pageable);
        List<HCSupportBookDTO> result = new ArrayList<>();
        for (HCSupportBookDTO hcSupportBookDTO : hcSupportBookDTOList) {
            List<String> idFlowerInOn = Arrays.asList(hcSupportBookDTO.getFlowerInOnId().split(","));
            if ((idFlowerInOn.contains(userId) || hcSupportBookDTO.getSupporterInOnId().equals(userId) || hcSupportBookDTO.getCreatedBy().equals(userId)) && !hcSupportBookDTO.getDeleted()) {
                result.add(hcSupportBookDTO);
            }
        }

        try {
            for (HCSupportBookDTO hcSupportBookDTO : result) {
                if (hcSupportBookDTO.getFlowerInOnId() != null && !hcSupportBookDTO.getFlowerInOnId().isEmpty()) {
                    List<UsersDTO> usersDTOList = new ArrayList<>();
                    String[] followerId = hcSupportBookDTO.getFlowerInOnId().split(",");
                    for (String s : followerId) {
                        UsersDTO usersDTO = userClient.getUserInfoById(s);
                        UsersDTO follower = new UsersDTO();
                        follower.setId(usersDTO.getId());
                        follower.setFullName(usersDTO.getFullName());
                        usersDTOList.add(follower);
                    }
                    hcSupportBookDTO.setFollowerInOn(usersDTOList);
                }
            }
        } catch (Exception e) {
            log.error(String.valueOf(e));
        }

        return result;
    }

    @Override
    public List<HCSupportBookDTO> findAllBySupporterInOnId(String userId) {
        List<HCSupportBookDTO> result = hCSupportBookRepository
            .findAllBySupporterInOnId(userId)
            .stream()
            .map(hCSupportBookMapper::toDto)
            .collect(Collectors.toList());

        try {
            for (HCSupportBookDTO hcSupportBookDTO : result) {
                if (hcSupportBookDTO.getFlowerInOnId() != null && !hcSupportBookDTO.getFlowerInOnId().isEmpty()) {
                    List<UsersDTO> usersDTOList = new ArrayList<>();
                    String[] followerId = hcSupportBookDTO.getFlowerInOnId().split(",");
                    for (String s : followerId) {
                        String jwt = tokenProvider.createAdminFakeToken();
                        String token = "Bearer " + jwt;
                        UsersDTO usersDTO = userClient.getUserInfoById(s, token);
                        UsersDTO follower = new UsersDTO();
                        follower.setId(usersDTO.getId());
                        follower.setFullName(usersDTO.getFullName());
                        usersDTOList.add(follower);
                    }
                    hcSupportBookDTO.setFollowerInOn(usersDTOList);
                }
            }
        } catch (Exception e) {
            log.error(String.valueOf(e));
        }

        return result;
    }

    @Override
    public List<HCSupportBookDTO> findAllBySupporterInOnIdAndHCStatusIn(String
                                                                            supporterInOnIdn, List<HCStatus> hcStatuses) {
        List<HCSupportBookDTO> result = hCSupportBookRepository.findAllBySupporterInOnIdAndStatusIn(supporterInOnIdn, hcStatuses)
            .stream()
            .map(hCSupportBookMapper::toDto)
            .collect(Collectors.toList());

        try {
            for (HCSupportBookDTO hcSupportBookDTO : result) {
                if (hcSupportBookDTO.getFlowerInOnId() != null && !hcSupportBookDTO.getFlowerInOnId().isEmpty()) {
                    List<UsersDTO> usersDTOList = new ArrayList<>();
                    String[] followerId = hcSupportBookDTO.getFlowerInOnId().split(",");
                    for (String s : followerId) {
                        UsersDTO usersDTO = userClient.getUserInfoById(s);
                        UsersDTO follower = new UsersDTO();
                        follower.setId(usersDTO.getId());
                        follower.setFullName(usersDTO.getFullName());
                        usersDTOList.add(follower);
                    }
                    hcSupportBookDTO.setFollowerInOn(usersDTOList);
                }
            }
        } catch (Exception e) {
            log.error(String.valueOf(e));
        }
        return result;
    }

    @Override
    public Optional<HCSupportBookDTO> findOneByRoomId(String roomId, String userId) {
        Optional<HCSupportBookDTO> hcSupportBookDTOOptional = hCSupportBookRepository
            .findOneByRoomIdOrRoomInOnId(roomId, roomId).map(hCSupportBookMapper::toDto);

        String token = "Bearer " + tokenProvider.createAdminFakeToken();
        if (!hcSupportBookDTOOptional.isPresent()) {
            return Optional.empty();
        } else {
            HCSupportBookDTO result = hcSupportBookDTOOptional.get();

            List<UsersDTO> usersDTOList = new ArrayList<>();
            String[] followerId = result.getFlowerInOnId().split(",");
            for (String s : followerId) {
                UsersDTO usersDTO = userClient.getUserInfoById(s,token);
                UsersDTO follower = new UsersDTO();
                follower.setId(usersDTO.getId());
                follower.setFullName(usersDTO.getFullName());
                usersDTOList.add(follower);
            }
            result.setFollowerInOn(usersDTOList);

            return Optional.of(result);
        }
    }

    @Override
    public List<HCSupportBookDTO> findAllBySupportInOnIdMonth(String supporterInOnId,Instant fromDate, Instant toDate) {
        return hCSupportBookRepository.findAllBySupporterInOnIdAndCreatedDateBetween(supporterInOnId, fromDate, toDate)
            .stream()
            .map(hCSupportBookMapper::toDto)
            .collect(Collectors.toList());
    }


    @Async
    protected void sendNotifications(HCSupportBookDTO hcSupportBookDTO) {

        try {
            /*
             * todo send notification.
             *  */
            NotificationsDTO notificationsDTO = new NotificationsDTO();
            notificationsDTO.setTitle("Yêu cầu hỗ trợ mới");
            notificationsDTO.setShortContent("Yêu cầu hỗ trợ mới mã số: " + hcSupportBookDTO.getCode());
            notificationsDTO.setContent(("Bạn có yêu cầu mới cần xử lý. Mã số yêu cầu là <b>" + hcSupportBookDTO.getCode()
                + "</b>. Vui lòng " + " <a href='" + urlSupportBook + hcSupportBookDTO.getId() + "'"
                + " target='_blank'>truy cập</a> và xử lý.").getBytes());
            notificationsDTO.setUserId(Long.parseLong(hcSupportBookDTO.getSupporterInOnId()));
            notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);

            /*
             * todo send notifications to user.
             *  */
            NotificationsDTO notificationsDTOUser = new NotificationsDTO();
            notificationsDTOUser.setTitle("Tạo mới yêu cầu hỗ trợ thành công");
            notificationsDTOUser.setShortContent("Bạn đã tạo mới thành công yêu cầu hỗ trợ");
            notificationsDTOUser.setContent(("Bạn đã tạo mới thành công yêu cầu hỗ trợ. Mã số yêu cầu hỗ trợ: <b>" + hcSupportBookDTO.getCode() + "</b>. " + " <a href='" + urlSupportBook + hcSupportBookDTO.getId() + "'" + " target='_blank'>Truy cập</a> để xem chi tiết.").getBytes());
            if (hcSupportBookDTO.gethCUser().getUserIdInOn() != null) {
                notificationsDTOUser.setUserId(Long.parseLong(hcSupportBookDTO.gethCUser().getUserIdInOn()));
            } else {
                notificationsDTOUser.setDeviceId(hcSupportBookDTO.gethCUser().getDeviceId());
            }
            notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTOUser);

            /*
             * todo send notifications to flower InOn.
             *  */
//            String[] flowerIds = hcSupportBookDTO.getFlowerInOnId().split(",");
//            for (String flowerId : flowerIds) {
//                NotificationsDTO notificationsDTOFlower = new NotificationsDTO();
//                if (flowerId.equals(hcSupportBookDTO.getSupporterInOnId())) {
//                    continue;
//                }
//                notificationsDTOFlower.setTitle("Theo dõi yêu cầu hỗ trợ");
//                notificationsDTOFlower.setShortContent("Theo dõi yêu cầu hỗ trợ số: "+hcSupportBookDTO.getCode());
//                notificationsDTOFlower.setContent(("Bạn được thêm vào theo dõi Phiếu yêu cầu hỗ trợ <b>" + hcSupportBookDTO.getCode() + "</b>. Xem chi tiết " + " <a href='https://sit2.inon.vn/app/support/chat/" + hcSupportBookDTO.getId() + "'" + " target='_blank'>tại</a>").getBytes());
//                notificationsDTOFlower.setUserId(Long.parseLong(flowerId));
//                notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTOFlower);
//
//            }

            /*
             * todo send email to create support book.
             *  */
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setReceiverMail(hcSupportBookDTO.gethCUser().getEmail());
            sendEmailDTO.setTemplate(MailTemplate.INON_NEW_SUPPORT_BOOK);
            HashMap<String, String> mailVariable = new HashMap<>();
            mailVariable.put("code", hcSupportBookDTO.getCode());
            mailVariable.put("fullName", hcSupportBookDTO.gethCUser().getName());
            mailVariable.put("phoneNumber", hcSupportBookDTO.gethCUser().getPhoneNumber());
            mailVariable.put("email", hcSupportBookDTO.gethCUser().getEmail());
            mailVariable.put("title", hcSupportBookDTO.getTitle());
            mailVariable.put("content", hcSupportBookDTO.getContent());
            String pathMySupportBook = urlSupportBook + hcSupportBookDTO.getId();
            mailVariable.put("link", pathMySupportBook);
            sendEmailDTO.setMailVariables(mailVariable);
            ObjectMapper objectMapper = new ObjectMapper();
            log.info("request send email {}", objectMapper.writeValueAsString(sendEmailDTO));
            sendEmailDTO.setSubject("(InOn) phiếu yêu cầu hỗ trợ " + hcSupportBookDTO.getCode() + " được mở.");
            notificationsClient.sendMail(sendEmailDTO);

            /*
             * todo send email to InOn supporter.
             *  */
            String userInOnId = hcSupportBookDTO.getSupporterInOnId();
            String token = "Bearer " + tokenProvider.createFakeToken("openapi", false);
            UsersDTO bVH = userClient.getUserInfoByIdAuthor(userInOnId, token);

            sendEmailDTO.setMailVariables(mailVariable);
            sendEmailDTO.setReceiverMail(bVH.getEmail());
            sendEmailDTO.setTemplate(MailTemplate.INON_NEW_SUPPORT_BOOK_PROCESS);
            sendEmailDTO.setSubject("(InOn) phiếu yêu cầu hỗ trợ " + hcSupportBookDTO.getCode() + " được chỉ định.");
            notificationsClient.sendMail(sendEmailDTO);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package nth.module.utilities.service.impl;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import nth.lib.integration.model.utilities.NotificationsDTO;
import nth.module.utilities.client.NotificationsClient;
import nth.module.utilities.domain.HCMessage;
import nth.module.utilities.repository.HCMessageRepository;
import nth.module.utilities.service.HCMessageService;
import nth.module.utilities.service.HCSupportBookService;
import nth.module.utilities.service.dto.HCMessageDTO;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.service.mapper.HCMessageMapper;
import nth.module.utilities.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HCMessage}.
 */
@Service
@Transactional
public class HCMessageServiceImpl implements HCMessageService {

    private static final Logger log = LoggerFactory.getLogger(HCMessageServiceImpl.class);

    private final HCMessageRepository hCMessageRepository;
    private final HCMessageMapper hCMessageMapper;
    private final HCSupportBookService hcSupportBookService;
    private final NotificationsClient notificationsClient;

    public HCMessageServiceImpl(HCMessageRepository hCMessageRepository, HCMessageMapper hCMessageMapper, HCSupportBookService hcSupportBookService, NotificationsClient notificationsClient) {
        this.hCMessageRepository = hCMessageRepository;
        this.hCMessageMapper = hCMessageMapper;
        this.hcSupportBookService = hcSupportBookService;
        this.notificationsClient = notificationsClient;
    }

    @Override
    public HCMessageDTO save(HCMessageDTO hCMessageDTO) {
        log.debug("Request to save HCMessage : {}", hCMessageDTO);
        HCMessage hCMessage = hCMessageMapper.toEntity(hCMessageDTO);
        hCMessage = hCMessageRepository.save(hCMessage);
        return hCMessageMapper.toDto(hCMessage);
    }

    @Override
    public Optional<HCMessageDTO> partialUpdate(HCMessageDTO hCMessageDTO) {
        log.debug("Request to partially update HCMessage : {}", hCMessageDTO);

        return hCMessageRepository
            .findById(hCMessageDTO.getId())
            .map(existingHCMessage -> {
                hCMessageMapper.partialUpdate(existingHCMessage, hCMessageDTO);

                return existingHCMessage;
            })
            .map(hCMessageRepository::save)
            .map(hCMessageMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HCMessageDTO> findAll() {
        log.debug("Request to get all HCMessages");
        return hCMessageRepository.findAll().stream().map(hCMessageMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HCMessageDTO> findOne(Long id) {
        log.debug("Request to get HCMessage : {}", id);
        return hCMessageRepository.findById(id).map(hCMessageMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HCMessage : {}", id);
        hCMessageRepository.deleteById(id);
    }

    @Override
    public HCMessageDTO createMessage(HCMessageDTO hcMessageDTO) {
        hcMessageDTO.setContent(hcMessageDTO.getContentText() == null ? null : hcMessageDTO.getContentText().getBytes());
        hcMessageDTO.setSendDate(Instant.now());
        hcMessageDTO.setStartedDate(Instant.now());
        hcMessageDTO = this.save(hcMessageDTO);
        return hcMessageDTO;
    }

    @Async
    @Override
    public void createMessageAsync(HCMessageDTO hcMessageDTO, String currentUser) {
        try {
            HCSupportBookDTO hcSupportBookDTO = hcSupportBookService.findOneByRoomId(hcMessageDTO.getRoomId(), currentUser).get();
            List<String> userIdList = new ArrayList<>();
            if (hcMessageDTO.getContentText() != null) {
                String[] words = hcMessageDTO.getContentText().split("]\\(");
                String userName = hcMessageDTO.getFromUser().split("___")[1];
                for (String word : words) {
                    try{
                        String userId = word.split(":")[1].split("\\)")[0];
                        if (userIdList.contains(userId)) {
                            continue;
                        }
                        userIdList.add(userId);
                        NotificationsDTO notificationsDTO = new NotificationsDTO();
                        notificationsDTO.setUserId(Long.valueOf(userId));
                        notificationsDTO.setTitle("Bạn được đề cập trong tin nhắn mới");
                        notificationsDTO.setShortContent(userName + " nhắc đến bạn");
                        notificationsDTO.setContent((userName + " đã nhắc đến bạn trong yêu cầu hỗ trợ " + hcSupportBookDTO.getCode()).getBytes());
                        notificationsClient.sendNotificationWhenHaveSupportBookNeedProcess(notificationsDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        hcMessageDTO.setContent(hcMessageDTO.getContentText() == null ? null : hcMessageDTO.getContentText().getBytes());
        hcMessageDTO.setSendDate(Instant.now());
        hcMessageDTO.setStartedDate(Instant.now());
        this.save(hcMessageDTO);
    }

    @Async
    @Override
    public void updateMessage(HCMessageDTO hcMessageDTO) throws InstantiationException, IllegalAccessException {
        Optional<HCMessageDTO> oldHCMessageDTOOptional = this.findOne(hcMessageDTO.getId());
        ObjectUtils.mergeObjects(hcMessageDTO, oldHCMessageDTOOptional);
        hcMessageDTO.setReceiveDate(Instant.now());
        this.save(hcMessageDTO);
    }

    @Override
    public List<HCMessageDTO> findAllByRoomId(String roomId, Pageable pageable) {
        List<HCMessageDTO> hcMessageDTOList =  hCMessageRepository.findAllByRoomId(roomId, pageable)
            .stream()
            .map(hCMessageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));



        return null;
    }

    @Async
    @Override
    public void checkingMessage(String userId, String roomId) {
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE);
        List<HCMessageDTO> hcMessageDTOList = this.findAllByRoomId(roomId, pageable);
        hcMessageDTOList.forEach(hcMessageDTO -> {
            if (hcMessageDTO.getReaderId() == null) {
                hcMessageDTO.setReaderId(userId);
            } else {
                List<String> readerId = Arrays.asList(hcMessageDTO.getReaderId().split(","));
                if (userId.equals(hcMessageDTO.getReaderId()) || readerId.contains(userId)) {
                    return;
                }
                hcMessageDTO.setReaderId(hcMessageDTO.getReaderId() + "," + userId);
            }
            hcMessageDTO.setUpdatedDate(Instant.now());
            this.save(hcMessageDTO);
        });
    }

}

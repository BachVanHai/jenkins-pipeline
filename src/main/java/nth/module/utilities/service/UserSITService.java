package nth.module.utilities.service;

import nth.module.utilities.service.dto.UserSITDTO;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface UserSITService {

    Optional<UserSITDTO> findFirstByUserNameAndPassword(String userName, String password,String deviceId);
    Optional<UserSITDTO> findFirstByUserName(String username);

}

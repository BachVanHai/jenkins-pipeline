package nth.module.utilities.service;

import java.util.List;
import java.util.Optional;
import nth.module.utilities.service.dto.HCMessageDTO;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.HCMessage}.
 */
public interface HCMessageService {
    /**
     * Save a hCMessage.
     *
     * @param hCMessageDTO the entity to save.
     * @return the persisted entity.
     */
    HCMessageDTO save(HCMessageDTO hCMessageDTO);

    /**
     * Partially updates a hCMessage.
     *
     * @param hCMessageDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<HCMessageDTO> partialUpdate(HCMessageDTO hCMessageDTO);

    /**
     * Get all the hCMessages.
     *
     * @return the list of entities.
     */
    List<HCMessageDTO> findAll();

    /**
     * Get the "id" hCMessage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HCMessageDTO> findOne(Long id);

    /**
     * Delete the "id" hCMessage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //todo save message.
    HCMessageDTO createMessage(HCMessageDTO hcMessageDTO);

    /*
    * todo create message async.
    *  */
    void createMessageAsync(HCMessageDTO hcMessageDTO, String userId);

    //todo update message.
    void updateMessage (HCMessageDTO hcMessageDTO) throws InstantiationException, IllegalAccessException;

    List<HCMessageDTO> findAllByRoomId(String roomId, Pageable pageable);


    /*
    * todo checking message.
    *  */
    void checkingMessage(String userId, String roomId);
}

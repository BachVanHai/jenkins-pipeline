package nth.module.utilities.service;

import java.util.List;
import java.util.Optional;

import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.CategoryQuestion}.
 */
public interface CategoryQuestionService {
    /**
     * Save a categoryQuestion.
     *
     * @param categoryQuestionDTO the entity to save.
     * @return the persisted entity.
     */
    CategoryQuestionDTO save(CategoryQuestionDTO categoryQuestionDTO);

    /**
     * Get all the categoryQuestions.
     *
     * @return the list of entities.
     */
    List<CategoryQuestionDTO> findAll(Pageable pageable);

    List<CategoryQuestionDTO> findAllCategoryQuestionByUserId(String userId, Pageable pageable, UserClient userClient);

    /**
     * Get the "id" categoryQuestion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CategoryQuestionDTO> findOne(Long id);


    Optional<CategoryQuestionDTO> findOneCategoryQuestionByUserId(String userId,Long id, UserClient userClient);


    /**
     * Delete the "id" categoryQuestion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /*
    * find all category question.
    * */
    List<CategoryQuestionDTO> findAllCategoryQuestionByType(CategoryQuestionType categoryQuestionType, Pageable pageable);
}

package nth.module.utilities.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link nth.module.utilities.domain.HCFileUpload} entity.
 */
public class HCFileUploadDTO implements Serializable {

    private Long id;

    @Size(max = 1000)
    private String filePathId;

    @Size(max = 1000)
    private String filePath;

    private Instant createdDate;

    private String createdBy;

    private Instant updatedDate;

    private String updateBy;

    private Long hCMessageId;

    private HCMessageDTO hCMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long gethCMessageId() {
        return hCMessageId;
    }

    public void sethCMessageId(Long hCMessageId) {
        this.hCMessageId = hCMessageId;
    }

    public String getFilePathId() {
        return filePathId;
    }

    public void setFilePathId(String filePathId) {
        this.filePathId = filePathId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public HCMessageDTO gethCMessage() {
        return hCMessage;
    }

    public void sethCMessage(HCMessageDTO hCMessage) {
        this.hCMessage = hCMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCFileUploadDTO)) {
            return false;
        }

        HCFileUploadDTO hCFileUploadDTO = (HCFileUploadDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hCFileUploadDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCFileUploadDTO{" +
            "id=" + getId() +
            ", filePathId='" + getFilePathId() + "'" +
            ", filePath='" + getFilePath() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", hCMessage=" + gethCMessage() +
            "}";
    }
}

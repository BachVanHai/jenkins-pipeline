package nth.module.utilities.service.dto;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Lob;

import nth.lib.common.utils.XSSFilter;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.domain.enumeration.HCPriorityLevel;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.domain.enumeration.HCSupportType;
import nth.module.utilities.utils.IgnoreUpdate;

/**
 * A DTO for the {@link nth.module.utilities.domain.HCSupportBook} entity.
 */
public class HCSupportBookDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String code;

    @IgnoreUpdate
    private String roomId;

    @IgnoreUpdate
    private String roomInOnId;

    @IgnoreUpdate
    private String supporterInOnId;

    @IgnoreUpdate
    private String supporterInOnIdName;

    @IgnoreUpdate
    private HCStatus status;

    @IgnoreUpdate
    @Lob
    private byte[] title;

    @IgnoreUpdate
    private String titleText;

    @IgnoreUpdate
    private String titleContentType;

    @IgnoreUpdate
    @Lob
    private byte[] content;

    @IgnoreUpdate
    private String contentText;

    @IgnoreUpdate
    private String contentContentType;

    @IgnoreUpdate
    private HCPriorityLevel priority;

    @IgnoreUpdate
    private String fromChannel;

    @IgnoreUpdate
    private Instant createdDate;

    @IgnoreUpdate
    private String createdBy;

    @IgnoreUpdate
    private Instant updatedDate;

    @IgnoreUpdate
    private String updateBy;

    @IgnoreUpdate
    private Long hCUserId;

    @IgnoreUpdate
    private HCUserDTO hCUser;

    @IgnoreUpdate
    private Long hcMessageId;

    @IgnoreUpdate
    private String deviceId;

    @IgnoreUpdate
    Set<HCMessageDTO> hcMessageDTOList;

    @IgnoreUpdate
    private HCSupportType type;

    @IgnoreUpdate
    private Boolean deleted;

    @IgnoreUpdate
    private String flowerInOnId;

    @IgnoreUpdate
    private String voteScore;

    @IgnoreUpdate
    private String note;

    @IgnoreUpdate
    private String createByName;

    @IgnoreUpdate
    private List<UsersDTO> followerInOn;

    public List<UsersDTO> getFollowerInOn() {
        return followerInOn;
    }

    public void setFollowerInOn(List<UsersDTO> followerInOn) {
        this.followerInOn = followerInOn;
    }

    public String getCreateByName() {
        return createByName;
    }

    public void setCreateByName(String createByName) {
        this.createByName = createByName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getVoteScore() {
        return voteScore;
    }

    public void setVoteScore(String voteScore) {
        this.voteScore = voteScore;
    }

    public String getFlowerInOnId() {
        return flowerInOnId;
    }

    public void setFlowerInOnId(String flowerInOnId) {
        this.flowerInOnId = flowerInOnId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Set<HCMessageDTO> getHcMessageDTOList() {
        return hcMessageDTOList;
    }

    public void setHcMessageDTOList(Set<HCMessageDTO> hcMessageDTOList) {
        this.hcMessageDTOList = hcMessageDTOList;
    }

    public HCSupportType getType() {
        return type;
    }

    public void setType(HCSupportType type) {
        this.type = type;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getHcMessageId() {
        return hcMessageId;
    }

    public void setHcMessageId(Long hcMessageId) {
        this.hcMessageId = hcMessageId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setRoomInOnId(String roomInOnId) {
        this.roomInOnId = roomInOnId;
    }

    public String getTitleText() {
        return XSSFilter.stripXSS(this.titleText);
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getContentText() {
        return XSSFilter.stripXSS(this.contentText);
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getRoomInOnId() {
        return roomInOnId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSupporterInOnId() {
        return supporterInOnId;
    }

    public void setSupporterInOnId(String supporterInOnId) {
        this.supporterInOnId = supporterInOnId;
    }

    public String getSupporterInOnIdName() {
        return supporterInOnIdName;
    }

    public void setSupporterInOnIdName(String supporterInOnIdName) {
        this.supporterInOnIdName = supporterInOnIdName;
    }

    public HCStatus getStatus() {
        return status;
    }

    public void setStatus(HCStatus status) {
        this.status = status;
    }

    public String getTitle() {
        return this.title == null ? null : XSSFilter.stripXSS(new String(this.title, StandardCharsets.UTF_8));
    }

    public void setTitle(byte[] title) {
        this.title = title;
    }

    public String getTitleContentType() {
        return titleContentType;
    }

    public void setTitleContentType(String titleContentType) {
        this.titleContentType = titleContentType;
    }

    public String getContent() {
        return this.content == null ? null : XSSFilter.stripXSS(new String(this.content, StandardCharsets.UTF_8));
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public HCPriorityLevel getPriority() {
        return priority;
    }

    public void setPriority(HCPriorityLevel priority) {
        this.priority = priority;
    }

    public String getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(String fromChannel) {
        this.fromChannel = fromChannel;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public HCUserDTO gethCUser() {
        return hCUser;
    }

    public void sethCUser(HCUserDTO hCUser) {
        this.hCUser = hCUser;
    }

    public Long gethCUserId() {
        return hCUserId;
    }

    public void sethCUserId(Long hCUserId) {
        this.hCUserId = hCUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCSupportBookDTO)) {
            return false;
        }

        HCSupportBookDTO hCSupportBookDTO = (HCSupportBookDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hCSupportBookDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCSupportBookDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", roomId=" + getRoomId() +
            ", roomInOnId=" + getRoomInOnId() +
            ", supporterInOnId='" + getSupporterInOnId() + "'" +
            ", supporterInOnIdName='" + getSupporterInOnIdName() + "'" +
            ", status='" + getStatus() + "'" +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", priority='" + getPriority() + "'" +
            ", fromChannel='" + getFromChannel() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", hCUser=" + gethCUser() +
            "}";
    }
}

package nth.module.utilities.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.persistence.Lob;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import nth.module.utilities.utils.AlwaysUpdate;
import nth.module.utilities.utils.IgnoreUpdate;

/**
 * A DTO for the {@link nth.module.utilities.domain.Question} entity.
 */
public class QuestionDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String code;

    @IgnoreUpdate
    private String question;

    @IgnoreUpdate
    private String resultText;

    @IgnoreUpdate
    @Lob
    private byte[] rsText;

    @IgnoreUpdate
    private String rsTextContentType;

    @IgnoreUpdate
    private String rsLinkPDF;

    @IgnoreUpdate
    private String rsLinkYT;

    @IgnoreUpdate
    private ApplyFor applyFor;

    @IgnoreUpdate
    private String reasonReject;

    @IgnoreUpdate
    private StatusQuestion status;

    @IgnoreUpdate
    private CategoryQuestionType categoryQuestionType;

    @AlwaysUpdate
    private String sendTo;

    @IgnoreUpdate
    private Boolean deleted;

    @IgnoreUpdate
    private Instant createdDate;

    @IgnoreUpdate
    private Instant enableDate;

    @IgnoreUpdate
    private String createdBy;

    @IgnoreUpdate
    private Instant updateDate;

    @IgnoreUpdate
    private String updateBy;

    @IgnoreUpdate
    private Long categoryQuestionId;

    @IgnoreUpdate
    private CategoryQuestionDTO categoryQuestion;

    @IgnoreUpdate
    private String categoryQuestionName;

    public Instant getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Instant enableDate) {
        this.enableDate = enableDate;
    }

    public String getCategoryQuestionName() {
        return categoryQuestionName;
    }

    public void setCategoryQuestionName(String categoryQuestionName) {
        this.categoryQuestionName = categoryQuestionName;
    }

    public CategoryQuestionType getCategoryQuestionType() {
        return categoryQuestionType;
    }

    public void setCategoryQuestionType(CategoryQuestionType categoryQuestionType) {
        this.categoryQuestionType = categoryQuestionType;
    }

    public byte[] getRsText() {
        return rsText;
    }

    public void setRsText(byte[] rsText) {
        this.rsText = rsText;
    }

    public Long getCategoryQuestionId() {
        return categoryQuestionId;
    }

    public void setCategoryQuestionId(Long categoryQuestionId) {
        this.categoryQuestionId = categoryQuestionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getRsTextContentType() {
        return rsTextContentType;
    }

    public void setRsTextContentType(String rsTextContentType) {
        this.rsTextContentType = rsTextContentType;
    }

    public String getRsLinkPDF() {
        return rsLinkPDF;
    }

    public void setRsLinkPDF(String rsLinkPDF) {
        this.rsLinkPDF = rsLinkPDF;
    }

    public String getRsLinkYT() {
        return rsLinkYT;
    }

    public void setRsLinkYT(String rsLinkYT) {
        this.rsLinkYT = rsLinkYT;
    }

    public ApplyFor getApplyFor() {
        return applyFor;
    }

    public void setApplyFor(ApplyFor applyFor) {
        this.applyFor = applyFor;
    }

    public String getReasonReject() {
        return reasonReject;
    }

    public void setReasonReject(String reasonReject) {
        this.reasonReject = reasonReject;
    }

    public StatusQuestion getStatus() {
        return status;
    }

    public void setStatus(StatusQuestion status) {
        this.status = status;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public CategoryQuestionDTO getCategoryQuestion() {
        return categoryQuestion;
    }

    public void setCategoryQuestion(CategoryQuestionDTO categoryQuestion) {
        this.categoryQuestion = categoryQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuestionDTO)) {
            return false;
        }

        QuestionDTO questionDTO = (QuestionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, questionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + getId() +
            ", name='" + getCode() + "'" +
            ", questTion='" + getQuestion() + "'" +
            ", rsText='" + getResultText() + "'" +
            ", rsLinkPDF='" + getRsLinkPDF() + "'" +
            ", rsLinkYT='" + getRsLinkYT() + "'" +
            ", applyFor='" + getApplyFor() + "'" +
            ", reasonReject='" + getReasonReject() + "'" +
            ", status='" + getStatus() + "'" +
            ", sendTo='" + getSendTo() + "'" +
            ", deleted='" + getDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", categoryQuestion=" + getCategoryQuestion() +
            "}";
    }
}

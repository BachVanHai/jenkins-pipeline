package nth.module.utilities.service.dto;

import nth.module.utilities.utils.IgnoreUpdate;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link nth.module.utilities.domain.HCUser} entity.
 */
public class HCUserDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String userIdInOn;

    @IgnoreUpdate
    private String name;

    @IgnoreUpdate
    private String phoneNumber;

    @IgnoreUpdate
    private String email;

    @IgnoreUpdate
    private Instant createdDate;

    @IgnoreUpdate
    private String createdBy;

    @IgnoreUpdate
    private Instant updatedDate;

    @IgnoreUpdate
    private String updateBy;

    @IgnoreUpdate
    private String deviceId;

    @IgnoreUpdate
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserIdInOn() {
        return userIdInOn;
    }

    public void setUserIdInOn(String userIdInOn) {
        this.userIdInOn = userIdInOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCUserDTO)) {
            return false;
        }

        HCUserDTO hCUserDTO = (HCUserDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hCUserDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCUserDTO{" +
            "id=" + getId() +
            ", userIdInOn='" + getUserIdInOn() + "'" +
            ", name='" + getName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

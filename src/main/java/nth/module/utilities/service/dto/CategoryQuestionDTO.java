package nth.module.utilities.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.utils.IgnoreUpdate;

/**
 * A DTO for the {@link nth.module.utilities.domain.CategoryQuestion} entity.
 */
public class CategoryQuestionDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String name;

    @IgnoreUpdate
    private CategoryQuestionType categoryQuestionType;

    @IgnoreUpdate
    private Instant createdDate;

    @IgnoreUpdate
    private String createdBy;

    @IgnoreUpdate
    private Instant updateDate;

    @IgnoreUpdate
    private String updateBy;

    private List<QuestionDTO> questionDTOList = new ArrayList<>();

    public List<QuestionDTO> getQuestionDTOList() {
        return questionDTOList;
    }

    public void setQuestionDTOList(List<QuestionDTO> questionDTOList) {
        this.questionDTOList = questionDTOList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryQuestionType getCategoryQuestionType() {
        return categoryQuestionType;
    }

    public void setCategoryQuestionType(CategoryQuestionType categoryQuestionType) {
        this.categoryQuestionType = categoryQuestionType;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryQuestionDTO)) {
            return false;
        }

        CategoryQuestionDTO categoryQuestionDTO = (CategoryQuestionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, categoryQuestionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategoryQuestionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", categoryQuestionType='" + getCategoryQuestionType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

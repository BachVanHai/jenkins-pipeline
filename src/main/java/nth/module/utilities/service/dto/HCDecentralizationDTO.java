package nth.module.utilities.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCSupportType;
import nth.module.utilities.utils.IgnoreUpdate;

/**
 * A DTO for the {@link nth.module.utilities.domain.HCDecentralization} entity.
 */
public class HCDecentralizationDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String userId;

    @IgnoreUpdate
    private String userInOnName;

    @IgnoreUpdate
    private HCDecentralizationType decentralization;

    @IgnoreUpdate
    private Instant createdDate;

    @IgnoreUpdate
    private String createdBy;

    @IgnoreUpdate
    private Instant updatedDate;

    @IgnoreUpdate
    private String updateBy;

    @IgnoreUpdate
    private HCSupportType hcSupportType;

    @IgnoreUpdate
    private HCDepartment department;

    @IgnoreUpdate
    private Long processing;

    @IgnoreUpdate
    private Boolean enable;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getUserInOnName() {
        return userInOnName;
    }

    public void setUserInOnName(String userInOnName) {
        this.userInOnName = userInOnName;
    }

    public Long getProcessing() {
        return processing;
    }

    public void setProcessing(Long processing) {
        this.processing = processing;
    }

    public HCDepartment getDepartment() {
        return department;
    }

    public void setDepartment(HCDepartment department) {
        this.department = department;
    }

    public HCSupportType getHcSupportType() {
        return hcSupportType;
    }

    public void setHcSupportType(HCSupportType hcSupportType) {
        this.hcSupportType = hcSupportType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public HCDecentralizationType getDecentralization() {
        return decentralization;
    }

    public void setDecentralization(HCDecentralizationType decentralization) {
        this.decentralization = decentralization;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCDecentralizationDTO)) {
            return false;
        }

        HCDecentralizationDTO hCDecentralizationDTO = (HCDecentralizationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hCDecentralizationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCDecentralizationDTO{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", decentralization='" + getDecentralization() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

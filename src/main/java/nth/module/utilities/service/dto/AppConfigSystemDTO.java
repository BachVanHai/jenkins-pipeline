package nth.module.utilities.service.dto;

import nth.module.utilities.utils.IgnoreUpdate;

public class AppConfigSystemDTO {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    private String key;

    @IgnoreUpdate
    private String value;

    @IgnoreUpdate
    private Boolean enable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}

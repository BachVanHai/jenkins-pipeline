package nth.module.utilities.service.dto;


import java.time.Instant;

public class UserSITDTO {

    private Long id;

    private String userName;

    private String password;

    private Instant createdDate;

    private boolean enable;

    private String createdBy;

    private Instant updateDate;

    private String updateBy;

    private Instant lastLoginSuccess;

    private String lastLoginSuccessDeviceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Instant getLastLoginSuccess() {
        return lastLoginSuccess;
    }

    public void setLastLoginSuccess(Instant lastLoginSuccess) {
        this.lastLoginSuccess = lastLoginSuccess;
    }

    public String getLastLoginSuccessDeviceId() {
        return lastLoginSuccessDeviceId;
    }

    public void setLastLoginSuccessDeviceId(String lastLoginSuccessDeviceId) {
        this.lastLoginSuccessDeviceId = lastLoginSuccessDeviceId;
    }
}

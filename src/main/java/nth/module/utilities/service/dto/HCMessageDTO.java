package nth.module.utilities.service.dto;

import nth.lib.common.utils.XSSFilter;
import nth.module.utilities.domain.enumeration.TypeMessage;
import nth.module.utilities.utils.IgnoreUpdate;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link nth.module.utilities.domain.HCMessage} entity.
 */
public class HCMessageDTO implements Serializable {

    @IgnoreUpdate
    private Long id;

    @IgnoreUpdate
    @Lob
    private byte[] content;

    @IgnoreUpdate
    private String contentText;

    @IgnoreUpdate
    private String contentContentType;

    @IgnoreUpdate
    private String fromUser;

    @IgnoreUpdate
    private String toUser;

    @IgnoreUpdate
    private Boolean isInOn;

    @IgnoreUpdate
    private Instant sendDate;

    @IgnoreUpdate
    private String roomId;

    @IgnoreUpdate
    private Instant receiveDate;

    @IgnoreUpdate
    private Instant startedDate;

    @IgnoreUpdate
    private Instant endDate;

    @IgnoreUpdate
    private Long hCSupportBookId;

    @IgnoreUpdate
    private HCSupportBookDTO hCSupportBook;

    @IgnoreUpdate
    private String readerId;

    @IgnoreUpdate
    private TypeMessage type;

    @IgnoreUpdate
    private Long notification;

    @IgnoreUpdate
    private Instant updatedDate;

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getNotification() {
        return notification;
    }

    public void setNotification(Long notification) {
        this.notification = notification;
    }

    public TypeMessage getType() {
        return type;
    }

    public void setType(TypeMessage type) {
        this.type = type;
    }

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public Long gethCSupportBookId() {
        return hCSupportBookId;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public void sethCSupportBookId(Long hCSupportBookId) {
        this.hCSupportBookId = hCSupportBookId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return this.content == null ? null : XSSFilter.stripXSS(new String (this.content, StandardCharsets.UTF_8));
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public void setContentContentType(String contentContentType) {
        XSSFilter.stripXSS(this.contentContentType = contentContentType);
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public Boolean getIsInOn() {
        return isInOn;
    }

    public void setIsInOn(Boolean isInOn) {
        this.isInOn = isInOn;
    }

    public Instant getSendDate() {
        return sendDate;
    }

    public void setSendDate(Instant sendDate) {
        this.sendDate = sendDate;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Instant getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Instant receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Instant getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Instant startedDate) {
        this.startedDate = startedDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public HCSupportBookDTO gethCSupportBook() {
        return hCSupportBook;
    }

    public void sethCSupportBook(HCSupportBookDTO hCSupportBook) {
        this.hCSupportBook = hCSupportBook;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCMessageDTO)) {
            return false;
        }

        HCMessageDTO hCMessageDTO = (HCMessageDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hCMessageDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCMessageDTO{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", fromUser='" + getFromUser() + "'" +
            ", toUser='" + getToUser() + "'" +
            ", isInOn='" + getIsInOn() + "'" +
            ", sendDate='" + getSendDate() + "'" +
            ", roomId='" + getRoomId() + "'" +
            ", receiveDate='" + getReceiveDate() + "'" +
            ", startedDate='" + getStartedDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", hCSupportBook=" + gethCSupportBook() +
            "}";
    }
}

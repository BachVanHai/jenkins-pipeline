package nth.module.utilities.service;

import nth.lib.integration.model.ContractDTO;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.util.List;

public interface UtilitiesContractService {

    void checkPaidContract(String contractCode, BigDecimal totalFeeTranserred);

    List<ContractDTO> finAllExpiredContract() throws IOException, ParseException;
}

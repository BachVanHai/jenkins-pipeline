package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.UserSIT;
import nth.module.utilities.service.dto.UserSITDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface UserSITMapper extends EntityMapper<UserSITDTO, UserSIT> {

    @Override
    List<UserSIT> toEntity(List<UserSITDTO> dtoList);

    @Override
    List<UserSITDTO> toDto(List<UserSIT> entityList);

    default UserSITDTO fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserSITDTO userSITDTO = new UserSITDTO();
        userSITDTO.setId(id);
        return userSITDTO;
    }
}

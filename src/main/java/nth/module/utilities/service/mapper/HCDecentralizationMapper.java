package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link HCDecentralization} and its DTO {@link HCDecentralizationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HCDecentralizationMapper extends EntityMapper<HCDecentralizationDTO, HCDecentralization> {

    @Override
    HCDecentralizationDTO toDto(HCDecentralization entity);
}

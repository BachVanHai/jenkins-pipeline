package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.AppConfigSystem;
import nth.module.utilities.service.dto.AppConfigSystemDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface AppConfigSystemMapper extends EntityMapper<AppConfigSystemDTO, AppConfigSystem>{

    @Override
    AppConfigSystemDTO toDto(AppConfigSystem entity);

    @Override
    List<AppConfigSystem> toEntity(List<AppConfigSystemDTO> dtoList);

    default AppConfigSystem fromId (Long id) {
        AppConfigSystem appConfigSystem = new AppConfigSystem();
        if (id == null) {
            return null ;
        }
        appConfigSystem.setId(id);
        return appConfigSystem;
    }
}

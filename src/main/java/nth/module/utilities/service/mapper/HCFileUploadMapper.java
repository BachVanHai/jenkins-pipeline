package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.HCFileUpload;
import nth.module.utilities.service.dto.HCFileUploadDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link HCFileUpload} and its DTO {@link HCFileUploadDTO}.
 */
@Mapper(componentModel = "spring", uses = { HCMessageMapper.class })
public interface HCFileUploadMapper extends EntityMapper<HCFileUploadDTO, HCFileUpload> {

    @Mapping(target = "hCMessageId", source = "HCMessage.id")
    @Override
    HCFileUploadDTO toDto(HCFileUpload s);

    @Mapping(target = "hCMessage.id", source = "hCMessageId")
    @Override
    HCFileUpload toEntity(HCFileUploadDTO dto);

    default HCFileUpload fromId (Long id ){
        if (id == null) {
            return null;
        }
        HCFileUpload hcFileUpload = new HCFileUpload();
        hcFileUpload.setId(id);
        return hcFileUpload;
    }
}

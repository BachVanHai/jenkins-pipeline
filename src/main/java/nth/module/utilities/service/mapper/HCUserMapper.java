package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.HCSupportBook;
import nth.module.utilities.domain.HCUser;
import nth.module.utilities.service.dto.HCUserDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link HCUser} and its DTO {@link HCUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HCUserMapper extends EntityMapper<HCUserDTO, HCUser> {


    @Override
    HCUserDTO toDto(HCUser entity);

    @Override
    HCUser toEntity(HCUserDTO dto);

    default HCUser fromId (Long id){
        if (id == null) {
            return null;
        }
        HCUser hcUser = new HCUser();
        hcUser.setId(id);
        return hcUser;
    }
}

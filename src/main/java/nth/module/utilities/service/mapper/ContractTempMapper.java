package nth.module.utilities.service.mapper;


import nth.module.utilities.domain.ContractTemp;
import nth.module.utilities.service.dto.ContractTempDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ContractTempMapper extends EntityMapper<ContractTempDTO, ContractTemp>{

    @Override
    ContractTempDTO toDto(ContractTemp entity);

    @Override
    ContractTemp toEntity(ContractTempDTO dto);

    default ContractTemp fromId (Long id) {
        if (id == null) {
            return null;
        }
        ContractTemp contractTemp = new ContractTemp();
        contractTemp.setId(id);
        return contractTemp;
    }
}

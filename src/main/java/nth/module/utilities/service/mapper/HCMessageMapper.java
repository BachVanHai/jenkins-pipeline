package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.HCMessage;
import nth.module.utilities.service.dto.HCMessageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link HCMessage} and its DTO {@link HCMessageDTO}.
 */
@Mapper(componentModel = "spring", uses = {HCSupportBookMapper.class})
public interface HCMessageMapper extends EntityMapper<HCMessageDTO, HCMessage> {

    @Mapping(target = "hCSupportBookId", source = "HCSupportBook.id")
    @Mapping(target = "hCSupportBook", ignore = true )
    @Override
    HCMessageDTO toDto(HCMessage s);


    @Mapping(target = "hCSupportBook.id", source = "hCSupportBookId")
    @Override
    HCMessage toEntity(HCMessageDTO dto);

    default HCMessage fromId(Long id) {
        if (id == null) {
            return null;
        }
        HCMessage hcMessage = new HCMessage();
        hcMessage.setId(id);
        return hcMessage;
    }
}

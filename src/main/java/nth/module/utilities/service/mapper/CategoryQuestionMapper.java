package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.*;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategoryQuestion} and its DTO {@link CategoryQuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategoryQuestionMapper extends EntityMapper<CategoryQuestionDTO, CategoryQuestion> {

    @Override
    CategoryQuestionDTO toDto(CategoryQuestion entity);

    @Override
    CategoryQuestion toEntity(CategoryQuestionDTO dto);

    default CategoryQuestion fromId (Long id) {
        if (id == null) {
            return null;
        }
        CategoryQuestion categoryQuestion = new CategoryQuestion();
        categoryQuestion.setId(id);
        return categoryQuestion;
    }
}

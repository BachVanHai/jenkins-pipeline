package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.*;
import nth.module.utilities.service.dto.QuestionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Question} and its DTO {@link QuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = { CategoryQuestionMapper.class })
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {

    @Mapping(target = "categoryQuestionId", source = "categoryQuestion.id")
    @Mapping(target = "categoryQuestion", ignore = true)
    QuestionDTO toDto(Question s);

    @Mapping(target = "categoryQuestion.id", source = "categoryQuestionId")
    @Override
    Question toEntity(QuestionDTO dto);

    default Question fromId (Long id) {
        if (id == null) {
            return null;
        }
        Question question = new Question();
        question.setId(id);
        return question;
    }
}

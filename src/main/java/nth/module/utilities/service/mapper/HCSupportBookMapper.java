package nth.module.utilities.service.mapper;

import nth.module.utilities.domain.HCSupportBook;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import org.mapstruct.*;

import java.nio.charset.StandardCharsets;

/**
 * Mapper for the entity {@link HCSupportBook} and its DTO {@link HCSupportBookDTO}.
 */
@Mapper(componentModel = "spring", uses = { HCUserMapper.class, HCMessageMapper.class })
public interface HCSupportBookMapper extends EntityMapper<HCSupportBookDTO, HCSupportBook> {

    @Mapping(target = "hCUserId", source = "HCUser.id")
    @Mapping(target = "hcMessageDTOList", source = "HCMessages")
    HCSupportBookDTO toDto(HCSupportBook s);

    @Mapping(target = "hCUser", source = "hCUserId")
    @Override
    HCSupportBook toEntity(HCSupportBookDTO dto);

    default HCSupportBook fromId(Long id) {
        if (id == null) {
            return null;
        }
        HCSupportBook hcSupportBook = new HCSupportBook();
        hcSupportBook.setId(id);
        return hcSupportBook;
    }


    default byte[] map(String value) {
        return value == null ? null : value.getBytes();
    }

    default String map(byte[] value) {
        return value == null ? null : new String(value, StandardCharsets.UTF_8);
    }
}

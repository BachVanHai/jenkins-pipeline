package nth.module.utilities.service;

import nth.module.utilities.service.dto.ContractTempDTO;

import java.util.List;
import java.util.Optional;

public interface ContractTempService {
    ContractTempDTO save (ContractTempDTO contractTempDTO);
    Optional<ContractTempDTO> findOne(Long id);
    Optional<ContractTempDTO> findByCode(String code);
    List<ContractTempDTO> createContractTemp(List<ContractTempDTO> contractTempDTO, String userId, String contractType);
}

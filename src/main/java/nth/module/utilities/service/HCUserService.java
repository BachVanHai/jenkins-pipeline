package nth.module.utilities.service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.service.dto.HCUserDTO;
import nth.module.utilities.utils.RoleUtils;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.HCUser}.
 */
public interface HCUserService {
    /**
     * Save a hCUser.
     *
     * @param hCUserDTO the entity to save.
     * @return the persisted entity.
     */
    HCUserDTO save(HCUserDTO hCUserDTO);

    /**
     * Get all the hCUsers.
     *
     * @return the list of entities.
     */
    List<HCUserDTO> findAll();

    /**
     * Get the "id" hCUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HCUserDTO> findOne(Long id);

    /**
     * Delete the "id" hCUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //todo create user add support book.
    HCUserDTO createUserSupportBook(String userId,HCUserDTO hCUserDTO);

    //todo get all member in department.
    HashMap<String, List<UsersDTO>> findAllUserInDepartment();

    /*
    * todo update user add support book.
    *  */
    Optional<HCUserDTO> updateUserSupportBook (String userId, HCUserDTO hcUserDTO) throws InstantiationException, IllegalAccessException;

    HCUserDTO checkExistHCUser(String userId);

    Optional<HCUserDTO> findOneByUserInOnId(Long id);
}

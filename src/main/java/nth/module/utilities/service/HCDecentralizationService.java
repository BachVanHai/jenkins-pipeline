package nth.module.utilities.service;

import java.util.List;
import java.util.Optional;

import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.service.dto.HCDecentralizationDTO;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.HCDecentralization}.
 */
public interface HCDecentralizationService {
    /**
     * Save a hCDecentralization.
     *
     * @param hCDecentralizationDTO the entity to save.
     * @return the persisted entity.
     */
    HCDecentralizationDTO save(HCDecentralizationDTO hCDecentralizationDTO);

    /**
     * Get all the hCDecentralizations.
     *
     * @return the list of entities.
     */
    List<HCDecentralizationDTO> findAll();

    /**
     * Get the "id" hCDecentralization.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HCDecentralizationDTO> findOne(Long id);

    /**
     * Delete the "id" hCDecentralization.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //todo find all by user id.
    List<HCDecentralizationDTO> findAllByUserId(String userId);

    //todo find all by department.
    List<HCDecentralizationDTO> findAllByDepartment (HCDepartment hcDepartment);

    //todo create Decentralization
    HCDecentralizationDTO createDecentralization (String userId, HCDecentralizationDTO hcDecentralizationDTO) throws InstantiationException, IllegalAccessException;

    //todo update decentralization
    HCDecentralizationDTO updateDecentralization(String userId, HCDecentralizationDTO hcDecentralizationDTO) throws InstantiationException, IllegalAccessException;

    /*
    * todo find by user id and department.
    *  */
    Optional<HCDecentralizationDTO> findFirstByUserIdAndDepartment(String userId, HCDepartment hcDepartment);

}

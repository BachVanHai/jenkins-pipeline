package nth.module.utilities.service;

import nth.lib.integration.model.ContractDTO;

import java.time.Instant;
import java.util.List;

public interface ContractManagerService {

    List<ContractDTO> getAllEliteContracts(String appId, Instant  fromDate, Instant toDate);
}

package nth.module.utilities.service;

import java.security.Principal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.service.dto.HCSupportBookDTO;
import nth.module.utilities.service.dto.HCUserDTO;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.HCSupportBook}.
 */
public interface HCSupportBookService {
    /**
     * Save a hCSupportBook.
     *
     * @param hCSupportBookDTO the entity to save.
     * @return the persisted entity.
     */
    HCSupportBookDTO save(HCSupportBookDTO hCSupportBookDTO);

    /**
     * Get all the hCSupportBooks.
     *
     * @return the list of entities.
     */
    List<HCSupportBookDTO> findAll(Pageable pageable);

    /**
     * Get the "id" hCSupportBook.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HCSupportBookDTO> findOne(String userId, Long id);

    /**
     * Delete the "id" hCSupportBook.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //todo create support book.
    HCSupportBookDTO createSupportBook (String userId, HCSupportBookDTO hcSupportBookDTO);

    //todo update support book.
    HCSupportBookDTO updateSupportBook (String userId, HCSupportBookDTO hcSupportBookDTO) throws InstantiationException, IllegalAccessException, JsonProcessingException;

    //todo find all my HC support book
    List<HCSupportBookDTO> findAllMyHCSupportBook (String userId, Pageable pageable);

    //todo find all my HC support book must process.
    List<HCSupportBookDTO> findAllMySupportBookManager (String userId, Pageable pageable);

    /*
    * todo find all hc support book by user id.
    *  */
    List<HCSupportBookDTO> findAllBySupporterInOnId (String userId);

    /*
    * todo find all support book by supporter InOn id and HCStatus.
    *  */
    List<HCSupportBookDTO> findAllBySupporterInOnIdAndHCStatusIn(String supporterInOnIdn, List<HCStatus>  hcStatuses);

    //todo find one by roomId
    Optional<HCSupportBookDTO> findOneByRoomId(String roomId, String userId);

    /*
    * todo find all message month.
    *  */
    List<HCSupportBookDTO> findAllBySupportInOnIdMonth(String supporterInOnId,Instant fromDate, Instant toDate);

    List<HCSupportBookDTO> findAllByhCUserAndDeletedIsFalse(HCUserDTO hcUserDTO);

}

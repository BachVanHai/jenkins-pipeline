package nth.module.utilities.service;

import java.util.List;
import java.util.Optional;
import nth.module.utilities.service.dto.HCFileUploadDTO;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.HCFileUpload}.
 */
public interface HCFileUploadService {
    /**
     * Save a hCFileUpload.
     *
     * @param hCFileUploadDTO the entity to save.
     * @return the persisted entity.
     */
    HCFileUploadDTO save(HCFileUploadDTO hCFileUploadDTO);

    /**
     * Get all the hCFileUploads.
     *
     * @return the list of entities.
     */
    List<HCFileUploadDTO> findAll();

    /**
     * Get the "id" hCFileUpload.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HCFileUploadDTO> findOne(Long id);

    /**
     * Delete the "id" hCFileUpload.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //todo create file upload.
    List<HCFileUploadDTO> createFileUpload(String userId, List<HCFileUploadDTO> fileUploadDTOList);

    /*
    * todo find all by hc message id.
    *  */
    List<HCFileUploadDTO> findAllByHCMessageId(String hcMessageId);
}

package nth.module.utilities.service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.service.dto.QuestionDTO;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link nth.module.utilities.domain.Question}.
 */
public interface QuestionService {
    /**
     * Save a question.
     *
     * @param questionDTO the entity to save.
     * @return the persisted entity.
     */
    QuestionDTO save(QuestionDTO questionDTO);

    /**
     * Get all the questions.
     *
     * @return the list of entities.
     */
    List<QuestionDTO> findAll();

    /**
     * Get the "id" question.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuestionDTO> findOne(Long id);

    /**
     * Delete the "id" question.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /*
    * create question
    * */
    Optional<QuestionDTO> createQuestion(QuestionDTO questionDTO, CategoryQuestionType categoryQuestionType, String userId, UserClient userClient) throws InstantiationException, IllegalAccessException;

    /*
    * update question.
    * */
    Optional<QuestionDTO> updateQuestion(QuestionDTO questionDTO, String userId, UserClient userClient, CategoryQuestionType categoryQuestionType) throws InstantiationException, IllegalAccessException;

    /*
    * get all Question.
    * */
    List<QuestionDTO> findAllByCategoryQuestionAndApplyFor(List<CategoryQuestionDTO> categoryQuestionDTOList, ApplyFor applyFor, String userId,CategoryQuestionType categoryQuestionType, Pageable pageable) throws JsonProcessingException;

    /*
    * Search question.
    * */
    HashMap<String, Object> searchDocuments(String searchKey, Pageable pageable, Optional<String> userId);

    /*
     * get all question manager.
     * */
    HashMap<String, Object> getAllQuestionManager(String userId, boolean isAdmin, Pageable pageable);

}

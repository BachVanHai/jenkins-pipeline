package nth.module.utilities.service;

import nth.module.utilities.service.dto.AppConfigSystemDTO;

import java.util.List;

public interface AppConfigSystemService {

    List<AppConfigSystemDTO> findAll();

    List<AppConfigSystemDTO> findAllByKeyAndEnable(String key, Boolean enable);
}

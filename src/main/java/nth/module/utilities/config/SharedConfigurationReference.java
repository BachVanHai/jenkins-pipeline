package nth.module.utilities.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("nth.lib.common")
public class SharedConfigurationReference {}


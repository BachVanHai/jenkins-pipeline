package nth.module.utilities.websocket;

import nth.lib.common.utils.XSSFilter;
import nth.module.utilities.service.HCMessageService;
import nth.module.utilities.service.dto.HCMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.time.Instant;

@RestController
public class MessageResource {

    private final Logger log = LoggerFactory.getLogger(MessageResource.class);

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final HCMessageService hcMessageService;

    public MessageResource(SimpMessagingTemplate simpMessagingTemplate, HCMessageService hcMessageService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.hcMessageService = hcMessageService;
    }

    @MessageMapping("/chat/{to}")
    public void sendMessage(Principal principal, @DestinationVariable String to, HCMessageDTO message) {
        try {
            message.setContentText(XSSFilter.stripXSS(message.getContentText()));
            hcMessageService.createMessageAsync(message, principal.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        message.setSendDate(Instant.now());
        simpMessagingTemplate.convertAndSend("/topic/messages/" + to, message);
    }
}

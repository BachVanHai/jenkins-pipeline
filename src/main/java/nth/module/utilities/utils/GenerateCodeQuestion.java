package nth.module.utilities.utils;

import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.service.dto.QuestionDTO;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class GenerateCodeQuestion {

    public static String generateCodeQuestion(QuestionDTO questionDTO, CategoryQuestionType categoryQuestionType) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
        String code = "";
        switch (categoryQuestionType) {
            case CHTG:
                code = "QA_" + questionDTO.getCategoryQuestionId() + "_" + simpleDateFormat.format(Date.from(Instant.now())) + "_" + String.format("%02d", questionDTO.getId());
                ;
                return code;
            case HDSD:
                code = "UM_" + questionDTO.getCategoryQuestionId() + "_" + simpleDateFormat.format(Date.from(Instant.now())) + "_" + String.format("%02d", questionDTO.getId());
                return code;
            case TLNV:
                code = "DOC_" + questionDTO.getCategoryQuestionId() + "_" + simpleDateFormat.format(Date.from(Instant.now())) + "_" + String.format("%02d", questionDTO.getId());
                return code;
        }
        return null;
    }
}

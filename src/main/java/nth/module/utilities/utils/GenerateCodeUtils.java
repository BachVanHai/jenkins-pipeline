package nth.module.utilities.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class GenerateCodeUtils {

    private static final Logger log = LoggerFactory.getLogger(GenerateCodeUtils.class);

    @Value("${app-configs.template-dir}")
    private static String templateDir;

    final static String QRCodePath = templateDir + "qrCodePti.png";

    static String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm0123456789";

    public static String generateCode(int n) {
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());

            sb.append(AlphaNumericString.charAt(index));
        }
        String code = sb.toString();
        int lenth = code.length();
        StringBuilder stringBuilder = new StringBuilder(lenth);
        for (int i = 0; i < lenth; i++) {
            int index = (int) (code.length() * Math.random());

            stringBuilder.append(code.charAt(index));
        }
        return stringBuilder.toString();
    }

    public static void decodeBase64ToImage(String data) throws IOException {
        byte[] imageBytes = DatatypeConverter.parseBase64Binary(data);
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));
        File outPutFile = new File(QRCodePath);
        ImageIO.write(image, "png", outPutFile);
    }
}

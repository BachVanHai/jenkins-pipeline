package nth.module.utilities.utils;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import nth.lib.integration.model.UserAuthDTO;
import nth.module.utilities.client.GatewayClient;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCSupportType;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DecentralizationUtils {


    public boolean isHCADOP(String userId, HCSupportType hcSupportType, HCDecentralizationService hcDecentralizationService) {
        List<HCDecentralizationDTO> hcDecentralizationDTOList = hcDecentralizationService.findAllByUserId(userId);
        hcDecentralizationDTOList = hcDecentralizationDTOList
            .stream()
            .filter(item -> item.getDecentralization().equals(HCDecentralizationType.AD_OP) && item.getHcSupportType().equals(hcSupportType))
            .collect(Collectors.toCollection(LinkedList::new));
        return hcDecentralizationDTOList.size() > 0;
    }

    public boolean isHCMAJOR(String userId, HCSupportType hcSupportType, HCDecentralizationService hcDecentralizationService) {
        List<HCDecentralizationDTO> hcDecentralizationDTOList = hcDecentralizationService.findAllByUserId(userId);
        hcDecentralizationDTOList = hcDecentralizationDTOList
            .stream()
            .filter(item -> item.getDecentralization().equals(HCDecentralizationType.MAJOR) && item.getHcSupportType().equals(hcSupportType))
            .collect(Collectors.toCollection(LinkedList::new));
        return hcDecentralizationDTOList.size() > 0;
    }

    public static String generateFlowerInOn(HCSupportType hcSupportType, HCDecentralizationService hcDecentralizationService, GatewayClient gatewayClient) {

        HCDepartment hcDepartment = autoSelectDepartment(hcSupportType);
        List<HCDecentralizationDTO> hcDecentralizationDTOList = hcDecentralizationService.findAllByDepartment(hcDepartment);
        List<String> userIds = hcDecentralizationDTOList
            .stream()
            .map(HCDecentralizationDTO::getUserId)
            .collect(Collectors.toCollection(LinkedList::new));

        List<String> userAuthIdList = gatewayClient.getUserAuths(userIds);

        return String.join(",", userAuthIdList);
    }

    public static HashMap<String, Object> autoMappingSupporterInOn(HCSupportType hcSupportType, HCDecentralizationService hcDecentralizationService) {

        HCDepartment hcDepartment = autoSelectDepartment(hcSupportType);
        List<HCDecentralizationDTO> hcDecentralizationDTOList = hcDecentralizationService.findAllByDepartment(hcDepartment);
        List<String> userIds = hcDecentralizationDTOList
            .stream()
            .map(HCDecentralizationDTO::getUserId)
            .collect(Collectors.toCollection(LinkedList::new));

        HashMap<String, Object> result = new HashMap<>();
        result.put("userIds", userIds);
        result.put("hcDepartment", hcDepartment);
        return result;
    }

    public static HCDepartment autoSelectDepartment(HCSupportType hcSupportType) {

        switch (hcSupportType) {
            case PRODUCT:
            case FEEDBACK:
            case ASSURANCE_COMPENSATION:
            case CUSTOMER_SERVICE:
            case OTHER:
                return HCDepartment.OPERATE;
            case PAYMENT:
                return HCDepartment.ACCOUNTANT;
            case TECHNOLOGY:
                return HCDepartment.TECHNOLOGY;
        }
        return null;
    }

    public static List<HCSupportType> autoSelectHCSupportType(HCDepartment hcDepartment){
        List<HCSupportType> hcSupportTypeList = new ArrayList<>();
        switch (hcDepartment) {
            case OPERATE:
                hcSupportTypeList.add(HCSupportType.PRODUCT);
                hcSupportTypeList.add(HCSupportType.ASSURANCE_COMPENSATION);
                hcSupportTypeList.add(HCSupportType.CUSTOMER_SERVICE);
                hcSupportTypeList.add(HCSupportType.FEEDBACK);
                hcSupportTypeList.add(HCSupportType.OTHER);
                break;
            case ACCOUNTANT:
                hcSupportTypeList.add(HCSupportType.PAYMENT);
                break;
            default:
                hcSupportTypeList.add(HCSupportType.TECHNOLOGY);

        }

        return hcSupportTypeList;
    }
}

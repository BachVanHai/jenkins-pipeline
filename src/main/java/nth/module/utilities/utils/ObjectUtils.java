package nth.module.utilities.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.enumeration.InsuranceCode;
import nth.lib.integration.enumeration.PaymentType;
import nth.lib.integration.enumeration.PlaceType;
import nth.lib.integration.model.*;
import nth.module.utilities.model.inon.InsuranceModelES;
import nth.module.utilities.model.inon.VehicleModelES;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ObjectUtils {

    private static Logger log = LoggerFactory.getLogger(ObjectUtils.class);

    private final RestHighLevelClient restClient;

    final static ObjectMapper objectMapper = new ObjectMapper();

    public ObjectUtils(RestHighLevelClient restClient) {
        this.restClient = restClient;
    }

    @SuppressWarnings("unchecked")
    public static <T> void mergeObjects(T dest, T source) throws IllegalAccessException, InstantiationException {
        Class<?> clazz = dest.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value1 = field.get(dest);
            Object value2 = field.get(source);
            Object value = null;
            if (field.getDeclaredAnnotation(IgnoreUpdate.class) != null && value1 != null) {
                value = value1;
            } else if(field.getDeclaredAnnotation(AlwaysUpdate.class) != null){
                value = value1;
            } else {
                value = (value1 != null && value2 == null) ? value1 : value2;
            }
            field.set(dest, value);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T cloneObject(T source) throws IllegalAccessException, InstantiationException {
        Class<?> clazz = source.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Object returnValue = clazz.newInstance();
        for (Field field : fields) {
            Class<?> fieldTypeClazz = field.getType();
            if (field.getDeclaredAnnotation(IgnoreCopy.class) == null) {
                field.setAccessible(true);
                field.set(returnValue, field.get(source));
            }
        }
        return (T) returnValue;
    }

    public static ContractDTO mapESToObject(Map<String, Object> map) throws JsonProcessingException, ParseException {
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setId(map.get("id") + "");
        contractDTO.setPaymentType(map.get("payment_type") == null ? PaymentType.FUND_TRANSFER : PaymentType.valueOf(map.get("payment_type") + ""));
        contractDTO.setTotalFeeInclVAT(map.get("total_fee_incl_vat") == null ? BigDecimal.ZERO : BigDecimal.valueOf(Long.parseLong(String.format("%.0f", Float.valueOf(map.get("total_fee_incl_vat") + "")))));
        contractDTO.setCreatedDate(map.get("created_date") == null ? Instant.now() : Instant.parse(map.get("created_date") + ""));
        contractDTO.setUpdateDate(map.get("update_date") == null ? Instant.now() : Instant.parse(map.get("update_date") + ""));
        contractDTO.setLatestApprovalStatus(map.get("latest_approval_status") + "");
        contractDTO.setContractCode(map.get("contract_code") + "");
        contractDTO.setContractType(ContractType.valueOf(map.get("contract_type") + ""));
        contractDTO.setPaid(map.get("paid") + "");
        contractDTO.setCertUrl(map.get("url_in_on") + "");
        contractDTO.setSalerName(map.get("saler_name") + "");
        contractDTO.setSalerCode(map.get("sale_code") + "");
        contractDTO.setCompanyId(map.get("company_id") + "");
        contractDTO.setInsurCompanyName(map.get("company_name") + "");
//            Instant.parse(map.get("start_value_date") + "")

        CustomerInfoDTO owner = new CustomerInfoDTO();
        owner.setFullName(map.get("full_name") + "");
        owner.setPhoneNumber(map.get("phone_number") + "");
        owner.setEmail(map.get("email") + "");
        owner.setAddress(map.get("address") + "");
        contractDTO.setOwner(owner);

        InsurCompCertDTO insurCompCertDTO = new InsurCompCertDTO();
        insurCompCertDTO.setPrintedCertNo(map.get("printed_cert_no") + "");
        insurCompCertDTO.setInsurCompContractNo(map.get("insur_comp_contract_no") + "");
        insurCompCertDTO.setUrlInOn(map.get("url_in_on") + "");
        insurCompCertDTO.setUrlInsurComp(map.get("url_insur_comp") + "");
        contractDTO.setInsurCompCertDTO(insurCompCertDTO);

        if (map.get("addition_info") != null && !String.valueOf(map.get("addition_info")).isEmpty()) {
            VehicleModelES vehicleModelES = objectMapper.readValue(map.get("addition_info") + "", VehicleModelES.class);
            VehicleDTO vehicleDTO = new VehicleDTO();
            vehicleDTO.setFrameNo(vehicleModelES.getFrame_no() == null || vehicleModelES.getFrame_no().isEmpty() ? "" : vehicleModelES.getMachine_no());
            vehicleDTO.setIssDate(vehicleModelES.getIss_date() == null || vehicleModelES.getIss_date().isEmpty() ? LocalDate.now() : LocalDate.parse(vehicleModelES.getIss_date(), dateTimeFormatter));
            vehicleDTO.setIssPlace(vehicleModelES.getIss_place() == null || vehicleModelES.getIss_place().isEmpty() ? PlaceType.VIETNAM : PlaceType.valueOf(vehicleModelES.getIss_place()));
            vehicleDTO.setInitValue(vehicleModelES.getInit_value() == null || vehicleModelES.getInit_value().isEmpty() ? BigDecimal.ZERO : new BigDecimal(vehicleModelES.getInit_value()));
            vehicleDTO.setMachineNo(vehicleModelES.getMachine_no() == null || vehicleModelES.getMachine_no().isEmpty() ? "" : vehicleModelES.getMachine_no());
            vehicleDTO.setNumberPlate(vehicleModelES.getNumber_plate() == null || vehicleModelES.getNumber_plate().isEmpty() ? "" : vehicleModelES.getNumber_plate());
            vehicleDTO.setContractValue(vehicleModelES.getContract_value() == null || vehicleModelES.getContract_value().isEmpty() ? BigDecimal.ZERO : new BigDecimal(vehicleModelES.getContract_value()));

            VehicleTypeDTO vehicleTypeDTO = new VehicleTypeDTO();
            vehicleTypeDTO.setName(vehicleModelES.getVehicle_type_name() == null || vehicleModelES.getVehicle_type_name().isEmpty() ? "" : vehicleModelES.getVehicle_type_name());
            vehicleDTO.setVehicleTypeDTO(vehicleTypeDTO);
            contractDTO.setVehicles(Collections.singleton(vehicleDTO));
        }

        List<String> insurancesString = Arrays.asList((map.get("insurances") + "").split("\\|"));
        List<InsuranceModelES> insuranceModelES = new ArrayList<>();
        for (String s : insurancesString) {
            if (s != null && !s.isEmpty()) {
                try {
                    InsuranceModelES insuranceModelES1 = objectMapper.readValue(s, InsuranceModelES.class);
                    if (insuranceModelES1 != null) {
                        insuranceModelES.add(insuranceModelES1);
                    }
                } catch (Exception exception) {
                    log.error(objectMapper.writeValueAsString(s));
                }

            }

        }
        Set<InsuranceDTO> insuranceDTOS = new HashSet<>();
        InsuranceDTO insuranceDTO = new InsuranceDTO();
        BigDecimal tnpxnnOto = BigDecimal.ZERO;
        BigDecimal bhvcOto = BigDecimal.ZERO;
        BigDecimal tndstnOto = BigDecimal.ZERO;
        BigDecimal tndsbbOto = BigDecimal.ZERO;
        BigDecimal tndshhOto = BigDecimal.ZERO;
        BigDecimal tndsbbXeMay = BigDecimal.ZERO;
        BigDecimal nnXeMay = BigDecimal.ZERO;

        for (InsuranceModelES insuranceModelE : insuranceModelES) {
            insuranceDTO.setCount1(new BigDecimal(insuranceModelE.getCount_1() == null ? "0" : insuranceModelE.getCount_1()));
            insuranceDTO.setCount2(new BigDecimal(insuranceModelE.getCount_2() == null ? "0" : insuranceModelE.getCount_2()));
            insuranceDTO.setCount3(new BigDecimal(insuranceModelE.getCount_3() == null ? "0" : insuranceModelE.getCount_3()));
            insuranceDTO.setValue1(new BigDecimal(insuranceModelE.getValue_1() == null ? "0" : insuranceModelE.getValue_1()));
            insuranceDTO.setValue2(new BigDecimal(insuranceModelE.getValue_2() == null ? "0" : insuranceModelE.getValue_2()));
            insuranceDTO.setValue3(new BigDecimal(insuranceModelE.getValue_3() == null ? "0" : insuranceModelE.getValue_3()));
            insuranceDTO.setDuration(Integer.valueOf(insuranceModelE.getDuration() == null ? "0" : insuranceModelE.getDuration()));
            try {
                insuranceDTO.setIsEnable(insuranceModelE.getIs_enable() == 1);
            } catch (Exception e) {
                insuranceDTO.setIsEnable(false);
            }
            insuranceDTO.setStartValueDate((insuranceModelE.getStart_value_date() == null || insuranceModelE.getStart_value_date().isEmpty()) ? null : date.parse(insuranceModelE.getStart_value_date()).toInstant());
            insuranceDTO.setEndValueDate((insuranceModelE.getEnd_value_date() == null || insuranceModelE.getEnd_value_date().isEmpty()) ? null : date.parse(insuranceModelE.getEnd_value_date()).toInstant());
            insuranceDTO.setInsuranceCode(InsuranceCode.valueOf(insuranceModelE.getInsurance_code()));
            if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_CONNGUOI)) {
                tnpxnnOto = tnpxnnOto.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            } else if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_HANGHOA)) {
                tndshhOto = tndshhOto.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            } else if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_TNDS)) {
                tndsbbOto = tndsbbOto.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            } else if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_VATCHAT)) {
                bhvcOto = bhvcOto.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            } else if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.CAR_TNDS_TN)) {
                tndstnOto = tndstnOto.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1()).add(insuranceDTO.getValue2() == null ? BigDecimal.ZERO : insuranceDTO.getValue2()).add(insuranceDTO.getValue3() == null ? BigDecimal.ZERO : insuranceDTO.getValue3());
            } else if (insuranceDTO.getInsuranceCode().equals(InsuranceCode.MOTOR_CONNGUOI)) {
                nnXeMay = nnXeMay.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            } else {
                tndsbbXeMay = tndsbbXeMay.add(insuranceDTO.getValue1() == null ? BigDecimal.ZERO : insuranceDTO.getValue1());
            }
            insuranceDTOS.add(insuranceDTO);

        }
        contractDTO.setTnpxnnOto(String.format("%.0f", tnpxnnOto.floatValue()));
        contractDTO.setBhvcOto(String.format("%.0f", bhvcOto.floatValue()));
        contractDTO.setTndstnOto(String.format("%.0f", tndstnOto.floatValue()));
        contractDTO.setTndsbbOto(String.format("%.0f", tndsbbOto.floatValue()));
        contractDTO.setTndshhOto(String.format("%.0f", tndshhOto.floatValue()));
        contractDTO.setNnXeMay(String.format("%.0f", nnXeMay.floatValue()));
        contractDTO.setTndsbbXeMay(String.format("%.0f", tndsbbXeMay.floatValue()));
        contractDTO.setTotalFeeBonus(String.format("%.0f", tnpxnnOto.add(bhvcOto).add(tndstnOto).add(tndsbbOto).add(tndshhOto).add(nnXeMay).add(tndsbbXeMay).floatValue()));
        contractDTO.setInsurances(insuranceDTOS);

        return contractDTO;
    }
}

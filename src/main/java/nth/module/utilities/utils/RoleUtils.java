package nth.module.utilities.utils;

import nth.lib.common.type.UserRole;
import nth.lib.integration.model.UsersDTO;
import nth.module.utilities.client.UserClient;
import nth.module.utilities.service.HCDecentralizationService;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import org.apache.commons.lang.ArrayUtils;

import java.util.List;

public class RoleUtils {

    public static final String[] ADMIN_ROLES = new String[]{UserRole.ADMIN.getValue()};
    public static final String[] DT_INON_DV = new String[]{
        UserRole.DIVAYL1.getValue(),
        UserRole.DIVAYL2.getValue(),
        UserRole.DIVAYL3.getValue(),
        UserRole.DIVAYL4.getValue(),
        UserRole.DIVAYL5.getValue()};
    public static final String[] DT_INON = (String[]) ArrayUtils.addAll(DT_INON_DV, UserRole.getPartnerRoles());
    public static final String[] KT_INON = new String[]{UserRole.KT.getValue()};
    public static final String[] INSUR_COMP = new String[]{UserRole.BH.getValue()};
    public static final String[] SUPPORT = new String[]{UserRole.HTKD.getValue()};
    public static final String[] OPERATE = new String[]{UserRole.VH.getValue()};
    public static final String[] KD = new String[]{UserRole.KD.getValue()};
    public static final String[] SPKD_InOn = new String[]{UserRole.HTKD.getValue()};

    public static String getRoleGroup(UserClient userClient, String userName) {
        UsersDTO userInfo = userClient.getUserInfoById(userName);
        String userGroupId = userInfo.getGroupId();
        return (userGroupId);
    }

    public static boolean isAdmin(String roleGroup) {
        for (String role : ADMIN_ROLES) {
            if (role.equals(roleGroup)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAccountant(String roleGroup) {
        for (String role : KT_INON) {
            if (role.equals(roleGroup)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isPartner(String roleGroup) {
        for (String role : DT_INON) {
            if (role.equals(roleGroup)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInsuranceComp(String roleGroup) {
        for (String role : INSUR_COMP) {
            if (role.equals(roleGroup)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSupport(String roleGroup) {
        for (String role : SUPPORT) {
            if (role.equals(roleGroup)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isOperate(String rolegroup) {
        for (String role : OPERATE) {
            if (role.equals(rolegroup)){
                return true;
            }
        }
        return false;
    }

    public static boolean isSPKDInOn(String rolegroup) {
        for (String role : SPKD_InOn) {
            if (role.equals(rolegroup)){
                return true;
            }
        }
        return false;
    }

}

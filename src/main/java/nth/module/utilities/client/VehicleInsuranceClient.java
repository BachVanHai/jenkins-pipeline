package nth.module.utilities.client;

import nth.lib.integration.enumeration.ApprovalStatus;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.model.ContractDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.Instant;
import java.util.List;

@FeignClient(name = "vehicleinsurance")
public interface VehicleInsuranceClient {

    @GetMapping(value = "/api/authenticate/contracts/{contractCode}")
    ContractDTO getContractByContractCode(@PathVariable(name = "contractCode") String contractCode);

    /*
     * todo need fake token.
     *  */
    @PostMapping(value = "/api/contract-manager/approval/{contractType}")
    void approvalContract(@RequestHeader(value = "Authorization", required = false) String token, @PathVariable(name = "contractType") ContractType contractType,
                          @RequestParam(name = "approvalStatus") ApprovalStatus status,
                          @RequestBody List<String> contractIds);

    @GetMapping(value = "/api/contracts/expired-contract/{contractType}")
    List<ContractDTO> getExpiredContract(@PathVariable(name = "contractType") String contractType);

    @GetMapping(value = "/api/contract-manager/all-elite-contracts")
    List<ContractDTO> getAllEliteContract(@RequestHeader(name = "appId") String appId,
                                          @RequestParam(name = "fromDate") Instant fromDate,
                                          @RequestParam(name = "toDate") Instant toDate);

}

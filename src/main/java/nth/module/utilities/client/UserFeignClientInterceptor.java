package nth.module.utilities.client;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import nth.module.utilities.security.SecurityUtils;
import nth.module.utilities.security.jwt.TokenProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class UserFeignClientInterceptor implements RequestInterceptor {
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String APPID_HEADER = "appId";
    private static final String BEARER = "Bearer";

    private final HttpServletRequest request;

    private final TokenProvider tokenProvider;

    public UserFeignClientInterceptor(HttpServletRequest request, TokenProvider tokenProvider) {
        this.request = request;
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void apply(RequestTemplate template) {

        SecurityUtils.getCurrentUserJWT()
            .ifPresent(s -> template.header(AUTHORIZATION_HEADER, String.format("%s %s", BEARER, s)));

    }
}

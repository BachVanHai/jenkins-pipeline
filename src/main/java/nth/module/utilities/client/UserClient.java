package nth.module.utilities.client;

import nth.lib.integration.model.UserInsuranceSetting;
import nth.lib.integration.model.UsersDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@FeignClient("user")
public interface UserClient {
    @GetMapping("/api/users/{username}")
    UsersDTO getUserInfo(@PathVariable(name = "username") String username);

    @GetMapping("/api/users-by-id/{userId}")
    UsersDTO getUserInfoById(@PathVariable(name = "userId") String userId);

    @GetMapping("/api/users-by-id/{userId}")
    UsersDTO getUserInfoById(@PathVariable(name = "userId") String userId, @RequestHeader(name = "Authorization") String token);

    @GetMapping("/api/users-by-id/{userId}")
    UsersDTO getUserInfoByIdAuthor(@PathVariable(name = "userId") String userId, @RequestHeader(value = "Authorization", required = false) String token);

    @GetMapping("/api/child-users/{userId}")
    List<UsersDTO> getChildUser(@PathVariable(name = "userId") String userId);

    @GetMapping("/api/group-users/{group}")
    List<UsersDTO> getListUserOfGroupRole(@PathVariable(name = "group") String group);

    @GetMapping("/api/user-insurance-settings/{userId}")
    List<UserInsuranceSetting> getInsuranceSetting(@PathVariable(name = "userId") String group);

    @GetMapping(value = "/api/authenticate/users/phone-email")
    Optional<UsersDTO> getUsersByPhoneNumberAndEmail(@RequestParam(name = "phoneNumber") String fullName,
                                                     @RequestParam(name = "email") String email);
}

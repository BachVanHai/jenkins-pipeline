package nth.module.utilities.client;

import nth.lib.common.status.ApprovalStatus;
import nth.lib.integration.enumeration.ContractType;
import nth.lib.integration.model.ContractDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.Instant;
import java.util.List;

@FeignClient(name = "homeinsurance")
public interface HomeInsuranceClient {

    @GetMapping(value = "/api/authenticate/contracts/{contractCode}")
    ContractDTO getContractsByContractCode(@PathVariable(name = "contractCode") String contractCode);

    /*
    * todo need fake token.
    *  */
    @PostMapping(value = "/api/contract-manager/approval/{contractType}")
    void approvalContract(@RequestHeader(value = "Authorization", required = false) String token, @PathVariable(name = "contractType") ContractType contractType,
                          @RequestParam(name = "approvalStatus") ApprovalStatus status,
                          @RequestBody List<String> contractIds);

    @GetMapping(value = "/api/contract-manager/all-elite-contracts")
    List<ContractDTO> getAllEliteContract(@RequestHeader(name = "appId") String appId,
                                          @RequestParam(name = "fromDate") Instant fromDate,
                                          @RequestParam(name = "toDate") Instant toDate);
}

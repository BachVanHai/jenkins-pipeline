package nth.module.utilities.client;

import nth.lib.integration.model.CustomerInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("contactmanager")
public interface ContactManagerClient {
    @GetMapping("/api/customers/{id}")
    CustomerInfoDTO getContact(@PathVariable(name = "id") Long id);

    @GetMapping("/api/customers/list/ids")
    List<CustomerInfoDTO> getListContactByIds(@RequestParam(name = "ids") List<Long> id);
}

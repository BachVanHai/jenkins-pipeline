package nth.module.utilities.client;

import nth.lib.integration.model.FileInfoDTO;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "file",configuration = FeignSupportConfig.class)
public interface FileClient {
    @PostMapping("/api/download")
    FileInfoDTO downloadFile(@RequestHeader(name = "appId") String appId, @RequestBody FileInfoDTO info, @RequestParam(name = "link") String link);

    @PostMapping(value = "/api/upload-docs" , consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
    FileInfoDTO uploadDocs(@RequestParam(name = "categoryQuestionType") CategoryQuestionType categoryQuestionType,
                           @RequestParam(name = "docType") String docType,
                           @RequestPart("file") MultipartFile file,
                           @RequestPart("body") String body);

}

package nth.module.utilities.client;

import nth.lib.integration.model.UserAuthDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("gateway")
public interface GatewayClient {

    @GetMapping(value = "/api/authenticate/user-auths-ids")
    List<String> getUserAuths (@RequestParam(name = "ids") List<String> ids);

}

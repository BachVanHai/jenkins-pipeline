package nth.module.utilities.client;

import nth.lib.integration.model.SendEmailDTO;
import nth.lib.integration.model.utilities.NotificationsDTO;
import nth.lib.integration.model.zns.ZNSVehicleContractExpiredRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "notification",configuration = FeignSupportConfig.class)
public interface NotificationsClient {

    @PostMapping(value = "/api/authenticate/notifications/support-book")
    void sendNotificationWhenHaveSupportBookNeedProcess(@RequestBody NotificationsDTO notificationsDTO);

    @PostMapping("/api/notifications/send-mail")
    void sendMail(@RequestBody SendEmailDTO emailDTO);

    @PostMapping(value = "/api/notification/expired-contract")
    void sendExpiredContractNotification(@RequestBody SendEmailDTO emailDTO);

    @PostMapping(value = "/api/notification/zns-vehicle-expired")
    void sentZnsVehicleExpired(@RequestBody ZNSVehicleContractExpiredRequest znsVehicleContractExpiredRequest);
}

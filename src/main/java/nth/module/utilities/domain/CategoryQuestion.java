package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CategoryQuestion.
 */
@Entity
@Table(name = "category_question")
public class CategoryQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "category_question_type")
    private CategoryQuestionType categoryQuestionType;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "update_by")
    private String updateBy;

    @OneToMany(mappedBy = "categoryQuestion")
    @JsonIgnoreProperties(value = { "categoryQuestion" }, allowSetters = true)
    private Set<Question> questions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CategoryQuestion id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public CategoryQuestion name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryQuestionType getCategoryQuestionType() {
        return this.categoryQuestionType;
    }

    public CategoryQuestion categoryQuestionType(CategoryQuestionType categoryQuestionType) {
        this.setCategoryQuestionType(categoryQuestionType);
        return this;
    }

    public void setCategoryQuestionType(CategoryQuestionType categoryQuestionType) {
        this.categoryQuestionType = categoryQuestionType;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public CategoryQuestion createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public CategoryQuestion createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return this.updateDate;
    }

    public CategoryQuestion updateDate(Instant updateDate) {
        this.setUpdateDate(updateDate);
        return this;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public CategoryQuestion updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Set<Question> getQuestions() {
        return this.questions;
    }

    public void setQuestions(Set<Question> questions) {
        if (this.questions != null) {
            this.questions.forEach(i -> i.setCategoryQuestion(null));
        }
        if (questions != null) {
            questions.forEach(i -> i.setCategoryQuestion(this));
        }
        this.questions = questions;
    }

    public CategoryQuestion questions(Set<Question> questions) {
        this.setQuestions(questions);
        return this;
    }

    public CategoryQuestion addQuestion(Question question) {
        this.questions.add(question);
        question.setCategoryQuestion(this);
        return this;
    }

    public CategoryQuestion removeQuestion(Question question) {
        this.questions.remove(question);
        question.setCategoryQuestion(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryQuestion)) {
            return false;
        }
        return id != null && id.equals(((CategoryQuestion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategoryQuestion{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", categoryQuestionType='" + getCategoryQuestionType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

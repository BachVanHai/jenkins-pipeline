package nth.module.utilities.domain.enumeration;

/**
 * The HCDecentralizationType enumeration.
 */
public enum HCDecentralizationType {
    AD_OP,
    MAJOR,
}

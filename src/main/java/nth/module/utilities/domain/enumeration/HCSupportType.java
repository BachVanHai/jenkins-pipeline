package nth.module.utilities.domain.enumeration;

public enum HCSupportType {
    PRODUCT,
    PAYMENT,
    TECHNOLOGY,
    ASSURANCE_COMPENSATION,
    CUSTOMER_SERVICE,
    FEEDBACK,
    OTHER
}

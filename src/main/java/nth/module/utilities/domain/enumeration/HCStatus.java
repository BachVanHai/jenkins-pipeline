package nth.module.utilities.domain.enumeration;

/**
 * The HCStatus enumeration.
 */
public enum HCStatus {
    TODO,
    DOING,
    DONE,
}

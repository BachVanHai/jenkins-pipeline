package nth.module.utilities.domain.enumeration;

public enum TypeMessage {
    TEXT,
    ATTACHMENT,
    EMOJI
}

package nth.module.utilities.domain.enumeration;

public enum HCDepartment {
    OPERATE,
    ACCOUNTANT,
    TECHNOLOGY
}

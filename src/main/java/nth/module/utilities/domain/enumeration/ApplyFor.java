package nth.module.utilities.domain.enumeration;

/**
 * The ApplyFor enumeration.
 */
public enum ApplyFor {
    PARTNER,
    INDIVIDUAL,
    ALL,
}

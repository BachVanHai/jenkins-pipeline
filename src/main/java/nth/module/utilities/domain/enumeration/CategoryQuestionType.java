package nth.module.utilities.domain.enumeration;

/**
 * The CategoryQuestionType enumeration.
 */
public enum CategoryQuestionType {
    CHTG,
    HDSD,
    TLNV,
}

package nth.module.utilities.domain.enumeration;

/**
 * The StatusQuestion enumeration.
 */
public enum StatusQuestion {
    DRAFT,
    APPROVALED,
    REJECTED,
    WAITTING_APPROVAL,
    DISABLED
}

package nth.module.utilities.domain.enumeration;

/**
 * The HCPriorityLevel enumeration.
 */
public enum HCPriorityLevel {
    MEDIUM,
    HIGH,
    DROP
}

package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HCUser.
 */
@Entity
@Table(name = "hc_user")
public class HCUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id_in_on")
    private String userIdInOn;

    @Column(name = "name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "avatar")
    private String avatar;

    @OneToMany(mappedBy = "hCUser")
    @JsonIgnoreProperties(value = { "hCMessages", "hCUser" }, allowSetters = true)
    private Set<HCSupportBook> hCSupportBooks = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getId() {
        return this.id;
    }

    public HCUser id(Long id) {
        this.setId(id);
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserIdInOn() {
        return this.userIdInOn;
    }

    public HCUser userIdInOn(String userIdInOn) {
        this.setUserIdInOn(userIdInOn);
        return this;
    }

    public void setUserIdInOn(String userIdInOn) {
        this.userIdInOn = userIdInOn;
    }

    public String getName() {
        return this.name;
    }

    public HCUser name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public HCUser phoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public HCUser email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public HCUser createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public HCUser createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public HCUser updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public HCUser updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Set<HCSupportBook> getHCSupportBooks() {
        return this.hCSupportBooks;
    }

    public void setHCSupportBooks(Set<HCSupportBook> hCSupportBooks) {
        if (this.hCSupportBooks != null) {
            this.hCSupportBooks.forEach(i -> i.setHCUser(null));
        }
        if (hCSupportBooks != null) {
            hCSupportBooks.forEach(i -> i.setHCUser(this));
        }
        this.hCSupportBooks = hCSupportBooks;
    }

    public HCUser hCSupportBooks(Set<HCSupportBook> hCSupportBooks) {
        this.setHCSupportBooks(hCSupportBooks);
        return this;
    }

    public HCUser addHCSupportBook(HCSupportBook hCSupportBook) {
        this.hCSupportBooks.add(hCSupportBook);
        hCSupportBook.setHCUser(this);
        return this;
    }

    public HCUser removeHCSupportBook(HCSupportBook hCSupportBook) {
        this.hCSupportBooks.remove(hCSupportBook);
        hCSupportBook.setHCUser(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCUser)) {
            return false;
        }
        return id != null && id.equals(((HCUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCUser{" +
            "id=" + getId() +
            ", userIdInOn='" + getUserIdInOn() + "'" +
            ", name='" + getName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

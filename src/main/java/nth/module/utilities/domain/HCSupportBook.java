package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import nth.module.utilities.domain.enumeration.HCPriorityLevel;
import nth.module.utilities.domain.enumeration.HCStatus;
import nth.module.utilities.domain.enumeration.HCSupportType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HCSupportBook.
 */
@Entity
@Table(name = "hc_support_book")
public class HCSupportBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "room_id")
    private String roomId;

    @Column(name = "room_in_on_id")
    private String roomInOnId;

    @Column(name = "supporter_in_on_id")
    private String supporterInOnId;

    @Column(name = "supporter_in_on_id_name")
    private String supporterInOnIdName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private HCStatus status;

    @Lob
    @Column(name = "title")
    private byte[] title;

    @Column(name = "title_content_type")
    private String titleContentType;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "content_content_type")
    private String contentContentType;

    @Enumerated(EnumType.STRING)
    @Column(name = "priority")
    private HCPriorityLevel priority;

    @Column(name = "from_channel")
    private String fromChannel;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "vote_score")
    private String voteScore;

    @Column(name = "flower_inon_id")
    private String flowerInOnId;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "note")
    private String note;

    @Column(name = "device_id")
    private String deviceId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private HCSupportType type;

    @OneToMany(mappedBy = "hCSupportBook")
    @JsonIgnoreProperties(value = { "hCFileUploads", "hCSupportBook" }, allowSetters = true)
    private Set<HCMessage> hCMessages = new HashSet<>();

    @ManyToOne
    private HCUser hCUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getId() {
        return this.id;
    }

    public HCSupportBook id(Long id) {
        this.setId(id);
        return this;
    }

    public String getVoteScore() {
        return voteScore;
    }

    public void setVoteScore(String voteScore) {
        this.voteScore = voteScore;
    }

    public String getFlowerInOnId() {
        return flowerInOnId;
    }

    public void setFlowerInOnId(String flowerInOnId) {
        this.flowerInOnId = flowerInOnId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public HCSupportType getType() {
        return type;
    }

    public void setType(HCSupportType type) {
        this.type = type;
    }

    public String getCode() {
        return this.code;
    }

    public HCSupportBook code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomInOnId() {
        return roomInOnId;
    }

    public void setRoomInOnId(String roomInOnId) {
        this.roomInOnId = roomInOnId;
    }

    public String getSupporterInOnId() {
        return this.supporterInOnId;
    }

    public HCSupportBook supporterInOnId(String supporterInOnId) {
        this.setSupporterInOnId(supporterInOnId);
        return this;
    }

    public void setSupporterInOnId(String supporterInOnId) {
        this.supporterInOnId = supporterInOnId;
    }

    public String getSupporterInOnIdName() {
        return this.supporterInOnIdName;
    }

    public HCSupportBook supporterInOnIdName(String supporterInOnIdName) {
        this.setSupporterInOnIdName(supporterInOnIdName);
        return this;
    }

    public void setSupporterInOnIdName(String supporterInOnIdName) {
        this.supporterInOnIdName = supporterInOnIdName;
    }

    public HCStatus getStatus() {
        return this.status;
    }

    public HCSupportBook status(HCStatus status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(HCStatus status) {
        this.status = status;
    }

    public byte[] getTitle() {
        return this.title;
    }

    public HCSupportBook title(byte[] title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(byte[] title) {
        this.title = title;
    }

    public String getTitleContentType() {
        return this.titleContentType;
    }

    public HCSupportBook titleContentType(String titleContentType) {
        this.titleContentType = titleContentType;
        return this;
    }

    public void setTitleContentType(String titleContentType) {
        this.titleContentType = titleContentType;
    }

    public byte[] getContent() {
        return this.content;
    }

    public HCSupportBook content(byte[] content) {
        this.setContent(content);
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return this.contentContentType;
    }

    public HCSupportBook contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public HCPriorityLevel getPriority() {
        return this.priority;
    }

    public HCSupportBook priority(HCPriorityLevel priority) {
        this.setPriority(priority);
        return this;
    }

    public void setPriority(HCPriorityLevel priority) {
        this.priority = priority;
    }

    public String getFromChannel() {
        return this.fromChannel;
    }

    public HCSupportBook fromChannel(String fromChannel) {
        this.setFromChannel(fromChannel);
        return this;
    }

    public void setFromChannel(String fromChannel) {
        this.fromChannel = fromChannel;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public HCSupportBook createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public HCSupportBook createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public HCSupportBook updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public HCSupportBook updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Set<HCMessage> getHCMessages() {
        return this.hCMessages;
    }

    public void setHCMessages(Set<HCMessage> hCMessages) {
        if (this.hCMessages != null) {
            this.hCMessages.forEach(i -> i.setHCSupportBook(null));
        }
        if (hCMessages != null) {
            hCMessages.forEach(i -> i.setHCSupportBook(this));
        }
        this.hCMessages = hCMessages;
    }

    public HCSupportBook hCMessages(Set<HCMessage> hCMessages) {
        this.setHCMessages(hCMessages);
        return this;
    }

    public HCSupportBook addHCMessage(HCMessage hCMessage) {
        this.hCMessages.add(hCMessage);
        hCMessage.setHCSupportBook(this);
        return this;
    }

    public HCSupportBook removeHCMessage(HCMessage hCMessage) {
        this.hCMessages.remove(hCMessage);
        hCMessage.setHCSupportBook(null);
        return this;
    }

    public HCUser getHCUser() {
        return this.hCUser;
    }

    public void setHCUser(HCUser hCUser) {
        this.hCUser = hCUser;
    }

    public HCSupportBook hCUser(HCUser hCUser) {
        this.setHCUser(hCUser);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCSupportBook)) {
            return false;
        }
        return id != null && id.equals(((HCSupportBook) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCSupportBook{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", roomId=" + getRoomId() +
            ", roomInOnId=" + getRoomInOnId() +
            ", supporterInOnId='" + getSupporterInOnId() + "'" +
            ", supporterInOnIdName='" + getSupporterInOnIdName() + "'" +
            ", status='" + getStatus() + "'" +
            ", title='" + getTitle() + "'" +
            ", titleContentType='" + getTitleContentType() + "'" +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + getContentContentType() + "'" +
            ", priority='" + getPriority() + "'" +
            ", fromChannel='" + getFromChannel() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

package nth.module.utilities.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "user_sit")
public class UserSIT {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "enable")
    private boolean enable;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "last_login_success")
    private Instant lastLoginSuccess;

    @Column(name = "last_login_success_device_id")
    private String lastLoginSuccessDeviceId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Instant getLastLoginSuccess() {
        return lastLoginSuccess;
    }

    public void setLastLoginSuccess(Instant lastLoginSuccess) {
        this.lastLoginSuccess = lastLoginSuccess;
    }

    public String getLastLoginSuccessDeviceId() {
        return lastLoginSuccessDeviceId;
    }

    public void setLastLoginSuccessDeviceId(String lastLoginSuccessDeviceId) {
        this.lastLoginSuccessDeviceId = lastLoginSuccessDeviceId;
    }
}

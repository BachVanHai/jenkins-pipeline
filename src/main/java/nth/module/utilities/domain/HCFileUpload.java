package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HCFileUpload.
 */
@Entity
@Table(name = "hc_file_upload")
public class HCFileUpload implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Size(max = 1000)
    @Column(name = "file_path_id", length = 1000)
    private String filePathId;

    @Size(max = 1000)
    @Column(name = "file_path", length = 1000)
    private String filePath;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "update_by")
    private String updateBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "hCFileUploads", "hCSupportBook" }, allowSetters = true)
    private HCMessage hCMessage;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public HCFileUpload id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilePathId() {
        return this.filePathId;
    }

    public HCFileUpload filePathId(String filePathId) {
        this.setFilePathId(filePathId);
        return this;
    }

    public void setFilePathId(String filePathId) {
        this.filePathId = filePathId;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public HCFileUpload filePath(String filePath) {
        this.setFilePath(filePath);
        return this;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public HCFileUpload createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public HCFileUpload createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public HCFileUpload updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public HCFileUpload updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public HCMessage getHCMessage() {
        return this.hCMessage;
    }

    public void setHCMessage(HCMessage hCMessage) {
        this.hCMessage = hCMessage;
    }

    public HCFileUpload hCMessage(HCMessage hCMessage) {
        this.setHCMessage(hCMessage);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCFileUpload)) {
            return false;
        }
        return id != null && id.equals(((HCFileUpload) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCFileUpload{" +
            "id=" + getId() +
            ", filePathId='" + getFilePathId() + "'" +
            ", filePath='" + getFilePath() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

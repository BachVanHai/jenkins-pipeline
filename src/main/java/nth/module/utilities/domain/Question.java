package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Question.
 */
@Entity
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "question")
    private String question;

    @Enumerated(EnumType.STRING)
    @Column(name = "category_question_type")
    private CategoryQuestionType categoryQuestionType;

    @Lob
    @Column(name = "rs_text")
    private byte[] rsText;

    @Column(name = "rs_text_content_type")
    private String rsTextContentType;

    @Column(name = "rs_link_pdf")
    private String rsLinkPDF;

    @Column(name = "rs_link_yt")
    private String rsLinkYT;

    @Enumerated(EnumType.STRING)
    @Column(name = "apply_for")
    private ApplyFor applyFor;

    @Column(name = "reason_reject")
    private String reasonReject;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusQuestion status;

    @Column(name = "send_to")
    private String sendTo;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "enable_date")
    private Instant enableDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "update_by")
    private String updateBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "questions" }, allowSetters = true)
    private CategoryQuestion categoryQuestion;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Instant getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Instant enableDate) {
        this.enableDate = enableDate;
    }

    public CategoryQuestionType getCategoryQuestionType() {
        return categoryQuestionType;
    }

    public void setCategoryQuestionType(CategoryQuestionType categoryQuestionType) {
        this.categoryQuestionType = categoryQuestionType;
    }

    public Long getId() {
        return this.id;
    }

    public Question id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public Question code (String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String name) {
        this.code = name;
    }

    public String getQuestion() {
        return this.question;
    }

    public Question questTion(String questTion) {
        this.setQuestion(questTion);
        return this;
    }

    public void setQuestion(String questTion) {
        this.question = questTion;
    }

    public byte[] getRsText() {
        return this.rsText;
    }

    public Question rsText(byte[] rsText) {
        this.setRsText(rsText);
        return this;
    }

    public void setRsText(byte[] rsText) {
        this.rsText = rsText;
    }

    public String getRsTextContentType() {
        return this.rsTextContentType;
    }

    public Question rsTextContentType(String rsTextContentType) {
        this.rsTextContentType = rsTextContentType;
        return this;
    }

    public void setRsTextContentType(String rsTextContentType) {
        this.rsTextContentType = rsTextContentType;
    }

    public String getRsLinkPDF() {
        return this.rsLinkPDF;
    }

    public Question rsLinkPDF(String rsLinkPDF) {
        this.setRsLinkPDF(rsLinkPDF);
        return this;
    }

    public void setRsLinkPDF(String rsLinkPDF) {
        this.rsLinkPDF = rsLinkPDF;
    }

    public String getRsLinkYT() {
        return this.rsLinkYT;
    }

    public Question rsLinkYT(String rsLinkYT) {
        this.setRsLinkYT(rsLinkYT);
        return this;
    }

    public void setRsLinkYT(String rsLinkYT) {
        this.rsLinkYT = rsLinkYT;
    }

    public ApplyFor getApplyFor() {
        return this.applyFor;
    }

    public Question applyFor(ApplyFor applyFor) {
        this.setApplyFor(applyFor);
        return this;
    }

    public void setApplyFor(ApplyFor applyFor) {
        this.applyFor = applyFor;
    }

    public String getReasonReject() {
        return this.reasonReject;
    }

    public Question reasonReject(String reasonReject) {
        this.setReasonReject(reasonReject);
        return this;
    }

    public void setReasonReject(String reasonReject) {
        this.reasonReject = reasonReject;
    }

    public StatusQuestion getStatus() {
        return this.status;
    }

    public Question status(StatusQuestion status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(StatusQuestion status) {
        this.status = status;
    }

    public String getSendTo() {
        return this.sendTo;
    }

    public Question sendTo(String sendTo) {
        this.setSendTo(sendTo);
        return this;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public Question deleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Question createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Question createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdateDate() {
        return this.updateDate;
    }

    public Question updateDate(Instant updateDate) {
        this.setUpdateDate(updateDate);
        return this;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Question updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public CategoryQuestion getCategoryQuestion() {
        return this.categoryQuestion;
    }

    public void setCategoryQuestion(CategoryQuestion categoryQuestion) {
        this.categoryQuestion = categoryQuestion;
    }

    public Question categoryQuestion(CategoryQuestion categoryQuestion) {
        this.setCategoryQuestion(categoryQuestion);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        return id != null && id.equals(((Question) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", name='" + getCode() + "'" +
            ", questTion='" + getQuestion() + "'" +
            ", rsText='" + getRsText() + "'" +
            ", rsTextContentType='" + getRsTextContentType() + "'" +
            ", rsLinkPDF='" + getRsLinkPDF() + "'" +
            ", rsLinkYT='" + getRsLinkYT() + "'" +
            ", applyFor='" + getApplyFor() + "'" +
            ", reasonReject='" + getReasonReject() + "'" +
            ", status='" + getStatus() + "'" +
            ", sendTo='" + getSendTo() + "'" +
            ", deleted='" + getDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

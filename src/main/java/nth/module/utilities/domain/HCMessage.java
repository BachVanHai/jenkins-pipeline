package nth.module.utilities.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import nth.module.utilities.domain.enumeration.TypeMessage;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HCMessage.
 */
@Entity
@Table(name = "hc_message")
public class HCMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "content_content_type")
    private String contentContentType;

    @Column(name = "from_user")
    private String fromUser;

    @Column(name = "to_user")
    private String toUser;

    @Column(name = "is_in_on")
    private Boolean isInOn;

    @Column(name = "send_date")
    private Instant sendDate;

    @Column(name = "room_id")
    private String roomId;

    @Column(name = "receive_date")
    private Instant receiveDate;

    @Column(name = "started_date")
    private Instant startedDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "reader_id")
    private String readerId;

    @Column(name = "notification")
    private Long notification;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeMessage type;

    @OneToMany(mappedBy = "hCMessage")
    @JsonIgnoreProperties(value = { "hCMessage" }, allowSetters = true)
    private Set<HCFileUpload> hCFileUploads = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "hCMessages", "hCUser" }, allowSetters = true)
    private HCSupportBook hCSupportBook;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getNotification() {
        return notification;
    }

    public void setNotification(Long notification) {
        this.notification = notification;
    }

    public TypeMessage getType() {
        return type;
    }

    public void setType(TypeMessage type) {
        this.type = type;
    }

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public Long getId() {
        return this.id;
    }

    public HCMessage id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getContent() {
        return this.content;
    }

    public HCMessage content(byte[] content) {
        this.setContent(content);
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return this.contentContentType;
    }

    public HCMessage contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public String getFromUser() {
        return this.fromUser;
    }

    public HCMessage fromUser(String fromUser) {
        this.setFromUser(fromUser);
        return this;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return this.toUser;
    }

    public HCMessage toUser(String toUser) {
        this.setToUser(toUser);
        return this;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public Boolean getIsInOn() {
        return this.isInOn;
    }

    public HCMessage isInOn(Boolean isInOn) {
        this.setIsInOn(isInOn);
        return this;
    }

    public void setIsInOn(Boolean isInOn) {
        this.isInOn = isInOn;
    }

    public Instant getSendDate() {
        return this.sendDate;
    }

    public HCMessage sendDate(Instant sendDate) {
        this.setSendDate(sendDate);
        return this;
    }

    public void setSendDate(Instant sendDate) {
        this.sendDate = sendDate;
    }

    public String getRoomId() {
        return this.roomId;
    }

    public HCMessage roomId(String roomId) {
        this.setRoomId(roomId);
        return this;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Instant getReceiveDate() {
        return this.receiveDate;
    }

    public HCMessage receiveDate(Instant receiveDate) {
        this.setReceiveDate(receiveDate);
        return this;
    }

    public void setReceiveDate(Instant receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Instant getStartedDate() {
        return this.startedDate;
    }

    public HCMessage startedDate(Instant startedDate) {
        this.setStartedDate(startedDate);
        return this;
    }

    public void setStartedDate(Instant startedDate) {
        this.startedDate = startedDate;
    }

    public Instant getEndDate() {
        return this.endDate;
    }

    public HCMessage endDate(Instant endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Set<HCFileUpload> getHCFileUploads() {
        return this.hCFileUploads;
    }

    public void setHCFileUploads(Set<HCFileUpload> hCFileUploads) {
        if (this.hCFileUploads != null) {
            this.hCFileUploads.forEach(i -> i.setHCMessage(null));
        }
        if (hCFileUploads != null) {
            hCFileUploads.forEach(i -> i.setHCMessage(this));
        }
        this.hCFileUploads = hCFileUploads;
    }

    public HCMessage hCFileUploads(Set<HCFileUpload> hCFileUploads) {
        this.setHCFileUploads(hCFileUploads);
        return this;
    }

    public HCMessage addHCFileUpload(HCFileUpload hCFileUpload) {
        this.hCFileUploads.add(hCFileUpload);
        hCFileUpload.setHCMessage(this);
        return this;
    }

    public HCMessage removeHCFileUpload(HCFileUpload hCFileUpload) {
        this.hCFileUploads.remove(hCFileUpload);
        hCFileUpload.setHCMessage(null);
        return this;
    }

    public HCSupportBook getHCSupportBook() {
        return this.hCSupportBook;
    }

    public void setHCSupportBook(HCSupportBook hCSupportBook) {
        this.hCSupportBook = hCSupportBook;
    }

    public HCMessage hCSupportBook(HCSupportBook hCSupportBook) {
        this.setHCSupportBook(hCSupportBook);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCMessage)) {
            return false;
        }
        return id != null && id.equals(((HCMessage) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCMessage{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + getContentContentType() + "'" +
            ", fromUser='" + getFromUser() + "'" +
            ", toUser='" + getToUser() + "'" +
            ", isInOn='" + getIsInOn() + "'" +
            ", sendDate='" + getSendDate() + "'" +
            ", roomId='" + getRoomId() + "'" +
            ", receiveDate='" + getReceiveDate() + "'" +
            ", startedDate='" + getStartedDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}

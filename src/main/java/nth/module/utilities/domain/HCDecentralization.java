package nth.module.utilities.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.domain.enumeration.HCDepartment;
import nth.module.utilities.domain.enumeration.HCSupportType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HCDecentralization.
 */
@Entity
@Table(name = "hc_decentralization")
public class HCDecentralization implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private String userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "decentralization")
    private HCDecentralizationType decentralization;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "update_by")
    private String updateBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "department")
    private HCDepartment department;

    @Column(name = "processing")
    private Long processing;

    @Enumerated(EnumType.STRING)
    @Column(name = "hc_type")
    private HCSupportType hcSupportType;

    @Column(name = "enable")
    private Boolean enable;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Long getProcessing() {
        return processing;
    }

    public void setProcessing(Long processing) {
        this.processing = processing;
    }

    public HCDepartment getDepartment() {
        return department;
    }

    public void setDepartment(HCDepartment department) {
        this.department = department;
    }

    public HCSupportType getHcSupportType() {
        return hcSupportType;
    }

    public void setHcSupportType(HCSupportType hcSupportType) {
        this.hcSupportType = hcSupportType;
    }

    public Long getId() {
        return this.id;
    }

    public HCDecentralization id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public HCDecentralization userId(String userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public HCDecentralizationType getDecentralization() {
        return this.decentralization;
    }

    public HCDecentralization decentralization(HCDecentralizationType decentralization) {
        this.setDecentralization(decentralization);
        return this;
    }

    public void setDecentralization(HCDecentralizationType decentralization) {
        this.decentralization = decentralization;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public HCDecentralization createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public HCDecentralization createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public HCDecentralization updatedDate(Instant updatedDate) {
        this.setUpdatedDate(updatedDate);
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public HCDecentralization updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HCDecentralization)) {
            return false;
        }
        return id != null && id.equals(((HCDecentralization) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HCDecentralization{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", decentralization='" + getDecentralization() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            "}";
    }
}

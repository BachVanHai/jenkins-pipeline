package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.HCMessage;
import nth.module.utilities.repository.HCMessageRepository;
import nth.module.utilities.service.dto.HCMessageDTO;
import nth.module.utilities.service.mapper.HCMessageMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link HCMessageResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class HCMessageResourceIT {

    private static final byte[] DEFAULT_CONTENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CONTENT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_CONTENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CONTENT_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_FROM_USER = "AAAAAAAAAA";
    private static final String UPDATED_FROM_USER = "BBBBBBBBBB";

    private static final String DEFAULT_TO_USER = "AAAAAAAAAA";
    private static final String UPDATED_TO_USER = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_IN_ON = false;
    private static final Boolean UPDATED_IS_IN_ON = true;

    private static final Instant DEFAULT_SEND_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_SEND_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ROOM_ID = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_RECEIVE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_RECEIVE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_STARTED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_STARTED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/hc-messages";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HCMessageRepository hCMessageRepository;

    @Autowired
    private HCMessageMapper hCMessageMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHCMessageMockMvc;

    private HCMessage hCMessage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCMessage createEntity(EntityManager em) {
        HCMessage hCMessage = new HCMessage()
            .content(DEFAULT_CONTENT)
            .contentContentType(DEFAULT_CONTENT_CONTENT_TYPE)
            .fromUser(DEFAULT_FROM_USER)
            .toUser(DEFAULT_TO_USER)
            .isInOn(DEFAULT_IS_IN_ON)
            .sendDate(DEFAULT_SEND_DATE)
            .roomId(DEFAULT_ROOM_ID)
            .receiveDate(DEFAULT_RECEIVE_DATE)
            .startedDate(DEFAULT_STARTED_DATE)
            .endDate(DEFAULT_END_DATE);
        return hCMessage;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCMessage createUpdatedEntity(EntityManager em) {
        HCMessage hCMessage = new HCMessage()
            .content(UPDATED_CONTENT)
            .contentContentType(UPDATED_CONTENT_CONTENT_TYPE)
            .fromUser(UPDATED_FROM_USER)
            .toUser(UPDATED_TO_USER)
            .isInOn(UPDATED_IS_IN_ON)
            .sendDate(UPDATED_SEND_DATE)
            .roomId(UPDATED_ROOM_ID)
            .receiveDate(UPDATED_RECEIVE_DATE)
            .startedDate(UPDATED_STARTED_DATE)
            .endDate(UPDATED_END_DATE);
        return hCMessage;
    }

    @BeforeEach
    public void initTest() {
        hCMessage = createEntity(em);
    }

    @Test
    @Transactional
    void createHCMessage() throws Exception {
        int databaseSizeBeforeCreate = hCMessageRepository.findAll().size();
        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);
        restHCMessageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCMessageDTO)))
            .andExpect(status().isCreated());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeCreate + 1);
        HCMessage testHCMessage = hCMessageList.get(hCMessageList.size() - 1);
        assertThat(testHCMessage.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testHCMessage.getContentContentType()).isEqualTo(DEFAULT_CONTENT_CONTENT_TYPE);
        assertThat(testHCMessage.getFromUser()).isEqualTo(DEFAULT_FROM_USER);
        assertThat(testHCMessage.getToUser()).isEqualTo(DEFAULT_TO_USER);
        assertThat(testHCMessage.getIsInOn()).isEqualTo(DEFAULT_IS_IN_ON);
        assertThat(testHCMessage.getSendDate()).isEqualTo(DEFAULT_SEND_DATE);
        assertThat(testHCMessage.getRoomId()).isEqualTo(DEFAULT_ROOM_ID);
        assertThat(testHCMessage.getReceiveDate()).isEqualTo(DEFAULT_RECEIVE_DATE);
        assertThat(testHCMessage.getStartedDate()).isEqualTo(DEFAULT_STARTED_DATE);
        assertThat(testHCMessage.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    void createHCMessageWithExistingId() throws Exception {
        // Create the HCMessage with an existing ID
        hCMessage.setId(1L);
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        int databaseSizeBeforeCreate = hCMessageRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHCMessageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCMessageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllHCMessages() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        // Get all the hCMessageList
        restHCMessageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hCMessage.getId().intValue())))
            .andExpect(jsonPath("$.[*].contentContentType").value(hasItem(DEFAULT_CONTENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTENT))))
            .andExpect(jsonPath("$.[*].fromUser").value(hasItem(DEFAULT_FROM_USER)))
            .andExpect(jsonPath("$.[*].toUser").value(hasItem(DEFAULT_TO_USER)))
            .andExpect(jsonPath("$.[*].isInOn").value(hasItem(DEFAULT_IS_IN_ON.booleanValue())))
            .andExpect(jsonPath("$.[*].sendDate").value(hasItem(DEFAULT_SEND_DATE.toString())))
            .andExpect(jsonPath("$.[*].roomId").value(hasItem(DEFAULT_ROOM_ID)))
            .andExpect(jsonPath("$.[*].receiveDate").value(hasItem(DEFAULT_RECEIVE_DATE.toString())))
            .andExpect(jsonPath("$.[*].startedDate").value(hasItem(DEFAULT_STARTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }

    @Test
    @Transactional
    void getHCMessage() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        // Get the hCMessage
        restHCMessageMockMvc
            .perform(get(ENTITY_API_URL_ID, hCMessage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hCMessage.getId().intValue()))
            .andExpect(jsonPath("$.contentContentType").value(DEFAULT_CONTENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.content").value(Base64Utils.encodeToString(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.fromUser").value(DEFAULT_FROM_USER))
            .andExpect(jsonPath("$.toUser").value(DEFAULT_TO_USER))
            .andExpect(jsonPath("$.isInOn").value(DEFAULT_IS_IN_ON.booleanValue()))
            .andExpect(jsonPath("$.sendDate").value(DEFAULT_SEND_DATE.toString()))
            .andExpect(jsonPath("$.roomId").value(DEFAULT_ROOM_ID))
            .andExpect(jsonPath("$.receiveDate").value(DEFAULT_RECEIVE_DATE.toString()))
            .andExpect(jsonPath("$.startedDate").value(DEFAULT_STARTED_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingHCMessage() throws Exception {
        // Get the hCMessage
        restHCMessageMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHCMessage() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();

        // Update the hCMessage
        HCMessage updatedHCMessage = hCMessageRepository.findById(hCMessage.getId()).get();
        // Disconnect from session so that the updates on updatedHCMessage are not directly saved in db
        em.detach(updatedHCMessage);
        updatedHCMessage
            .content(UPDATED_CONTENT)
            .contentContentType(UPDATED_CONTENT_CONTENT_TYPE)
            .fromUser(UPDATED_FROM_USER)
            .toUser(UPDATED_TO_USER)
            .isInOn(UPDATED_IS_IN_ON)
            .sendDate(UPDATED_SEND_DATE)
            .roomId(UPDATED_ROOM_ID)
            .receiveDate(UPDATED_RECEIVE_DATE)
            .startedDate(UPDATED_STARTED_DATE)
            .endDate(UPDATED_END_DATE);
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(updatedHCMessage);

        restHCMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCMessageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isOk());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
        HCMessage testHCMessage = hCMessageList.get(hCMessageList.size() - 1);
        assertThat(testHCMessage.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testHCMessage.getContentContentType()).isEqualTo(UPDATED_CONTENT_CONTENT_TYPE);
        assertThat(testHCMessage.getFromUser()).isEqualTo(UPDATED_FROM_USER);
        assertThat(testHCMessage.getToUser()).isEqualTo(UPDATED_TO_USER);
        assertThat(testHCMessage.getIsInOn()).isEqualTo(UPDATED_IS_IN_ON);
        assertThat(testHCMessage.getSendDate()).isEqualTo(UPDATED_SEND_DATE);
        assertThat(testHCMessage.getRoomId()).isEqualTo(UPDATED_ROOM_ID);
        assertThat(testHCMessage.getReceiveDate()).isEqualTo(UPDATED_RECEIVE_DATE);
        assertThat(testHCMessage.getStartedDate()).isEqualTo(UPDATED_STARTED_DATE);
        assertThat(testHCMessage.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void putNonExistingHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCMessageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCMessageDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHCMessageWithPatch() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();

        // Update the hCMessage using partial update
        HCMessage partialUpdatedHCMessage = new HCMessage();
        partialUpdatedHCMessage.setId(hCMessage.getId());

        partialUpdatedHCMessage.fromUser(UPDATED_FROM_USER).startedDate(UPDATED_STARTED_DATE);

        restHCMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCMessage))
            )
            .andExpect(status().isOk());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
        HCMessage testHCMessage = hCMessageList.get(hCMessageList.size() - 1);
        assertThat(testHCMessage.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testHCMessage.getContentContentType()).isEqualTo(DEFAULT_CONTENT_CONTENT_TYPE);
        assertThat(testHCMessage.getFromUser()).isEqualTo(UPDATED_FROM_USER);
        assertThat(testHCMessage.getToUser()).isEqualTo(DEFAULT_TO_USER);
        assertThat(testHCMessage.getIsInOn()).isEqualTo(DEFAULT_IS_IN_ON);
        assertThat(testHCMessage.getSendDate()).isEqualTo(DEFAULT_SEND_DATE);
        assertThat(testHCMessage.getRoomId()).isEqualTo(DEFAULT_ROOM_ID);
        assertThat(testHCMessage.getReceiveDate()).isEqualTo(DEFAULT_RECEIVE_DATE);
        assertThat(testHCMessage.getStartedDate()).isEqualTo(UPDATED_STARTED_DATE);
        assertThat(testHCMessage.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    void fullUpdateHCMessageWithPatch() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();

        // Update the hCMessage using partial update
        HCMessage partialUpdatedHCMessage = new HCMessage();
        partialUpdatedHCMessage.setId(hCMessage.getId());

        partialUpdatedHCMessage
            .content(UPDATED_CONTENT)
            .contentContentType(UPDATED_CONTENT_CONTENT_TYPE)
            .fromUser(UPDATED_FROM_USER)
            .toUser(UPDATED_TO_USER)
            .isInOn(UPDATED_IS_IN_ON)
            .sendDate(UPDATED_SEND_DATE)
            .roomId(UPDATED_ROOM_ID)
            .receiveDate(UPDATED_RECEIVE_DATE)
            .startedDate(UPDATED_STARTED_DATE)
            .endDate(UPDATED_END_DATE);

        restHCMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCMessage))
            )
            .andExpect(status().isOk());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
        HCMessage testHCMessage = hCMessageList.get(hCMessageList.size() - 1);
        assertThat(testHCMessage.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testHCMessage.getContentContentType()).isEqualTo(UPDATED_CONTENT_CONTENT_TYPE);
        assertThat(testHCMessage.getFromUser()).isEqualTo(UPDATED_FROM_USER);
        assertThat(testHCMessage.getToUser()).isEqualTo(UPDATED_TO_USER);
        assertThat(testHCMessage.getIsInOn()).isEqualTo(UPDATED_IS_IN_ON);
        assertThat(testHCMessage.getSendDate()).isEqualTo(UPDATED_SEND_DATE);
        assertThat(testHCMessage.getRoomId()).isEqualTo(UPDATED_ROOM_ID);
        assertThat(testHCMessage.getReceiveDate()).isEqualTo(UPDATED_RECEIVE_DATE);
        assertThat(testHCMessage.getStartedDate()).isEqualTo(UPDATED_STARTED_DATE);
        assertThat(testHCMessage.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hCMessageDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHCMessage() throws Exception {
        int databaseSizeBeforeUpdate = hCMessageRepository.findAll().size();
        hCMessage.setId(count.incrementAndGet());

        // Create the HCMessage
        HCMessageDTO hCMessageDTO = hCMessageMapper.toDto(hCMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCMessageMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(hCMessageDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCMessage in the database
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHCMessage() throws Exception {
        // Initialize the database
        hCMessageRepository.saveAndFlush(hCMessage);

        int databaseSizeBeforeDelete = hCMessageRepository.findAll().size();

        // Delete the hCMessage
        restHCMessageMockMvc
            .perform(delete(ENTITY_API_URL_ID, hCMessage.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HCMessage> hCMessageList = hCMessageRepository.findAll();
        assertThat(hCMessageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

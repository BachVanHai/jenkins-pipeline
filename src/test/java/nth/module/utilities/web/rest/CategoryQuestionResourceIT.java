package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.CategoryQuestion;
import nth.module.utilities.domain.enumeration.CategoryQuestionType;
import nth.module.utilities.repository.CategoryQuestionRepository;
import nth.module.utilities.service.dto.CategoryQuestionDTO;
import nth.module.utilities.service.mapper.CategoryQuestionMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CategoryQuestionResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CategoryQuestionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final CategoryQuestionType DEFAULT_CATEGORY_QUESTION_TYPE = CategoryQuestionType.CHTG;
    private static final CategoryQuestionType UPDATED_CATEGORY_QUESTION_TYPE = CategoryQuestionType.HDSD;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/category-questions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CategoryQuestionRepository categoryQuestionRepository;

    @Autowired
    private CategoryQuestionMapper categoryQuestionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategoryQuestionMockMvc;

    private CategoryQuestion categoryQuestion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryQuestion createEntity(EntityManager em) {
        CategoryQuestion categoryQuestion = new CategoryQuestion()
            .name(DEFAULT_NAME)
            .categoryQuestionType(DEFAULT_CATEGORY_QUESTION_TYPE)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updateDate(DEFAULT_UPDATE_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return categoryQuestion;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryQuestion createUpdatedEntity(EntityManager em) {
        CategoryQuestion categoryQuestion = new CategoryQuestion()
            .name(UPDATED_NAME)
            .categoryQuestionType(UPDATED_CATEGORY_QUESTION_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        return categoryQuestion;
    }

    @BeforeEach
    public void initTest() {
        categoryQuestion = createEntity(em);
    }

    @Test
    @Transactional
    void createCategoryQuestion() throws Exception {
        int databaseSizeBeforeCreate = categoryQuestionRepository.findAll().size();
        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);
        restCategoryQuestionMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        CategoryQuestion testCategoryQuestion = categoryQuestionList.get(categoryQuestionList.size() - 1);
        assertThat(testCategoryQuestion.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCategoryQuestion.getCategoryQuestionType()).isEqualTo(DEFAULT_CATEGORY_QUESTION_TYPE);
        assertThat(testCategoryQuestion.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCategoryQuestion.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCategoryQuestion.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testCategoryQuestion.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    void createCategoryQuestionWithExistingId() throws Exception {
        // Create the CategoryQuestion with an existing ID
        categoryQuestion.setId(1L);
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        int databaseSizeBeforeCreate = categoryQuestionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoryQuestionMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCategoryQuestions() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        // Get all the categoryQuestionList
        restCategoryQuestionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoryQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].categoryQuestionType").value(hasItem(DEFAULT_CATEGORY_QUESTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)));
    }

    @Test
    @Transactional
    void getCategoryQuestion() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        // Get the categoryQuestion
        restCategoryQuestionMockMvc
            .perform(get(ENTITY_API_URL_ID, categoryQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categoryQuestion.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.categoryQuestionType").value(DEFAULT_CATEGORY_QUESTION_TYPE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY));
    }

    @Test
    @Transactional
    void getNonExistingCategoryQuestion() throws Exception {
        // Get the categoryQuestion
        restCategoryQuestionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCategoryQuestion() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();

        // Update the categoryQuestion
        CategoryQuestion updatedCategoryQuestion = categoryQuestionRepository.findById(categoryQuestion.getId()).get();
        // Disconnect from session so that the updates on updatedCategoryQuestion are not directly saved in db
        em.detach(updatedCategoryQuestion);
        updatedCategoryQuestion
            .name(UPDATED_NAME)
            .categoryQuestionType(UPDATED_CATEGORY_QUESTION_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(updatedCategoryQuestion);

        restCategoryQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, categoryQuestionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isOk());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
        CategoryQuestion testCategoryQuestion = categoryQuestionList.get(categoryQuestionList.size() - 1);
        assertThat(testCategoryQuestion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryQuestion.getCategoryQuestionType()).isEqualTo(UPDATED_CATEGORY_QUESTION_TYPE);
        assertThat(testCategoryQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCategoryQuestion.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCategoryQuestion.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testCategoryQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void putNonExistingCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, categoryQuestionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCategoryQuestionWithPatch() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();

        // Update the categoryQuestion using partial update
        CategoryQuestion partialUpdatedCategoryQuestion = new CategoryQuestion();
        partialUpdatedCategoryQuestion.setId(categoryQuestion.getId());

        partialUpdatedCategoryQuestion.name(UPDATED_NAME).createdDate(UPDATED_CREATED_DATE).updateBy(UPDATED_UPDATE_BY);

        restCategoryQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCategoryQuestion.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCategoryQuestion))
            )
            .andExpect(status().isOk());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
        CategoryQuestion testCategoryQuestion = categoryQuestionList.get(categoryQuestionList.size() - 1);
        assertThat(testCategoryQuestion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryQuestion.getCategoryQuestionType()).isEqualTo(DEFAULT_CATEGORY_QUESTION_TYPE);
        assertThat(testCategoryQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCategoryQuestion.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCategoryQuestion.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testCategoryQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void fullUpdateCategoryQuestionWithPatch() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();

        // Update the categoryQuestion using partial update
        CategoryQuestion partialUpdatedCategoryQuestion = new CategoryQuestion();
        partialUpdatedCategoryQuestion.setId(categoryQuestion.getId());

        partialUpdatedCategoryQuestion
            .name(UPDATED_NAME)
            .categoryQuestionType(UPDATED_CATEGORY_QUESTION_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restCategoryQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCategoryQuestion.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCategoryQuestion))
            )
            .andExpect(status().isOk());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
        CategoryQuestion testCategoryQuestion = categoryQuestionList.get(categoryQuestionList.size() - 1);
        assertThat(testCategoryQuestion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryQuestion.getCategoryQuestionType()).isEqualTo(UPDATED_CATEGORY_QUESTION_TYPE);
        assertThat(testCategoryQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCategoryQuestion.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCategoryQuestion.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testCategoryQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void patchNonExistingCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, categoryQuestionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCategoryQuestion() throws Exception {
        int databaseSizeBeforeUpdate = categoryQuestionRepository.findAll().size();
        categoryQuestion.setId(count.incrementAndGet());

        // Create the CategoryQuestion
        CategoryQuestionDTO categoryQuestionDTO = categoryQuestionMapper.toDto(categoryQuestion);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCategoryQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(categoryQuestionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CategoryQuestion in the database
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCategoryQuestion() throws Exception {
        // Initialize the database
        categoryQuestionRepository.saveAndFlush(categoryQuestion);

        int databaseSizeBeforeDelete = categoryQuestionRepository.findAll().size();

        // Delete the categoryQuestion
        restCategoryQuestionMockMvc
            .perform(delete(ENTITY_API_URL_ID, categoryQuestion.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
        assertThat(categoryQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

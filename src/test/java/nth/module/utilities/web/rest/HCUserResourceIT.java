package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.HCUser;
import nth.module.utilities.repository.HCUserRepository;
import nth.module.utilities.service.dto.HCUserDTO;
import nth.module.utilities.service.mapper.HCUserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HCUserResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class HCUserResourceIT {

    private static final String DEFAULT_USER_ID_IN_ON = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID_IN_ON = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/hc-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HCUserRepository hCUserRepository;

    @Autowired
    private HCUserMapper hCUserMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHCUserMockMvc;

    private HCUser hCUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCUser createEntity(EntityManager em) {
        HCUser hCUser = new HCUser()
            .userIdInOn(DEFAULT_USER_ID_IN_ON)
            .name(DEFAULT_NAME)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .email(DEFAULT_EMAIL)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return hCUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCUser createUpdatedEntity(EntityManager em) {
        HCUser hCUser = new HCUser()
            .userIdInOn(UPDATED_USER_ID_IN_ON)
            .name(UPDATED_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        return hCUser;
    }

    @BeforeEach
    public void initTest() {
        hCUser = createEntity(em);
    }

    @Test
    @Transactional
    void createHCUser() throws Exception {
        int databaseSizeBeforeCreate = hCUserRepository.findAll().size();
        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);
        restHCUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCUserDTO)))
            .andExpect(status().isCreated());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeCreate + 1);
        HCUser testHCUser = hCUserList.get(hCUserList.size() - 1);
        assertThat(testHCUser.getUserIdInOn()).isEqualTo(DEFAULT_USER_ID_IN_ON);
        assertThat(testHCUser.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHCUser.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testHCUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testHCUser.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testHCUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testHCUser.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testHCUser.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    void createHCUserWithExistingId() throws Exception {
        // Create the HCUser with an existing ID
        hCUser.setId(1L);
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        int databaseSizeBeforeCreate = hCUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHCUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllHCUsers() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        // Get all the hCUserList
        restHCUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hCUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userIdInOn").value(hasItem(DEFAULT_USER_ID_IN_ON)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)));
    }

    @Test
    @Transactional
    void getHCUser() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        // Get the hCUser
        restHCUserMockMvc
            .perform(get(ENTITY_API_URL_ID, hCUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hCUser.getId().intValue()))
            .andExpect(jsonPath("$.userIdInOn").value(DEFAULT_USER_ID_IN_ON))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY));
    }

    @Test
    @Transactional
    void getNonExistingHCUser() throws Exception {
        // Get the hCUser
        restHCUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHCUser() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();

        // Update the hCUser
        HCUser updatedHCUser = hCUserRepository.findById(hCUser.getId()).get();
        // Disconnect from session so that the updates on updatedHCUser are not directly saved in db
        em.detach(updatedHCUser);
        updatedHCUser
            .userIdInOn(UPDATED_USER_ID_IN_ON)
            .name(UPDATED_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        HCUserDTO hCUserDTO = hCUserMapper.toDto(updatedHCUser);

        restHCUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isOk());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
        HCUser testHCUser = hCUserList.get(hCUserList.size() - 1);
        assertThat(testHCUser.getUserIdInOn()).isEqualTo(UPDATED_USER_ID_IN_ON);
        assertThat(testHCUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHCUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testHCUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testHCUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCUser.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void putNonExistingHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCUserDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCUserDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHCUserWithPatch() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();

        // Update the hCUser using partial update
        HCUser partialUpdatedHCUser = new HCUser();
        partialUpdatedHCUser.setId(hCUser.getId());

        partialUpdatedHCUser
            .name(UPDATED_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restHCUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCUser))
            )
            .andExpect(status().isOk());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
        HCUser testHCUser = hCUserList.get(hCUserList.size() - 1);
        assertThat(testHCUser.getUserIdInOn()).isEqualTo(DEFAULT_USER_ID_IN_ON);
        assertThat(testHCUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHCUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testHCUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testHCUser.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testHCUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testHCUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCUser.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void fullUpdateHCUserWithPatch() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();

        // Update the hCUser using partial update
        HCUser partialUpdatedHCUser = new HCUser();
        partialUpdatedHCUser.setId(hCUser.getId());

        partialUpdatedHCUser
            .userIdInOn(UPDATED_USER_ID_IN_ON)
            .name(UPDATED_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restHCUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCUser))
            )
            .andExpect(status().isOk());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
        HCUser testHCUser = hCUserList.get(hCUserList.size() - 1);
        assertThat(testHCUser.getUserIdInOn()).isEqualTo(UPDATED_USER_ID_IN_ON);
        assertThat(testHCUser.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHCUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testHCUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testHCUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCUser.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCUser.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void patchNonExistingHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hCUserDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHCUser() throws Exception {
        int databaseSizeBeforeUpdate = hCUserRepository.findAll().size();
        hCUser.setId(count.incrementAndGet());

        // Create the HCUser
        HCUserDTO hCUserDTO = hCUserMapper.toDto(hCUser);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCUserMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(hCUserDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCUser in the database
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHCUser() throws Exception {
        // Initialize the database
        hCUserRepository.saveAndFlush(hCUser);

        int databaseSizeBeforeDelete = hCUserRepository.findAll().size();

        // Delete the hCUser
        restHCUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, hCUser.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HCUser> hCUserList = hCUserRepository.findAll();
        assertThat(hCUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

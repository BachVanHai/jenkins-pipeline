package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.HCDecentralization;
import nth.module.utilities.domain.enumeration.HCDecentralizationType;
import nth.module.utilities.repository.HCDecentralizationRepository;
import nth.module.utilities.service.dto.HCDecentralizationDTO;
import nth.module.utilities.service.mapper.HCDecentralizationMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HCDecentralizationResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class HCDecentralizationResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final HCDecentralizationType DEFAULT_DECENTRALIZATION = HCDecentralizationType.AD_OP;
    private static final HCDecentralizationType UPDATED_DECENTRALIZATION = HCDecentralizationType.MAJOR;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/hc-decentralizations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HCDecentralizationRepository hCDecentralizationRepository;

    @Autowired
    private HCDecentralizationMapper hCDecentralizationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHCDecentralizationMockMvc;

    private HCDecentralization hCDecentralization;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCDecentralization createEntity(EntityManager em) {
        HCDecentralization hCDecentralization = new HCDecentralization()
            .userId(DEFAULT_USER_ID)
            .decentralization(DEFAULT_DECENTRALIZATION)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return hCDecentralization;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCDecentralization createUpdatedEntity(EntityManager em) {
        HCDecentralization hCDecentralization = new HCDecentralization()
            .userId(UPDATED_USER_ID)
            .decentralization(UPDATED_DECENTRALIZATION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        return hCDecentralization;
    }

    @BeforeEach
    public void initTest() {
        hCDecentralization = createEntity(em);
    }

    @Test
    @Transactional
    void createHCDecentralization() throws Exception {
        int databaseSizeBeforeCreate = hCDecentralizationRepository.findAll().size();
        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);
        restHCDecentralizationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeCreate + 1);
        HCDecentralization testHCDecentralization = hCDecentralizationList.get(hCDecentralizationList.size() - 1);
        assertThat(testHCDecentralization.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testHCDecentralization.getDecentralization()).isEqualTo(DEFAULT_DECENTRALIZATION);
        assertThat(testHCDecentralization.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testHCDecentralization.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testHCDecentralization.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testHCDecentralization.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    void createHCDecentralizationWithExistingId() throws Exception {
        // Create the HCDecentralization with an existing ID
        hCDecentralization.setId(1L);
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        int databaseSizeBeforeCreate = hCDecentralizationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHCDecentralizationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllHCDecentralizations() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        // Get all the hCDecentralizationList
        restHCDecentralizationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hCDecentralization.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].decentralization").value(hasItem(DEFAULT_DECENTRALIZATION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)));
    }

    @Test
    @Transactional
    void getHCDecentralization() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        // Get the hCDecentralization
        restHCDecentralizationMockMvc
            .perform(get(ENTITY_API_URL_ID, hCDecentralization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hCDecentralization.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.decentralization").value(DEFAULT_DECENTRALIZATION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY));
    }

    @Test
    @Transactional
    void getNonExistingHCDecentralization() throws Exception {
        // Get the hCDecentralization
        restHCDecentralizationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHCDecentralization() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();

        // Update the hCDecentralization
        HCDecentralization updatedHCDecentralization = hCDecentralizationRepository.findById(hCDecentralization.getId()).get();
        // Disconnect from session so that the updates on updatedHCDecentralization are not directly saved in db
        em.detach(updatedHCDecentralization);
        updatedHCDecentralization
            .userId(UPDATED_USER_ID)
            .decentralization(UPDATED_DECENTRALIZATION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(updatedHCDecentralization);

        restHCDecentralizationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCDecentralizationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isOk());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
        HCDecentralization testHCDecentralization = hCDecentralizationList.get(hCDecentralizationList.size() - 1);
        assertThat(testHCDecentralization.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testHCDecentralization.getDecentralization()).isEqualTo(UPDATED_DECENTRALIZATION);
        assertThat(testHCDecentralization.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCDecentralization.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCDecentralization.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCDecentralization.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void putNonExistingHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCDecentralizationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHCDecentralizationWithPatch() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();

        // Update the hCDecentralization using partial update
        HCDecentralization partialUpdatedHCDecentralization = new HCDecentralization();
        partialUpdatedHCDecentralization.setId(hCDecentralization.getId());

        partialUpdatedHCDecentralization.createdBy(UPDATED_CREATED_BY).updatedDate(UPDATED_UPDATED_DATE).updateBy(UPDATED_UPDATE_BY);

        restHCDecentralizationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCDecentralization.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCDecentralization))
            )
            .andExpect(status().isOk());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
        HCDecentralization testHCDecentralization = hCDecentralizationList.get(hCDecentralizationList.size() - 1);
        assertThat(testHCDecentralization.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testHCDecentralization.getDecentralization()).isEqualTo(DEFAULT_DECENTRALIZATION);
        assertThat(testHCDecentralization.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testHCDecentralization.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCDecentralization.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCDecentralization.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void fullUpdateHCDecentralizationWithPatch() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();

        // Update the hCDecentralization using partial update
        HCDecentralization partialUpdatedHCDecentralization = new HCDecentralization();
        partialUpdatedHCDecentralization.setId(hCDecentralization.getId());

        partialUpdatedHCDecentralization
            .userId(UPDATED_USER_ID)
            .decentralization(UPDATED_DECENTRALIZATION)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restHCDecentralizationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCDecentralization.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCDecentralization))
            )
            .andExpect(status().isOk());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
        HCDecentralization testHCDecentralization = hCDecentralizationList.get(hCDecentralizationList.size() - 1);
        assertThat(testHCDecentralization.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testHCDecentralization.getDecentralization()).isEqualTo(UPDATED_DECENTRALIZATION);
        assertThat(testHCDecentralization.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCDecentralization.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCDecentralization.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCDecentralization.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void patchNonExistingHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hCDecentralizationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHCDecentralization() throws Exception {
        int databaseSizeBeforeUpdate = hCDecentralizationRepository.findAll().size();
        hCDecentralization.setId(count.incrementAndGet());

        // Create the HCDecentralization
        HCDecentralizationDTO hCDecentralizationDTO = hCDecentralizationMapper.toDto(hCDecentralization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCDecentralizationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCDecentralizationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCDecentralization in the database
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHCDecentralization() throws Exception {
        // Initialize the database
        hCDecentralizationRepository.saveAndFlush(hCDecentralization);

        int databaseSizeBeforeDelete = hCDecentralizationRepository.findAll().size();

        // Delete the hCDecentralization
        restHCDecentralizationMockMvc
            .perform(delete(ENTITY_API_URL_ID, hCDecentralization.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HCDecentralization> hCDecentralizationList = hCDecentralizationRepository.findAll();
        assertThat(hCDecentralizationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

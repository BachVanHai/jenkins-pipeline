package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.HCFileUpload;
import nth.module.utilities.repository.HCFileUploadRepository;
import nth.module.utilities.service.dto.HCFileUploadDTO;
import nth.module.utilities.service.mapper.HCFileUploadMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HCFileUploadResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class HCFileUploadResourceIT {

    private static final String DEFAULT_FILE_PATH_ID = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PATH_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PATH = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/hc-file-uploads";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HCFileUploadRepository hCFileUploadRepository;

    @Autowired
    private HCFileUploadMapper hCFileUploadMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHCFileUploadMockMvc;

    private HCFileUpload hCFileUpload;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCFileUpload createEntity(EntityManager em) {
        HCFileUpload hCFileUpload = new HCFileUpload()
            .filePathId(DEFAULT_FILE_PATH_ID)
            .filePath(DEFAULT_FILE_PATH)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return hCFileUpload;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HCFileUpload createUpdatedEntity(EntityManager em) {
        HCFileUpload hCFileUpload = new HCFileUpload()
            .filePathId(UPDATED_FILE_PATH_ID)
            .filePath(UPDATED_FILE_PATH)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        return hCFileUpload;
    }

    @BeforeEach
    public void initTest() {
        hCFileUpload = createEntity(em);
    }

    @Test
    @Transactional
    void createHCFileUpload() throws Exception {
        int databaseSizeBeforeCreate = hCFileUploadRepository.findAll().size();
        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);
        restHCFileUploadMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isCreated());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeCreate + 1);
        HCFileUpload testHCFileUpload = hCFileUploadList.get(hCFileUploadList.size() - 1);
        assertThat(testHCFileUpload.getFilePathId()).isEqualTo(DEFAULT_FILE_PATH_ID);
        assertThat(testHCFileUpload.getFilePath()).isEqualTo(DEFAULT_FILE_PATH);
        assertThat(testHCFileUpload.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testHCFileUpload.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testHCFileUpload.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testHCFileUpload.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    void createHCFileUploadWithExistingId() throws Exception {
        // Create the HCFileUpload with an existing ID
        hCFileUpload.setId(1L);
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        int databaseSizeBeforeCreate = hCFileUploadRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHCFileUploadMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllHCFileUploads() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        // Get all the hCFileUploadList
        restHCFileUploadMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hCFileUpload.getId().intValue())))
            .andExpect(jsonPath("$.[*].filePathId").value(hasItem(DEFAULT_FILE_PATH_ID)))
            .andExpect(jsonPath("$.[*].filePath").value(hasItem(DEFAULT_FILE_PATH)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)));
    }

    @Test
    @Transactional
    void getHCFileUpload() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        // Get the hCFileUpload
        restHCFileUploadMockMvc
            .perform(get(ENTITY_API_URL_ID, hCFileUpload.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hCFileUpload.getId().intValue()))
            .andExpect(jsonPath("$.filePathId").value(DEFAULT_FILE_PATH_ID))
            .andExpect(jsonPath("$.filePath").value(DEFAULT_FILE_PATH))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY));
    }

    @Test
    @Transactional
    void getNonExistingHCFileUpload() throws Exception {
        // Get the hCFileUpload
        restHCFileUploadMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHCFileUpload() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();

        // Update the hCFileUpload
        HCFileUpload updatedHCFileUpload = hCFileUploadRepository.findById(hCFileUpload.getId()).get();
        // Disconnect from session so that the updates on updatedHCFileUpload are not directly saved in db
        em.detach(updatedHCFileUpload);
        updatedHCFileUpload
            .filePathId(UPDATED_FILE_PATH_ID)
            .filePath(UPDATED_FILE_PATH)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(updatedHCFileUpload);

        restHCFileUploadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCFileUploadDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isOk());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
        HCFileUpload testHCFileUpload = hCFileUploadList.get(hCFileUploadList.size() - 1);
        assertThat(testHCFileUpload.getFilePathId()).isEqualTo(UPDATED_FILE_PATH_ID);
        assertThat(testHCFileUpload.getFilePath()).isEqualTo(UPDATED_FILE_PATH);
        assertThat(testHCFileUpload.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCFileUpload.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCFileUpload.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCFileUpload.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void putNonExistingHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hCFileUploadDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHCFileUploadWithPatch() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();

        // Update the hCFileUpload using partial update
        HCFileUpload partialUpdatedHCFileUpload = new HCFileUpload();
        partialUpdatedHCFileUpload.setId(hCFileUpload.getId());

        partialUpdatedHCFileUpload
            .filePathId(UPDATED_FILE_PATH_ID)
            .filePath(UPDATED_FILE_PATH)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restHCFileUploadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCFileUpload.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCFileUpload))
            )
            .andExpect(status().isOk());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
        HCFileUpload testHCFileUpload = hCFileUploadList.get(hCFileUploadList.size() - 1);
        assertThat(testHCFileUpload.getFilePathId()).isEqualTo(UPDATED_FILE_PATH_ID);
        assertThat(testHCFileUpload.getFilePath()).isEqualTo(UPDATED_FILE_PATH);
        assertThat(testHCFileUpload.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCFileUpload.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testHCFileUpload.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCFileUpload.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void fullUpdateHCFileUploadWithPatch() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();

        // Update the hCFileUpload using partial update
        HCFileUpload partialUpdatedHCFileUpload = new HCFileUpload();
        partialUpdatedHCFileUpload.setId(hCFileUpload.getId());

        partialUpdatedHCFileUpload
            .filePathId(UPDATED_FILE_PATH_ID)
            .filePath(UPDATED_FILE_PATH)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restHCFileUploadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHCFileUpload.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHCFileUpload))
            )
            .andExpect(status().isOk());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
        HCFileUpload testHCFileUpload = hCFileUploadList.get(hCFileUploadList.size() - 1);
        assertThat(testHCFileUpload.getFilePathId()).isEqualTo(UPDATED_FILE_PATH_ID);
        assertThat(testHCFileUpload.getFilePath()).isEqualTo(UPDATED_FILE_PATH);
        assertThat(testHCFileUpload.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testHCFileUpload.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testHCFileUpload.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testHCFileUpload.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void patchNonExistingHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hCFileUploadDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHCFileUpload() throws Exception {
        int databaseSizeBeforeUpdate = hCFileUploadRepository.findAll().size();
        hCFileUpload.setId(count.incrementAndGet());

        // Create the HCFileUpload
        HCFileUploadDTO hCFileUploadDTO = hCFileUploadMapper.toDto(hCFileUpload);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHCFileUploadMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hCFileUploadDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HCFileUpload in the database
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHCFileUpload() throws Exception {
        // Initialize the database
        hCFileUploadRepository.saveAndFlush(hCFileUpload);

        int databaseSizeBeforeDelete = hCFileUploadRepository.findAll().size();

        // Delete the hCFileUpload
        restHCFileUploadMockMvc
            .perform(delete(ENTITY_API_URL_ID, hCFileUpload.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HCFileUpload> hCFileUploadList = hCFileUploadRepository.findAll();
        assertThat(hCFileUploadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

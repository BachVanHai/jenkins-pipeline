package nth.module.utilities.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import nth.module.utilities.IntegrationTest;
import nth.module.utilities.domain.Question;
import nth.module.utilities.domain.enumeration.ApplyFor;
import nth.module.utilities.domain.enumeration.StatusQuestion;
import nth.module.utilities.repository.QuestionRepository;
import nth.module.utilities.service.dto.QuestionDTO;
import nth.module.utilities.service.mapper.QuestionMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link QuestionResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class QuestionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_QUEST_TION = "AAAAAAAAAA";
    private static final String UPDATED_QUEST_TION = "BBBBBBBBBB";

    private static final byte[] DEFAULT_RS_TEXT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_RS_TEXT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_RS_TEXT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_RS_TEXT_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_RS_LINK_PDF = "AAAAAAAAAA";
    private static final String UPDATED_RS_LINK_PDF = "BBBBBBBBBB";

    private static final String DEFAULT_RS_LINK_YT = "AAAAAAAAAA";
    private static final String UPDATED_RS_LINK_YT = "BBBBBBBBBB";

    private static final ApplyFor DEFAULT_APPLY_FOR = ApplyFor.PARTNER;
    private static final ApplyFor UPDATED_APPLY_FOR = ApplyFor.INDIVIDUAL;

    private static final String DEFAULT_REASON_REJECT = "AAAAAAAAAA";
    private static final String UPDATED_REASON_REJECT = "BBBBBBBBBB";

    private static final StatusQuestion DEFAULT_STATUS = StatusQuestion.DRAFT;
    private static final StatusQuestion UPDATED_STATUS = StatusQuestion.APPROVALED;

    private static final String DEFAULT_SEND_TO = "AAAAAAAAAA";
    private static final String UPDATED_SEND_TO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/questions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuestionMockMvc;

    private Question question;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Question createEntity(EntityManager em) {
        Question question = new Question()
            .code(DEFAULT_NAME)
            .questTion(DEFAULT_QUEST_TION)
            .rsText(DEFAULT_RS_TEXT)
            .rsTextContentType(DEFAULT_RS_TEXT_CONTENT_TYPE)
            .rsLinkPDF(DEFAULT_RS_LINK_PDF)
            .rsLinkYT(DEFAULT_RS_LINK_YT)
            .applyFor(DEFAULT_APPLY_FOR)
            .reasonReject(DEFAULT_REASON_REJECT)
            .status(DEFAULT_STATUS)
            .sendTo(DEFAULT_SEND_TO)
            .deleted(DEFAULT_DELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updateDate(DEFAULT_UPDATE_DATE)
            .updateBy(DEFAULT_UPDATE_BY);
        return question;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Question createUpdatedEntity(EntityManager em) {
        Question question = new Question()
            .code(UPDATED_NAME)
            .questTion(UPDATED_QUEST_TION)
            .rsText(UPDATED_RS_TEXT)
            .rsTextContentType(UPDATED_RS_TEXT_CONTENT_TYPE)
            .rsLinkPDF(UPDATED_RS_LINK_PDF)
            .rsLinkYT(UPDATED_RS_LINK_YT)
            .applyFor(UPDATED_APPLY_FOR)
            .reasonReject(UPDATED_REASON_REJECT)
            .status(UPDATED_STATUS)
            .sendTo(UPDATED_SEND_TO)
            .deleted(UPDATED_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        return question;
    }

    @BeforeEach
    public void initTest() {
        question = createEntity(em);
    }

    @Test
    @Transactional
    void createQuestion() throws Exception {
        int databaseSizeBeforeCreate = questionRepository.findAll().size();
        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);
        restQuestionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(questionDTO)))
            .andExpect(status().isCreated());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeCreate + 1);
        Question testQuestion = questionList.get(questionList.size() - 1);
        assertThat(testQuestion.getCode()).isEqualTo(DEFAULT_NAME);
        assertThat(testQuestion.getQuestion()).isEqualTo(DEFAULT_QUEST_TION);
        assertThat(testQuestion.getRsText()).isEqualTo(DEFAULT_RS_TEXT);
        assertThat(testQuestion.getRsTextContentType()).isEqualTo(DEFAULT_RS_TEXT_CONTENT_TYPE);
        assertThat(testQuestion.getRsLinkPDF()).isEqualTo(DEFAULT_RS_LINK_PDF);
        assertThat(testQuestion.getRsLinkYT()).isEqualTo(DEFAULT_RS_LINK_YT);
        assertThat(testQuestion.getApplyFor()).isEqualTo(DEFAULT_APPLY_FOR);
        assertThat(testQuestion.getReasonReject()).isEqualTo(DEFAULT_REASON_REJECT);
        assertThat(testQuestion.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testQuestion.getSendTo()).isEqualTo(DEFAULT_SEND_TO);
        assertThat(testQuestion.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testQuestion.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testQuestion.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testQuestion.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testQuestion.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
    }

    @Test
    @Transactional
    void createQuestionWithExistingId() throws Exception {
        // Create the Question with an existing ID
        question.setId(1L);
        QuestionDTO questionDTO = questionMapper.toDto(question);

        int databaseSizeBeforeCreate = questionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(questionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllQuestions() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList
        restQuestionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(question.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].questTion").value(hasItem(DEFAULT_QUEST_TION)))
            .andExpect(jsonPath("$.[*].rsTextContentType").value(hasItem(DEFAULT_RS_TEXT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].rsText").value(hasItem(Base64Utils.encodeToString(DEFAULT_RS_TEXT))))
            .andExpect(jsonPath("$.[*].rsLinkPDF").value(hasItem(DEFAULT_RS_LINK_PDF)))
            .andExpect(jsonPath("$.[*].rsLinkYT").value(hasItem(DEFAULT_RS_LINK_YT)))
            .andExpect(jsonPath("$.[*].applyFor").value(hasItem(DEFAULT_APPLY_FOR.toString())))
            .andExpect(jsonPath("$.[*].reasonReject").value(hasItem(DEFAULT_REASON_REJECT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].sendTo").value(hasItem(DEFAULT_SEND_TO)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)));
    }

    @Test
    @Transactional
    void getQuestion() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get the question
        restQuestionMockMvc
            .perform(get(ENTITY_API_URL_ID, question.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(question.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.questTion").value(DEFAULT_QUEST_TION))
            .andExpect(jsonPath("$.rsTextContentType").value(DEFAULT_RS_TEXT_CONTENT_TYPE))
            .andExpect(jsonPath("$.rsText").value(Base64Utils.encodeToString(DEFAULT_RS_TEXT)))
            .andExpect(jsonPath("$.rsLinkPDF").value(DEFAULT_RS_LINK_PDF))
            .andExpect(jsonPath("$.rsLinkYT").value(DEFAULT_RS_LINK_YT))
            .andExpect(jsonPath("$.applyFor").value(DEFAULT_APPLY_FOR.toString()))
            .andExpect(jsonPath("$.reasonReject").value(DEFAULT_REASON_REJECT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.sendTo").value(DEFAULT_SEND_TO))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY));
    }

    @Test
    @Transactional
    void getNonExistingQuestion() throws Exception {
        // Get the question
        restQuestionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewQuestion() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        int databaseSizeBeforeUpdate = questionRepository.findAll().size();

        // Update the question
        Question updatedQuestion = questionRepository.findById(question.getId()).get();
        // Disconnect from session so that the updates on updatedQuestion are not directly saved in db
        em.detach(updatedQuestion);
        updatedQuestion
            .code(UPDATED_NAME)
            .questTion(UPDATED_QUEST_TION)
            .rsText(UPDATED_RS_TEXT)
            .rsTextContentType(UPDATED_RS_TEXT_CONTENT_TYPE)
            .rsLinkPDF(UPDATED_RS_LINK_PDF)
            .rsLinkYT(UPDATED_RS_LINK_YT)
            .applyFor(UPDATED_APPLY_FOR)
            .reasonReject(UPDATED_REASON_REJECT)
            .status(UPDATED_STATUS)
            .sendTo(UPDATED_SEND_TO)
            .deleted(UPDATED_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);
        QuestionDTO questionDTO = questionMapper.toDto(updatedQuestion);

        restQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, questionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isOk());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
        Question testQuestion = questionList.get(questionList.size() - 1);
        assertThat(testQuestion.getCode()).isEqualTo(UPDATED_NAME);
        assertThat(testQuestion.getQuestion()).isEqualTo(UPDATED_QUEST_TION);
        assertThat(testQuestion.getRsText()).isEqualTo(UPDATED_RS_TEXT);
        assertThat(testQuestion.getRsTextContentType()).isEqualTo(UPDATED_RS_TEXT_CONTENT_TYPE);
        assertThat(testQuestion.getRsLinkPDF()).isEqualTo(UPDATED_RS_LINK_PDF);
        assertThat(testQuestion.getRsLinkYT()).isEqualTo(UPDATED_RS_LINK_YT);
        assertThat(testQuestion.getApplyFor()).isEqualTo(UPDATED_APPLY_FOR);
        assertThat(testQuestion.getReasonReject()).isEqualTo(UPDATED_REASON_REJECT);
        assertThat(testQuestion.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testQuestion.getSendTo()).isEqualTo(UPDATED_SEND_TO);
        assertThat(testQuestion.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testQuestion.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testQuestion.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void putNonExistingQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, questionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(questionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateQuestionWithPatch() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        int databaseSizeBeforeUpdate = questionRepository.findAll().size();

        // Update the question using partial update
        Question partialUpdatedQuestion = new Question();
        partialUpdatedQuestion.setId(question.getId());

        partialUpdatedQuestion
            .code(UPDATED_NAME)
            .questTion(UPDATED_QUEST_TION)
            .rsText(UPDATED_RS_TEXT)
            .rsTextContentType(UPDATED_RS_TEXT_CONTENT_TYPE)
            .applyFor(UPDATED_APPLY_FOR)
            .reasonReject(UPDATED_REASON_REJECT)
            .status(UPDATED_STATUS)
            .deleted(UPDATED_DELETED)
            .updateBy(UPDATED_UPDATE_BY);

        restQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuestion.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuestion))
            )
            .andExpect(status().isOk());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
        Question testQuestion = questionList.get(questionList.size() - 1);
        assertThat(testQuestion.getCode()).isEqualTo(UPDATED_NAME);
        assertThat(testQuestion.getQuestion()).isEqualTo(UPDATED_QUEST_TION);
        assertThat(testQuestion.getRsText()).isEqualTo(UPDATED_RS_TEXT);
        assertThat(testQuestion.getRsTextContentType()).isEqualTo(UPDATED_RS_TEXT_CONTENT_TYPE);
        assertThat(testQuestion.getRsLinkPDF()).isEqualTo(DEFAULT_RS_LINK_PDF);
        assertThat(testQuestion.getRsLinkYT()).isEqualTo(DEFAULT_RS_LINK_YT);
        assertThat(testQuestion.getApplyFor()).isEqualTo(UPDATED_APPLY_FOR);
        assertThat(testQuestion.getReasonReject()).isEqualTo(UPDATED_REASON_REJECT);
        assertThat(testQuestion.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testQuestion.getSendTo()).isEqualTo(DEFAULT_SEND_TO);
        assertThat(testQuestion.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testQuestion.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testQuestion.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testQuestion.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void fullUpdateQuestionWithPatch() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        int databaseSizeBeforeUpdate = questionRepository.findAll().size();

        // Update the question using partial update
        Question partialUpdatedQuestion = new Question();
        partialUpdatedQuestion.setId(question.getId());

        partialUpdatedQuestion
            .code(UPDATED_NAME)
            .questTion(UPDATED_QUEST_TION)
            .rsText(UPDATED_RS_TEXT)
            .rsTextContentType(UPDATED_RS_TEXT_CONTENT_TYPE)
            .rsLinkPDF(UPDATED_RS_LINK_PDF)
            .rsLinkYT(UPDATED_RS_LINK_YT)
            .applyFor(UPDATED_APPLY_FOR)
            .reasonReject(UPDATED_REASON_REJECT)
            .status(UPDATED_STATUS)
            .sendTo(UPDATED_SEND_TO)
            .deleted(UPDATED_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updateDate(UPDATED_UPDATE_DATE)
            .updateBy(UPDATED_UPDATE_BY);

        restQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuestion.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuestion))
            )
            .andExpect(status().isOk());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
        Question testQuestion = questionList.get(questionList.size() - 1);
        assertThat(testQuestion.getCode()).isEqualTo(UPDATED_NAME);
        assertThat(testQuestion.getQuestion()).isEqualTo(UPDATED_QUEST_TION);
        assertThat(testQuestion.getRsText()).isEqualTo(UPDATED_RS_TEXT);
        assertThat(testQuestion.getRsTextContentType()).isEqualTo(UPDATED_RS_TEXT_CONTENT_TYPE);
        assertThat(testQuestion.getRsLinkPDF()).isEqualTo(UPDATED_RS_LINK_PDF);
        assertThat(testQuestion.getRsLinkYT()).isEqualTo(UPDATED_RS_LINK_YT);
        assertThat(testQuestion.getApplyFor()).isEqualTo(UPDATED_APPLY_FOR);
        assertThat(testQuestion.getReasonReject()).isEqualTo(UPDATED_REASON_REJECT);
        assertThat(testQuestion.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testQuestion.getSendTo()).isEqualTo(UPDATED_SEND_TO);
        assertThat(testQuestion.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testQuestion.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testQuestion.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testQuestion.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
    }

    @Test
    @Transactional
    void patchNonExistingQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, questionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
        question.setId(count.incrementAndGet());

        // Create the Question
        QuestionDTO questionDTO = questionMapper.toDto(question);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuestionMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(questionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteQuestion() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        int databaseSizeBeforeDelete = questionRepository.findAll().size();

        // Delete the question
        restQuestionMockMvc
            .perform(delete(ENTITY_API_URL_ID, question.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

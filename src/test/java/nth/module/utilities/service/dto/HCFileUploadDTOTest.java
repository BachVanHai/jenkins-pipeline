package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCFileUploadDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCFileUploadDTO.class);
        HCFileUploadDTO hCFileUploadDTO1 = new HCFileUploadDTO();
        hCFileUploadDTO1.setId(1L);
        HCFileUploadDTO hCFileUploadDTO2 = new HCFileUploadDTO();
        assertThat(hCFileUploadDTO1).isNotEqualTo(hCFileUploadDTO2);
        hCFileUploadDTO2.setId(hCFileUploadDTO1.getId());
        assertThat(hCFileUploadDTO1).isEqualTo(hCFileUploadDTO2);
        hCFileUploadDTO2.setId(2L);
        assertThat(hCFileUploadDTO1).isNotEqualTo(hCFileUploadDTO2);
        hCFileUploadDTO1.setId(null);
        assertThat(hCFileUploadDTO1).isNotEqualTo(hCFileUploadDTO2);
    }
}

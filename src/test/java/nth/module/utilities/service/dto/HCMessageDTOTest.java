package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCMessageDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCMessageDTO.class);
        HCMessageDTO hCMessageDTO1 = new HCMessageDTO();
        hCMessageDTO1.setId(1L);
        HCMessageDTO hCMessageDTO2 = new HCMessageDTO();
        assertThat(hCMessageDTO1).isNotEqualTo(hCMessageDTO2);
        hCMessageDTO2.setId(hCMessageDTO1.getId());
        assertThat(hCMessageDTO1).isEqualTo(hCMessageDTO2);
        hCMessageDTO2.setId(2L);
        assertThat(hCMessageDTO1).isNotEqualTo(hCMessageDTO2);
        hCMessageDTO1.setId(null);
        assertThat(hCMessageDTO1).isNotEqualTo(hCMessageDTO2);
    }
}

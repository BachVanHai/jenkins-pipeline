package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCSupportBookDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCSupportBookDTO.class);
        HCSupportBookDTO hCSupportBookDTO1 = new HCSupportBookDTO();
        hCSupportBookDTO1.setId(1L);
        HCSupportBookDTO hCSupportBookDTO2 = new HCSupportBookDTO();
        assertThat(hCSupportBookDTO1).isNotEqualTo(hCSupportBookDTO2);
        hCSupportBookDTO2.setId(hCSupportBookDTO1.getId());
        assertThat(hCSupportBookDTO1).isEqualTo(hCSupportBookDTO2);
        hCSupportBookDTO2.setId(2L);
        assertThat(hCSupportBookDTO1).isNotEqualTo(hCSupportBookDTO2);
        hCSupportBookDTO1.setId(null);
        assertThat(hCSupportBookDTO1).isNotEqualTo(hCSupportBookDTO2);
    }
}

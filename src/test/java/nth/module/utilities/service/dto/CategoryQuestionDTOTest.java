package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CategoryQuestionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoryQuestionDTO.class);
        CategoryQuestionDTO categoryQuestionDTO1 = new CategoryQuestionDTO();
        categoryQuestionDTO1.setId(1L);
        CategoryQuestionDTO categoryQuestionDTO2 = new CategoryQuestionDTO();
        assertThat(categoryQuestionDTO1).isNotEqualTo(categoryQuestionDTO2);
        categoryQuestionDTO2.setId(categoryQuestionDTO1.getId());
        assertThat(categoryQuestionDTO1).isEqualTo(categoryQuestionDTO2);
        categoryQuestionDTO2.setId(2L);
        assertThat(categoryQuestionDTO1).isNotEqualTo(categoryQuestionDTO2);
        categoryQuestionDTO1.setId(null);
        assertThat(categoryQuestionDTO1).isNotEqualTo(categoryQuestionDTO2);
    }
}

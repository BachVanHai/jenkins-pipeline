package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCUserDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCUserDTO.class);
        HCUserDTO hCUserDTO1 = new HCUserDTO();
        hCUserDTO1.setId(1L);
        HCUserDTO hCUserDTO2 = new HCUserDTO();
        assertThat(hCUserDTO1).isNotEqualTo(hCUserDTO2);
        hCUserDTO2.setId(hCUserDTO1.getId());
        assertThat(hCUserDTO1).isEqualTo(hCUserDTO2);
        hCUserDTO2.setId(2L);
        assertThat(hCUserDTO1).isNotEqualTo(hCUserDTO2);
        hCUserDTO1.setId(null);
        assertThat(hCUserDTO1).isNotEqualTo(hCUserDTO2);
    }
}

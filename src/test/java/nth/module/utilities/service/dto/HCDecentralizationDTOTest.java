package nth.module.utilities.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCDecentralizationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCDecentralizationDTO.class);
        HCDecentralizationDTO hCDecentralizationDTO1 = new HCDecentralizationDTO();
        hCDecentralizationDTO1.setId(1L);
        HCDecentralizationDTO hCDecentralizationDTO2 = new HCDecentralizationDTO();
        assertThat(hCDecentralizationDTO1).isNotEqualTo(hCDecentralizationDTO2);
        hCDecentralizationDTO2.setId(hCDecentralizationDTO1.getId());
        assertThat(hCDecentralizationDTO1).isEqualTo(hCDecentralizationDTO2);
        hCDecentralizationDTO2.setId(2L);
        assertThat(hCDecentralizationDTO1).isNotEqualTo(hCDecentralizationDTO2);
        hCDecentralizationDTO1.setId(null);
        assertThat(hCDecentralizationDTO1).isNotEqualTo(hCDecentralizationDTO2);
    }
}

package nth.module.utilities;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("nth.module.utilities");

        noClasses()
            .that()
            .resideInAnyPackage("nth.module.utilities.service..")
            .or()
            .resideInAnyPackage("nth.module.utilities.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..nth.module.utilities.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}

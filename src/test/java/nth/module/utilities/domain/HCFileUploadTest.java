package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCFileUploadTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCFileUpload.class);
        HCFileUpload hCFileUpload1 = new HCFileUpload();
        hCFileUpload1.setId(1L);
        HCFileUpload hCFileUpload2 = new HCFileUpload();
        hCFileUpload2.setId(hCFileUpload1.getId());
        assertThat(hCFileUpload1).isEqualTo(hCFileUpload2);
        hCFileUpload2.setId(2L);
        assertThat(hCFileUpload1).isNotEqualTo(hCFileUpload2);
        hCFileUpload1.setId(null);
        assertThat(hCFileUpload1).isNotEqualTo(hCFileUpload2);
    }
}

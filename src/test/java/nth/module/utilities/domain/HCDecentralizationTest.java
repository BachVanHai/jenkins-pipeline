package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCDecentralizationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCDecentralization.class);
        HCDecentralization hCDecentralization1 = new HCDecentralization();
        hCDecentralization1.setId(1L);
        HCDecentralization hCDecentralization2 = new HCDecentralization();
        hCDecentralization2.setId(hCDecentralization1.getId());
        assertThat(hCDecentralization1).isEqualTo(hCDecentralization2);
        hCDecentralization2.setId(2L);
        assertThat(hCDecentralization1).isNotEqualTo(hCDecentralization2);
        hCDecentralization1.setId(null);
        assertThat(hCDecentralization1).isNotEqualTo(hCDecentralization2);
    }
}

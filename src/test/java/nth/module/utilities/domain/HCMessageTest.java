package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCMessageTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCMessage.class);
        HCMessage hCMessage1 = new HCMessage();
        hCMessage1.setId(1L);
        HCMessage hCMessage2 = new HCMessage();
        hCMessage2.setId(hCMessage1.getId());
        assertThat(hCMessage1).isEqualTo(hCMessage2);
        hCMessage2.setId(2L);
        assertThat(hCMessage1).isNotEqualTo(hCMessage2);
        hCMessage1.setId(null);
        assertThat(hCMessage1).isNotEqualTo(hCMessage2);
    }
}

package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCSupportBookTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCSupportBook.class);
        HCSupportBook hCSupportBook1 = new HCSupportBook();
        hCSupportBook1.setId(1L);
        HCSupportBook hCSupportBook2 = new HCSupportBook();
        hCSupportBook2.setId(hCSupportBook1.getId());
        assertThat(hCSupportBook1).isEqualTo(hCSupportBook2);
        hCSupportBook2.setId(2L);
        assertThat(hCSupportBook1).isNotEqualTo(hCSupportBook2);
        hCSupportBook1.setId(null);
        assertThat(hCSupportBook1).isNotEqualTo(hCSupportBook2);
    }
}

package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CategoryQuestionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoryQuestion.class);
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.setId(1L);
        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.setId(categoryQuestion1.getId());
        assertThat(categoryQuestion1).isEqualTo(categoryQuestion2);
        categoryQuestion2.setId(2L);
        assertThat(categoryQuestion1).isNotEqualTo(categoryQuestion2);
        categoryQuestion1.setId(null);
        assertThat(categoryQuestion1).isNotEqualTo(categoryQuestion2);
    }
}

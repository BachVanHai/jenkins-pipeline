package nth.module.utilities.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nth.module.utilities.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HCUserTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HCUser.class);
        HCUser hCUser1 = new HCUser();
        hCUser1.setId(1L);
        HCUser hCUser2 = new HCUser();
        hCUser2.setId(hCUser1.getId());
        assertThat(hCUser1).isEqualTo(hCUser2);
        hCUser2.setId(2L);
        assertThat(hCUser1).isNotEqualTo(hCUser2);
        hCUser1.setId(null);
        assertThat(hCUser1).isNotEqualTo(hCUser2);
    }
}

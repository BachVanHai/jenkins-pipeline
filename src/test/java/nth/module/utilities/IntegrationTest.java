package nth.module.utilities;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import nth.module.utilities.RedisTestContainerExtension;
import nth.module.utilities.UtilitiesApp;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = UtilitiesApp.class)
@ExtendWith(RedisTestContainerExtension.class)
public @interface IntegrationTest {
}
